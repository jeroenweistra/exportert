using System;
using System.Windows.Forms;
using TSM = Tekla.Structures.Model;
using TSG = Tekla.Structures.Geometry3d;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Tekla.Structures.Drawing;

namespace BVBS_Export
{
    public partial class MainForm : Tekla.Structures.Dialog.PluginFormBase
    {
        public MainForm()
        {
            InitializeComponent();

            TxKlasse.Width = 0;
            TxRadbutton.Width = 0;

            btnKorf.Text = TxKorven.Text;

            lblVersie.Text = lblVersie.Text.Replace("xxx", this.ProductVersion);
            lblDatum.Text = lblDatum.Text.Replace("datedate",
            File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location)
            .ToString("yyyy-MM-dd"));

        }

        private void OkApplyModifyGetOnOffCancel_OkClicked(object sender, EventArgs e)
        {
            this.Apply();
            this.Close();
        }

        private void OkApplyModifyGetOnOffCancel_ApplyClicked(object sender, EventArgs e)
        {
            this.Apply();
        }

        private void OkApplyModifyGetOnOffCancel_ModifyClicked(object sender, EventArgs e)
        {
            this.Modify();
        }

        private void OkApplyModifyGetOnOffCancel_GetClicked(object sender, EventArgs e)
        {
            this.Get();
        }

        private void OkApplyModifyGetOnOffCancel_OnOffClicked(object sender, EventArgs e)
        {
            this.ToggleSelection();
        }

        private void OkApplyModifyGetOnOffCancel_CancelClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxKlasse_TextChanged(object sender, EventArgs e)
        {
            //  this.SetAttributeValue(TxKlasse, TxKlasse.Text);
        }

        private void BtnBVBS_Click(object sender, EventArgs e)
        {
            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                var model = new TSM.Model();
                var selectedObjects = new ArrayList();
                var mos = new TSM.UI.ModelObjectSelector();
                TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();
                while (moe.MoveNext())
                {
                    var part = moe.Current as TSM.Part;
                    if (part != null)
                    {
                        selectedObjects.Add(moe.Current);
                    }
                }

                BtnKorf_Click(sender, e);

                mos.Select(selectedObjects);
                BtnBalken_Click(sender, e);

                mos.Select(selectedObjects);
                BtnBalkenD_Click(sender, e);

                mos.Select(selectedObjects);
                BtnKorvenSchuin_Click(sender, e);

                mos.Select(selectedObjects);
                BtnBalkenSchuin_Click(sender, e);

                mos.Select(selectedObjects);
                BtnKorf3_Click(sender, e);

                mos.Select(selectedObjects);
                BtnKorf3_Click(sender, e);

                //mos.Select(selectedObjects);
                //BtnNetSup_Click(sender, e);

                mos.Select(selectedObjects);
                BtnPoer_Click(sender, e);

                TSM.Operations.Operation.DisplayPrompt("Alle BVBS exporten gereed");
            }
            else
            {
                MessageBox.Show("Nummering is niet up-to-date");

            }

        }

        private void BtnNummeren_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();
            if (!TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                TSM.Operations.Operation.RunMacro("Number_Modified");
                MessageBox.Show("Nummering is bijgewerkt");
                lblNummering.Text = "Nummering is nu up-to-date";
            }
            else lblNummering.Text = "Nummering is up-to-date";
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void BtnKorf_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList korven = new ArrayList();
                    ArrayList wapKorven = new ArrayList();
                    string map = "";


                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null && part.Name == "KORF")
                        {
                            int _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                int _id = part.Identifier.ID;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }
                            korven.Add(part);
                        }


                    }
                    TSM.Operations.Operation.DisplayPrompt(korven.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                    string merk = "";
                    int acn = 0;
                    string bestandsnaam = "";

                    foreach (TSM.Part export in korven)
                    {
                        wapKorven.Clear();
                        export.GetReportProperty("ASSEMBLY_POS", ref merk);
                        export.GetReportProperty("ACN", ref acn);
                        //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                        bestandsnaam = merk + "_" + acn;

                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix != "LR")
                                {
                                    wapKorven.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix != "LR")
                                {
                                    wapKorven.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix != "D")
                                {
                                    wapKorven.Add(bar);
                                }
                            }
                        }
                        //}

                        //gefilterde staven
                        modelObjectSelector.Select(wapKorven);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("DrwNameSource", 0);
                        if (RadAlles.Checked)
                        {
                            component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                        }
                        if (RadPerFase.Checked)
                        {
                            string bestandsnaamFase = "";
                            //fase ophalen
                            int fase = 0;
                            export.GetReportProperty("PHASE", ref fase);
                            map = "fase " + fase;
                            bestandsnaamFase = fase + "-" + bestandsnaam;


                            if (!Directory.Exists("BVBS\\" + map))
                            {
                                Directory.CreateDirectory("BVBS\\" + map);
                            }
                            component.SetAttribute("FolderName", "BVBS\\" + map);
                            component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                        }
                        component.Insert();

                    }

                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }

                /*
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList korven = new ArrayList();
                    ArrayList korven_fase = new ArrayList();
                    List<int> faseList = new List<int>();
                    int _id = 0;
                    string map = "";

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null && part.Name == "KORF")
                        {
                            //check aanwezigheid van acn nummer
                            int _acn = 0;
                            part.GetReportProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                _id = part.Identifier.ID;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            if (RadPerFase.Checked)
                            {
                                korven_fase.Clear();
                                MessageBox.Show("korven fase " + korven_fase.Count + " | " + _acn);
                                //fase ophalen
                                int fase = 0;
                                part.GetReportProperty("PHASE", ref fase);
                                map = "fase " + fase;
                                //faseList.Add(fase);
                                korven_fase.Add(part);
                                MessageBox.Show("korven fase " + korven_fase.Count);


                                // MessageBox.Show("Fasemappen knop staat aan");

                                if (!Directory.Exists("BVBS\\" + map))
                                {
                                    Directory.CreateDirectory("BVBS\\" + map);
                                }
                                component.SetAttribute("FolderName", "BVBS\\" + map);

                                modelObjectSelector.Select(korven_fase);
                                component.LoadAttributesFromFile("bvbs_korf");
                                component.Insert();


                            }

                            if (RadAlles.Checked)
                            {
                                korven.Add(part);
                            }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(korven.Count.ToString());



                    modelObjectSelector.Select(korven);
                    component.LoadAttributesFromFile("bvbs_korf");

                    component.Insert();

                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
                */
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnBalken_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList balken = new ArrayList();
                    //string map = "";
                    ArrayList wapBalkenLR = new ArrayList();
                    // ArrayList wapBalkenLS = new ArrayList();
                    //ArrayList wapBalkenDekking = new ArrayList();

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null)
                        {
                            string naam = "";
                            part.GetReportProperty("NAME", ref naam);

                            if (naam == "BALK") { balken.Add(part); }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(balken.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    foreach (TSM.Part export in balken)
                    {
                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix == "LR")
                                {
                                    wapBalkenLR.Add(bars);
                                }
                                //if (bars.NumberingSeries.Prefix == "D")
                                //{
                                //    wapBalkenDekking.Add(bars);
                                //}
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix == "LR")
                                {
                                    wapBalkenLR.Add(bar);
                                }
                                //if (bar.NumberingSeries.Prefix == "D")
                                //{
                                //    wapBalkenDekking.Add(bar);
                                //}
                            }
                        }
                    }

                    //gefilterde staven
                    if (RadAlles.Checked)
                    {
                        //modelObjectSelector.Select(wapBalkenDekking);
                        //component.LoadAttributesFromFile("bs_standaard");
                        //component.SetAttribute("FileName", ".\\BVBS\\D1.abs");
                        //component.Insert();

                        modelObjectSelector.Select(wapBalkenLR);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\L1.abs");
                        component.Insert();
                    }

                    if (RadPerFase.Checked)
                    {
                        //geselecteerde filteren per fase
                        List<int> faseList = new List<int>();

                        //ArrayList wapBalkenDekking1 = new ArrayList();
                        //ArrayList wapBalkenDekking2 = new ArrayList();
                        //ArrayList wapBalkenDekking3 = new ArrayList();
                        ArrayList wapBalkenLR1 = new ArrayList();
                        ArrayList wapBalkenLR2 = new ArrayList();
                        ArrayList wapBalkenLR3 = new ArrayList();
                        // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                        int fase = 0;
                        //foreach (var itemSingle in wapBalkenDekking)
                        //{
                        //    var bars = itemSingle as TSM.RebarGroup;
                        //    var bar = itemSingle as TSM.SingleRebar;

                        //    if (bar != null)
                        //    {
                        //        bar.GetReportProperty("PHASE", ref fase);
                        //        if (fase == 1) wapBalkenDekking1.Add(itemSingle);
                        //        if (fase == 2) wapBalkenDekking2.Add(itemSingle);
                        //        if (fase == 3) wapBalkenDekking3.Add(itemSingle);
                        //    }

                        //    if (bars != null)
                        //    {
                        //        bars.GetReportProperty("PHASE", ref fase);
                        //        if (fase == 1) wapBalkenDekking1.Add(itemSingle);
                        //        if (fase == 2) wapBalkenDekking2.Add(itemSingle);
                        //        if (fase == 3) wapBalkenDekking3.Add(itemSingle);
                        //    }

                        //    // faseList.Add(fase);
                        //}

                        foreach (var itemSingle in wapBalkenLR)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            var bar = itemSingle as TSM.SingleRebar;

                            if (bar != null)
                            {
                                bar.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalkenLR1.Add(itemSingle);
                                if (fase == 2) wapBalkenLR2.Add(itemSingle);
                                if (fase == 3) wapBalkenLR3.Add(itemSingle);
                            }

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalkenLR1.Add(itemSingle);
                                if (fase == 2) wapBalkenLR2.Add(itemSingle);
                                if (fase == 3) wapBalkenLR3.Add(itemSingle);
                            }
                        }



                        //if (wapBalkenDekking1.Count > 0)
                        //{
                        //    modelObjectSelector.Select(wapBalkenDekking1);
                        //    component.LoadAttributesFromFile("bs_standaard");
                        //    if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                        //    component.SetAttribute("FolderName", "BVBS\\fase 1");
                        //    component.SetAttribute("FileName", ".\\BVBS\\fase 1\\D1.abs");
                        //    component.Insert();
                        //}

                        //if (wapBalkenDekking2.Count > 0)
                        //{
                        //    modelObjectSelector.Select(wapBalkenDekking2);
                        //    component.LoadAttributesFromFile("bs_standaard");
                        //    if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                        //    component.SetAttribute("FolderName", "BVBS\\fase 2");
                        //    component.SetAttribute("FileName", ".\\BVBS\\fase 2\\D1.abs");
                        //    component.Insert();
                        //}

                        //if (wapBalkenDekking3.Count > 0)
                        //{
                        //    modelObjectSelector.Select(wapBalkenDekking3);
                        //    component.LoadAttributesFromFile("bs_standaard");
                        //    if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                        //    component.SetAttribute("FolderName", "BVBS\\fase 3");
                        //    component.SetAttribute("FileName", ".\\BVBS\\fase 3\\D1.abs");
                        //    component.Insert();
                        //}

                        //LR
                        if (wapBalkenLR1.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLR1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\L1.abs");
                            component.Insert();
                        }

                        if (wapBalkenLR2.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLR2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\L1.abs");
                            component.Insert();
                        }

                        if (wapBalkenLR3.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLR3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\L1.abs");
                            component.Insert();
                        }
                    }


                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void RadAlles_CheckedChanged(object sender, EventArgs e)
        {
            TxRadbutton.Text = "A";
        }

        private void RadPerFase_CheckedChanged(object sender, EventArgs e)
        {
            TxRadbutton.Text = "P";
        }

        private void TxRadbutton_TextChanged(object sender, EventArgs e)
        {
            //switch (TxRadbutton.Text.ToString())
            //{
            //    //case "A":
            //    //    RadAlles.Checked = true;
            //    //    break;
            //    //case "P":
            //    //    RadPerFase.Checked = true;
            //    //    break;
            //}
        }

        private void ParametersTabPage_Click(object sender, EventArgs e)
        {

        }

        private void SaveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string s = dataGridView1.Rows[1].Cells[0].Value.ToString();
            btnKorf.Text = s;
        }

        private void BtnKorvenSchuin_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList korvenS = new ArrayList();
                    ArrayList wapKorvenS = new ArrayList();
                    string map = "";


                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null && part.Name == "KORF_SCHUIN")
                        {
                            int _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                int _id = part.Identifier.ID;
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }
                            korvenS.Add(part);
                        }


                    }
                    TSM.Operations.Operation.DisplayPrompt(korvenS.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                    string merk = "";
                    int acn = 0;
                    string bestandsnaam = "";

                    foreach (TSM.Part export in korvenS)
                    {
                        export.GetReportProperty("ASSEMBLY_POS", ref merk);
                        export.GetReportProperty("ACN", ref acn);
                        //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                        bestandsnaam = merk + "_" + acn;

                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix != "LS")
                                {
                                    wapKorvenS.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix != "LS")
                                {
                                    wapKorvenS.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix != "D")
                                {
                                    wapKorvenS.Add(bar);
                                }
                            }
                        }
                        //}

                        //gefilterde staven
                        modelObjectSelector.Select(wapKorvenS);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("DrwNameSource", 0);
                        if (RadAlles.Checked)
                        {
                            component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                        }
                        if (RadPerFase.Checked)
                        {
                            string bestandsnaamFase = "";
                            //fase ophalen
                            int fase = 0;
                            export.GetReportProperty("PHASE", ref fase);
                            map = "fase " + fase;
                            bestandsnaamFase = fase + "-" + bestandsnaam;


                            if (!Directory.Exists("BVBS\\" + map))
                            {
                                Directory.CreateDirectory("BVBS\\" + map);
                            }
                            component.SetAttribute("FolderName", "BVBS\\" + map);
                            component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                        }
                        component.Insert();

                    }

                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }

            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnKorf3_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList korven3 = new ArrayList();
                    ArrayList wapKorven3 = new ArrayList();
                    string map = "";


                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null && part.Name == "3_SNEDIGE_KORF")
                        {
                            int _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                int _id = part.Identifier.ID;
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }
                            korven3.Add(part);
                        }


                    }
                    TSM.Operations.Operation.DisplayPrompt(korven3.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                    string merk = "";
                    int acn = 0;
                    string bestandsnaam = "";

                    foreach (TSM.Part export in korven3)
                    {
                        export.GetReportProperty("ASSEMBLY_POS", ref merk);
                        export.GetReportProperty("ACN", ref acn);
                        //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                        bestandsnaam = merk + "_" + acn;

                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix != "LR")  // ervan uitgaand dat 3 snedige korven rechte staven hebben
                                {
                                    wapKorven3.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix != "LR")
                                {
                                    wapKorven3.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix != "D")
                                {
                                    wapKorven3.Add(bar);
                                }
                            }
                        }
                        //}

                        //gefilterde staven
                        modelObjectSelector.Select(wapKorven3);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("DrwNameSource", 0);
                        if (RadAlles.Checked)
                        {
                            component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                        }
                        if (RadPerFase.Checked)
                        {
                            string bestandsnaamFase = "";
                            //fase ophalen
                            int fase = 0;
                            export.GetReportProperty("PHASE", ref fase);
                            map = "fase " + fase;
                            bestandsnaamFase = fase + "-" + bestandsnaam;


                            if (!Directory.Exists("BVBS\\" + map))
                            {
                                Directory.CreateDirectory("BVBS\\" + map);
                            }
                            component.SetAttribute("FolderName", "BVBS\\" + map);
                            component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                        }
                        component.Insert();

                    }

                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }

            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnBalkenSchuin_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList balkenSchuin = new ArrayList();
                    //string map = "";
                    ArrayList wapBalkenLS = new ArrayList();
                    // ArrayList wapBalkenLS = new ArrayList();
                    ArrayList wapBalkenDekkingSchuin = new ArrayList();

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null)
                        {
                            string naam = "";
                            part.GetReportProperty("NAME", ref naam);

                            if (naam == "BALK_SCHUIN") { balkenSchuin.Add(part); }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(balkenSchuin.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    foreach (TSM.Part export in balkenSchuin)
                    {
                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix == "LS")
                                {
                                    wapBalkenLS.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix == "LS")
                                {
                                    wapBalkenLS.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix == "D")
                                {
                                    wapBalkenDekkingSchuin.Add(bar);
                                }
                            }
                        }
                    }

                    //gefilterde staven
                    if (RadAlles.Checked)
                    {
                        modelObjectSelector.Select(wapBalkenDekkingSchuin);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\D2.abs");
                        component.Insert();

                        modelObjectSelector.Select(wapBalkenLS);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\L2.abs");
                        component.Insert();
                    }

                    if (RadPerFase.Checked)
                    {
                        //geselecteerde filteren per fase
                        List<int> faseList = new List<int>();

                        ArrayList wapBalkenDekkingS1 = new ArrayList();
                        ArrayList wapBalkenDekkingS2 = new ArrayList();
                        ArrayList wapBalkenDekkingS3 = new ArrayList();
                        ArrayList wapBalkenLS1 = new ArrayList();
                        ArrayList wapBalkenLS2 = new ArrayList();
                        ArrayList wapBalkenLS3 = new ArrayList();
                        // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                        int fase = 0;
                        foreach (TSM.SingleRebar itemSingle in wapBalkenDekkingSchuin)
                        {
                            itemSingle.GetReportProperty("PHASE", ref fase);

                            if (fase == 1) wapBalkenDekkingS1.Add(itemSingle);
                            if (fase == 2) wapBalkenDekkingS2.Add(itemSingle);
                            if (fase == 3) wapBalkenDekkingS3.Add(itemSingle);

                            // faseList.Add(fase);
                        }

                        foreach (var itemSingle in wapBalkenLS)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            var bar = itemSingle as TSM.SingleRebar;

                            if (bar != null)
                            {
                                bar.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalkenLS1.Add(itemSingle);
                                if (fase == 2) wapBalkenLS2.Add(itemSingle);
                                if (fase == 3) wapBalkenLS3.Add(itemSingle);
                            }

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalkenLS1.Add(itemSingle);
                                if (fase == 2) wapBalkenLS2.Add(itemSingle);
                                if (fase == 3) wapBalkenLS3.Add(itemSingle);
                            }
                        }



                        if (wapBalkenDekkingS1.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenDekkingS1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\D2.abs");
                            component.Insert();
                        }

                        if (wapBalkenDekkingS2.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenDekkingS2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\D2.abs");
                            component.Insert();
                        }

                        if (wapBalkenDekkingS3.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenDekkingS3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\D2.abs");
                            component.Insert();
                        }

                        //LR
                        if (wapBalkenLS1.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLS1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\L2.abs");
                            component.Insert();
                        }

                        if (wapBalkenLS2.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLS2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\L2.abs");
                            component.Insert();
                        }

                        if (wapBalkenLS3.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalkenLS3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\L2.abs");
                            component.Insert();
                        }
                    }


                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnPdf_Click(object sender, EventArgs e)
        {
            try
            {

                var model = new TSM.Model();

                if (!Directory.Exists("Plotfiles"))
                {
                    Directory.CreateDirectory("Plotfiles");
                }

                DrawingHandler drawingHandler = new DrawingHandler();

                DPMPrinterAttributes printAttributes = GetPrintAttributes();

                DrawingEnumerator drawingEnumerator = drawingHandler.GetDrawings();
                while (drawingEnumerator.MoveNext())
                {
                    Drawing currentDrawing = drawingEnumerator.Current as Drawing;
                    if (currentDrawing != null)
                    {
                        //Name is now with []. Should be K18-....
                        if (currentDrawing.GetType() == typeof(CastUnitDrawing) || currentDrawing.GetType() == typeof(GADrawing))
                        {
                            DrawingObjectEnumerator objectList = currentDrawing.GetSheet().GetAllObjects(typeof(Tekla.Structures.Drawing.ModelObject));
                            while (objectList.MoveNext())
                            {
                                Tekla.Structures.Drawing.ModelObject currentObject = objectList.Current as Tekla.Structures.Drawing.ModelObject;

                                if (currentObject != null)
                                {
                                    TSM.Part myPart = model.SelectModelObject(currentObject.ModelIdentifier) as TSM.Part;
                                    if (myPart != null)
                                    {
                                        string castUnitPos = "";
                                        myPart.GetReportProperty("CAST_UNIT_POS", ref castUnitPos);

                                        if (currentDrawing.GetType() == typeof(GADrawing) && CbPdfG.Checked)
                                        {

                                            printAttributes.OutputFileName = "Plotfiles//" + currentDrawing.Name + " -" + currentDrawing.Mark.Replace('[', ' ').Replace(']', ' ') + ".pdf";
                                            drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                        }
                                        if (currentDrawing.GetType() == typeof(CastUnitDrawing) && CbPdfC.Checked)
                                        {
                                            //A == vloer dan eerst de naam + pos
                                            if (castUnitPos.Contains("A"))
                                            {
                                                printAttributes.OutputFileName = "Plotfiles//" + currentDrawing.Name + " - " + castUnitPos + ".pdf";
                                                drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                            }
                                            else
                                            {
                                                printAttributes.OutputFileName = "Plotfiles//" + castUnitPos + " - " + currentDrawing.Name + ".pdf";
                                                drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                            }
                                        }

                                        break;
                                    }

                                }
                            }
                        }
                    }
                }

                drawingHandler.CloseActiveDrawing();

            }
            catch (Exception ex)
            {

                MessageBox.Show("Melding PDF: " + ex.ToString());
            }
        }


        private static DPMPrinterAttributes GetPrintAttributes()
        {
            DPMPrinterAttributes printAttributes = new DPMPrinterAttributes();
            printAttributes.ColorMode = DotPrintColor.Color;
            printAttributes.NumberOfCopies = 1;
            printAttributes.OpenFileWhenFinished = false;
            printAttributes.Orientation = DotPrintOrientationType.Landscape;
            printAttributes.OutputType = DotPrintOutputType.PDF;
            printAttributes.PaperSize = DotPrintPaperSize.A4;
            printAttributes.PrinterName = "Microsoft Print to PDF"; // Must use local PDF printer name
            printAttributes.PrintToMultipleSheet = DotPrintToMultipleSheet.Off;
            printAttributes.ScaleFactor = 1.0;
            printAttributes.ScalingMethod = DotPrintScalingType.Auto;
            return printAttributes;
        }

        private void BtnBalken3_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList balken3 = new ArrayList();
                    //string map = "";
                    ArrayList wapBalken3LR = new ArrayList();
                    // ArrayList wapBalkenLS = new ArrayList();
                    ArrayList wapBalken3Dekking = new ArrayList();

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null)
                        {
                            string naam = "";
                            part.GetReportProperty("NAME", ref naam);

                            if (naam == "3_SNEDIGE_BALK") { balken3.Add(part); }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(balken3.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    foreach (TSM.Part export in balken3)
                    {
                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix == "LR")
                                {
                                    wapBalken3LR.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix == "LR")
                                {
                                    wapBalken3LR.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix == "D")
                                {
                                    wapBalken3Dekking.Add(bar);
                                }
                            }
                        }
                    }

                    //gefilterde staven
                    if (RadAlles.Checked)
                    {
                        modelObjectSelector.Select(wapBalken3Dekking);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\D1.abs");
                        component.Insert();

                        modelObjectSelector.Select(wapBalken3LR);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\L1.abs");
                        component.Insert();
                    }

                    if (RadPerFase.Checked)
                    {
                        //geselecteerde filteren per fase
                        List<int> faseList = new List<int>();

                        ArrayList wapBalken3Dekking1 = new ArrayList();
                        ArrayList wapBalken3Dekking2 = new ArrayList();
                        ArrayList wapBalken3Dekking3 = new ArrayList();
                        ArrayList wapBalken3LR1 = new ArrayList();
                        ArrayList wapBalken3LR2 = new ArrayList();
                        ArrayList wapBalken3LR3 = new ArrayList();
                        // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                        int fase = 0;
                        foreach (TSM.SingleRebar itemSingle in wapBalken3Dekking)
                        {
                            itemSingle.GetReportProperty("PHASE", ref fase);

                            if (fase == 1) wapBalken3Dekking1.Add(itemSingle);
                            if (fase == 2) wapBalken3Dekking2.Add(itemSingle);
                            if (fase == 3) wapBalken3Dekking3.Add(itemSingle);

                            // faseList.Add(fase);
                        }

                        foreach (var itemSingle in wapBalken3LR)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            var bar = itemSingle as TSM.SingleRebar;

                            if (bar != null)
                            {
                                bar.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalken3LR1.Add(itemSingle);
                                if (fase == 2) wapBalken3LR2.Add(itemSingle);
                                if (fase == 3) wapBalken3LR3.Add(itemSingle);
                            }

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapBalken3LR1.Add(itemSingle);
                                if (fase == 2) wapBalken3LR2.Add(itemSingle);
                                if (fase == 3) wapBalken3LR3.Add(itemSingle);
                            }
                        }



                        if (wapBalken3Dekking1.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3Dekking1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\D3.abs");
                            component.Insert();
                        }

                        if (wapBalken3Dekking2.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3Dekking2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\D3.abs");
                            component.Insert();
                        }

                        if (wapBalken3Dekking3.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3Dekking3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\D3.abs");
                            component.Insert();
                        }

                        //LR
                        if (wapBalken3LR1.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3LR1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\L3.abs");
                            component.Insert();
                        }

                        if (wapBalken3LR2.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3LR2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\L3.abs");
                            component.Insert();
                        }

                        if (wapBalken3LR3.Count > 0)
                        {
                            modelObjectSelector.Select(wapBalken3LR3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\L3.abs");
                            component.Insert();
                        }
                    }


                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnIFC_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists("IFC"))
            {
                Directory.CreateDirectory("IFC");
            }

            string file = ".\\IFC\\out";
            if (File.Exists(file + ".ifc")) MessageBox.Show("Bestand bestaat al. Overschijven?");

            var ComponentInput = new TSM.ComponentInput();
            ComponentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
            var Comp = new TSM.Component(ComponentInput);
            Comp.Name = "ExportIFC";
            Comp.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
            Comp.LoadAttributesFromFile("standard");
            Comp.SetAttribute("CreateAll", 0);  // 0 to export selected objects
            Comp.SetAttribute("OutputFile", file);
            Comp.Insert();
            TSM.Operations.Operation.DisplayPrompt("IFC export gereed");

        }

        private void BtnNetSup_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList platen = new ArrayList();
                    //string map = "";
                    ArrayList wapPlaten = new ArrayList();
                    // ArrayList wapBalkenLS = new ArrayList();
                    ArrayList wapPlatenDekking = new ArrayList();

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null)
                        {
                            string naam = "";
                            part.GetReportProperty("NAME", ref naam);

                            if (naam == "BALK") { platen.Add(part); }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(platen.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    foreach (TSM.Part export in platen)
                    {
                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix == "T" || bars.NumberingSeries.Prefix == "M")
                                {
                                    wapPlaten.Add(bars);
                                }
                                if (bars.NumberingSeries.Prefix == "D")
                                {
                                    wapPlatenDekking.Add(bars);
                                }
                            }

                            var bar = child.Current as TSM.SingleRebar;
                            if (bar != null)
                            {
                                if (bar.NumberingSeries.Prefix == "T" || bar.NumberingSeries.Prefix == "M")
                                {
                                    wapPlaten.Add(bar);
                                }
                                if (bar.NumberingSeries.Prefix == "D")
                                {
                                    wapPlatenDekking.Add(bar);
                                }
                            }
                        }
                    }

                    //gefilterde staven
                    if (RadAlles.Checked)
                    {
                        modelObjectSelector.Select(wapPlatenDekking);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\D50.abs");
                        component.Insert();

                        modelObjectSelector.Select(wapPlaten);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\M50.abs");
                        component.Insert();
                    }

                    if (RadPerFase.Checked)
                    {
                        //geselecteerde filteren per fase
                        List<int> faseList = new List<int>();

                        ArrayList wapPlatenDekking1 = new ArrayList();
                        ArrayList wapPlatenDekking2 = new ArrayList();
                        ArrayList wapPlatenDekking3 = new ArrayList();
                        ArrayList wapPlaten1 = new ArrayList();
                        ArrayList wapPlaten2 = new ArrayList();
                        ArrayList wapPlaten3 = new ArrayList();
                        // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                        int fase = 0;
                        foreach (var itemSingle in wapPlatenDekking)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            var bar = itemSingle as TSM.SingleRebar;

                            if (bar != null)
                            {
                                bar.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPlatenDekking1.Add(itemSingle);
                                if (fase == 2) wapPlatenDekking2.Add(itemSingle);
                                if (fase == 3) wapPlatenDekking3.Add(itemSingle);
                            }

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPlatenDekking1.Add(itemSingle);
                                if (fase == 2) wapPlatenDekking2.Add(itemSingle);
                                if (fase == 3) wapPlatenDekking3.Add(itemSingle);
                            }

                            // faseList.Add(fase);
                        }

                        foreach (var itemSingle in wapPlaten)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            var bar = itemSingle as TSM.SingleRebar;

                            if (bar != null)
                            {
                                bar.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPlaten.Add(itemSingle);
                                if (fase == 2) wapPlaten.Add(itemSingle);
                                if (fase == 3) wapPlaten.Add(itemSingle);
                            }

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPlaten1.Add(itemSingle);
                                if (fase == 2) wapPlaten2.Add(itemSingle);
                                if (fase == 3) wapPlaten3.Add(itemSingle);
                            }
                        }



                        if (wapPlatenDekking1.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlatenDekking1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\D50.abs");
                            component.Insert();
                        }

                        if (wapPlatenDekking2.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlatenDekking2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\D50.abs");
                            component.Insert();
                        }

                        if (wapPlatenDekking3.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlatenDekking3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\D50.abs");
                            component.Insert();
                        }


                        if (wapPlaten1.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlaten1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\M50.abs");
                            component.Insert();
                        }

                        if (wapPlaten2.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlaten2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\M50.abs");
                            component.Insert();
                        }

                        if (wapPlaten3.Count > 0)
                        {
                            modelObjectSelector.Select(wapPlaten3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\M50.abs");
                            component.Insert();
                        }
                    }


                    TSM.Operations.Operation.DisplayPrompt("BVBS export gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnPoer_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }

            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                try
                {
                    var modelObjectSelector = new TSM.UI.ModelObjectSelector();
                    TSM.UI.ModelObjectSelector mos = new TSM.UI.ModelObjectSelector();
                    TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

                    ArrayList poeren = new ArrayList();
                    //string map = "";
                    ArrayList wapPoeren = new ArrayList();
                    // ArrayList wapBalkenLS = new ArrayList();
                    ArrayList wapPoerenDekking = new ArrayList();

                    while (moe.MoveNext())
                    {
                        var part = moe.Current as TSM.Part;

                        if (part != null)
                        {
                            string naam = "";
                            part.GetReportProperty("NAME", ref naam);

                            if (naam == "PLAAT_POER") { poeren.Add(part); }

                        }
                    }
                    TSM.Operations.Operation.DisplayPrompt(poeren.Count.ToString());

                    TSM.ComponentInput componentInput = new TSM.ComponentInput();
                    componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                    TSM.Component component = new TSM.Component(componentInput);
                    component.Name = "BVBSExport";
                    component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                    foreach (TSM.Part export in poeren)
                    {
                        TSM.ModelObjectEnumerator child = export.GetChildren();
                        while (child.MoveNext())
                        {
                            var bars = child.Current as TSM.RebarGroup;
                            if (bars != null)
                            {
                                if (bars.NumberingSeries.Prefix == "MPB" || bars.NumberingSeries.Prefix == "MPO" || bars.NumberingSeries.Prefix == "SP")
                                {
                                    wapPoeren.Add(bars);
                                }
                                if (bars.NumberingSeries.Prefix == "DP")
                                {
                                    wapPoerenDekking.Add(bars);
                                }
                                //enkele staaf uitgezet. Nog nodig voor de dekking en/of de supporten?
                            }

                        }
                    }

                    //gefilterde staven
                    if (RadAlles.Checked)
                    {
                        modelObjectSelector.Select(wapPoerenDekking);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\DP1.abs");
                        component.Insert();

                        modelObjectSelector.Select(wapPoeren);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("FileName", ".\\BVBS\\MP1.abs");
                        component.Insert();
                    }

                    if (RadPerFase.Checked)
                    {
                        //geselecteerde filteren per fase
                        List<int> faseList = new List<int>();

                        ArrayList wapPoerenDekking1 = new ArrayList();
                        ArrayList wapPoerenDekking2 = new ArrayList();
                        ArrayList wapPoerenDekking3 = new ArrayList();
                        ArrayList wapPoeren1 = new ArrayList();
                        ArrayList wapPoeren2 = new ArrayList();
                        ArrayList wapPoeren3 = new ArrayList();
                        // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                        int fase = 0;
                        foreach (var itemSingle in wapPoerenDekking)
                        {
                            var bars = itemSingle as TSM.RebarGroup;
                            //  var bar = itemSingle as TSM.SingleRebar;

                            //if (bar != null)
                            //{
                            //    bar.GetReportProperty("PHASE", ref fase);
                            //    if (fase == 1) wapPlatenDekking1.Add(itemSingle);
                            //    if (fase == 2) wapPlatenDekking2.Add(itemSingle);
                            //    if (fase == 3) wapPlatenDekking3.Add(itemSingle);
                            //}

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPoerenDekking1.Add(itemSingle);
                                if (fase == 2) wapPoerenDekking2.Add(itemSingle);
                                if (fase == 3) wapPoerenDekking3.Add(itemSingle);
                            }

                            // faseList.Add(fase);
                        }

                        foreach (var itemSingle in wapPoeren)
                        {
                            var bars = itemSingle as TSM.RebarGroup;

                            if (bars != null)
                            {
                                bars.GetReportProperty("PHASE", ref fase);
                                if (fase == 1) wapPoeren1.Add(itemSingle);
                                if (fase == 2) wapPoeren2.Add(itemSingle);
                                if (fase == 3) wapPoeren3.Add(itemSingle);
                            }
                        }



                        if (wapPoerenDekking1.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoerenDekking1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\DP1.abs");
                            component.Insert();
                        }

                        if (wapPoerenDekking2.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoerenDekking2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\DP1.abs");
                            component.Insert();
                        }

                        if (wapPoerenDekking3.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoerenDekking3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\DP1.abs");
                            component.Insert();
                        }


                        if (wapPoeren1.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoeren1);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 1")) { Directory.CreateDirectory("BVBS\\fase 1"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 1");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 1\\MP1.abs");
                            component.Insert();
                        }

                        if (wapPoeren2.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoeren2);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 2")) { Directory.CreateDirectory("BVBS\\fase 2"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 2");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 2\\MP1.abs");
                            component.Insert();
                        }

                        if (wapPoeren3.Count > 0)
                        {
                            modelObjectSelector.Select(wapPoeren3);
                            component.LoadAttributesFromFile("bs_standaard");
                            if (!Directory.Exists("BVBS\\fase 3")) { Directory.CreateDirectory("BVBS\\fase 3"); }
                            component.SetAttribute("FolderName", "BVBS\\fase 3");
                            component.SetAttribute("FileName", ".\\BVBS\\fase 3\\MP1.abs");
                            component.Insert();
                        }
                    }


                    TSM.Operations.Operation.DisplayPrompt("BVBS export Poeren gereed");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding BVBS Export: " + ex.ToString());
                }
            }
            else MessageBox.Show("Nummering is niet up-to-date");
        }

        private void BtnBVBSchecked_Click(object sender, EventArgs e)
        {
            if (TSM.Operations.Operation.IsNumberingUpToDateAll())
            {
                var model = new TSM.Model();
                var selectedObjects = new ArrayList();
                var mos = new TSM.UI.ModelObjectSelector();
                TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();
                while (moe.MoveNext())
                {
                    var part = moe.Current as TSM.Part;
                    if (part != null)
                    {
                        selectedObjects.Add(moe.Current);
                    }
                }

                if (CbKorven.Checked) BtnKorf_Click(sender, e);

                if (CbBalken.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnBalken_Click(sender, e);
                }

                if (CbBalkenD.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnBalkenD_Click(sender, e);
                }

                if (CbKorvenSchuin.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnKorvenSchuin_Click(sender, e);
                }

                if (CbBalkenSchuin.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnBalkenSchuin_Click(sender, e);
                }

                if (CbKorven3sn.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnKorf3_Click(sender, e);
                }

                if (CbBalken3sn.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnKorf3_Click(sender, e);
                }

                //if (CbNettenSup.Checked)
                //{
                //    mos.Select(selectedObjects);
                //    BtnNetSup_Click(sender, e);
                //}

                if (CbPoeren.Checked)
                {
                    mos.Select(selectedObjects);
                    BtnPoer_Click(sender, e);
                }

                TSM.Operations.Operation.DisplayPrompt("Alle aangevinkte BVBS exporten gereed");
            }
            else
            {
                MessageBox.Show("Nummering is niet up-to-date");

            }



        }

        private void BtnDrawings_Click(object sender, EventArgs e)
        {
            try
            {
                TSM.Operations.Operation.RunMacro("#BS_Tekeningen.cs");
                //akit.Callback("acmd_create_dim_general_assembly_drawing", "IncludeView", "View_01 window_1");

                ////Create G drawing for Haarspelden
                //akit.ListSelect("Create GA-drawing", "dia_view_name_list", new string[] { " 3d" });
                //akit.PushButton("Pushbutton", "Create GA-drawing");
                //akit.PushButton("gr_gdraw_get", "gdraw_dial");
                //akit.ValueChange("gdraw_dial", "gr_gdraw_get_menu", "#bs_haarspelden");
                //akit.PushButton("gr_gdraw_get", "gdraw_dial");
                //akit.PushButton("gr_gdraw_ok", "gdraw_dial");
                //akit.PushButton("Pushbutton_127", "Create GA-drawing");

                ////Create G drawing for Balken
                //akit.PushButton("Pushbutton", "Create GA-drawing");
                //akit.PushButton("gr_gdraw_get", "gdraw_dial");
                //akit.ValueChange("gdraw_dial", "gr_gdraw_get_menu", "standard");
                //akit.PushButton("gr_gdraw_get", "gdraw_dial");
                //akit.PushButton("gr_gdraw_ok", "gdraw_dial");
                //akit.PushButton("Pushbutton_127", "Create GA-drawing");
                //akit.PushButton("Pushbutton_128", "Create GA-drawing");



                ////Create the rest of the drawings
                //CreateDrawings<TSM.Beam>("#korf_filter", "KORF", "#bs_korf");

                //CreateDrawings<TSM.Beam>("#korf_3-snedig_filter", "3_SNEDIGE_KORF", "#bs_korf");  //deze regel toegevoegd JW 190712

                //CreateDrawings<TSM.Beam>("#hrsp_filter", "KORF_HRSP", "#bs_hrsp");

                //CreateDrawings<TSM.Beam>("#korf_schuin_filter", "KORF_SCHUIN", "#bs_korf_schuin");

                //CreateDrawings<TSM.ContourPlate>("#plaat_filter", "PLAAT", "#bs_vloeren", "VLOER");

                //CreateDrawings<TSM.ContourPlate>("#matopmaat_filter", "MATOPMAAT", "#bs_mat", "MATOPMAAT");
            }

            catch (Exception ex)
            {
                // MessageBox.Show("Melding: " + ex.ToString());
            }

        }


        public static void CreateDrawings<T>(string filter, string name, string setting, string drawingName = "BALK - ") where T : TSM.Part
        {
            var model = new TSM.Model();
            TSM.ModelObjectEnumerator moe = model.GetModelObjectSelector().GetObjectsByFilterName(filter);

            while (moe.MoveNext())
            {
                T part = moe.Current as T;

                if (part != null && part.Name == name)
                {
                    var myDrawing = new CastUnitDrawing(part.GetAssembly().Identifier, setting);

                    string mainPartCastUnitPos = "";
                    ((T)part.GetAssembly().GetMainPart()).GetReportProperty("CAST_UNIT.CAST_UNIT.CAST_UNIT_POS", ref mainPartCastUnitPos);

                    myDrawing.Name = drawingName + mainPartCastUnitPos;

                    myDrawing.Insert();
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }

        private void BtnBalkenD_Click(object sender, EventArgs e)
        {

        }
    }
}