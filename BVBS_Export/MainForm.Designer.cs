namespace BVBS_Export
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Obsolete]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabProject = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.FullLengthButton = new System.Windows.Forms.Button();
            this.FullLengthTextBox = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.CoverInfo = new System.Windows.Forms.Button();
            this.CoverGroupBox = new System.Windows.Forms.GroupBox();
            this.CoverInsideComboBox = new System.Windows.Forms.ComboBox();
            this.CoverInsideCheckBox = new System.Windows.Forms.CheckBox();
            this.CoverEndComboBox = new System.Windows.Forms.ComboBox();
            this.CoverEndCheckBox = new System.Windows.Forms.CheckBox();
            this.CoverSidesComboBox = new System.Windows.Forms.ComboBox();
            this.CoverSidesCheckBox = new System.Windows.Forms.CheckBox();
            this.CoverStartComboBox = new System.Windows.Forms.ComboBox();
            this.CoverStartCheckBox = new System.Windows.Forms.CheckBox();
            this.CoverTopComboBox = new System.Windows.Forms.ComboBox();
            this.CoverBottomComboBox = new System.Windows.Forms.ComboBox();
            this.CoverTopCheckBox = new System.Windows.Forms.CheckBox();
            this.CoverBottomCheckBox = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CopyTplButton = new System.Windows.Forms.Button();
            this.CustomercheckBox = new System.Windows.Forms.CheckBox();
            this.CustomerComboBox = new System.Windows.Forms.ComboBox();
            this.NummerenButton = new System.Windows.Forms.Button();
            this.Tradional = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.G0 = new System.Windows.Forms.CheckBox();
            this.G10 = new System.Windows.Forms.CheckBox();
            this.G9 = new System.Windows.Forms.CheckBox();
            this.G8 = new System.Windows.Forms.CheckBox();
            this.G7 = new System.Windows.Forms.CheckBox();
            this.SwitchSelectionButton = new System.Windows.Forms.Button();
            this.BtnDrawingsNew = new System.Windows.Forms.Button();
            this.G6 = new System.Windows.Forms.CheckBox();
            this.G1 = new System.Windows.Forms.CheckBox();
            this.G5 = new System.Windows.Forms.CheckBox();
            this.G2 = new System.Windows.Forms.CheckBox();
            this.G4 = new System.Windows.Forms.CheckBox();
            this.G3 = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.G403 = new System.Windows.Forms.CheckBox();
            this.G402 = new System.Windows.Forms.CheckBox();
            this.BtnGtekStroken = new System.Windows.Forms.Button();
            this.G401 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.G308 = new System.Windows.Forms.CheckBox();
            this.G307 = new System.Windows.Forms.CheckBox();
            this.G306 = new System.Windows.Forms.CheckBox();
            this.G305 = new System.Windows.Forms.CheckBox();
            this.G304 = new System.Windows.Forms.CheckBox();
            this.G303 = new System.Windows.Forms.CheckBox();
            this.G302 = new System.Windows.Forms.CheckBox();
            this.G301 = new System.Windows.Forms.CheckBox();
            this.G300 = new System.Windows.Forms.CheckBox();
            this.BtnGtekBreedplaten = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.G210 = new System.Windows.Forms.CheckBox();
            this.G209 = new System.Windows.Forms.CheckBox();
            this.G208 = new System.Windows.Forms.CheckBox();
            this.G206 = new System.Windows.Forms.CheckBox();
            this.G207 = new System.Windows.Forms.CheckBox();
            this.G205 = new System.Windows.Forms.CheckBox();
            this.btnGtekKelderwanden = new System.Windows.Forms.Button();
            this.G204 = new System.Windows.Forms.CheckBox();
            this.G200 = new System.Windows.Forms.CheckBox();
            this.G201 = new System.Windows.Forms.CheckBox();
            this.G203 = new System.Windows.Forms.CheckBox();
            this.G202 = new System.Windows.Forms.CheckBox();
            this.m3GroupBox = new System.Windows.Forms.GroupBox();
            this.btnGtekm3 = new System.Windows.Forms.Button();
            this.G500 = new System.Windows.Forms.CheckBox();
            this.groupBoxTraditioneel = new System.Windows.Forms.GroupBox();
            this.G109 = new System.Windows.Forms.CheckBox();
            this.G108 = new System.Windows.Forms.CheckBox();
            this.G107 = new System.Windows.Forms.CheckBox();
            this.G106 = new System.Windows.Forms.CheckBox();
            this.G105 = new System.Windows.Forms.CheckBox();
            this.G104 = new System.Windows.Forms.CheckBox();
            this.G100 = new System.Windows.Forms.CheckBox();
            this.btnGtekTraditioneel = new System.Windows.Forms.Button();
            this.G103 = new System.Windows.Forms.CheckBox();
            this.G102 = new System.Windows.Forms.CheckBox();
            this.G101 = new System.Windows.Forms.CheckBox();
            this.tabCUDrawings = new System.Windows.Forms.TabPage();
            this.DrawingStatus = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.BtnDrawingBalkLos = new System.Windows.Forms.Button();
            this.WizardButton = new System.Windows.Forms.Button();
            this.BtnDrawingsBalk = new System.Windows.Forms.Button();
            this.TabExport = new System.Windows.Forms.TabPage();
            this.EmptyPhaseButton = new System.Windows.Forms.Button();
            this.TxRadJsonbutton = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ExportButton = new System.Windows.Forms.Button();
            this.JsonGroupBox = new System.Windows.Forms.GroupBox();
            this.CustomMeshButton = new System.Windows.Forms.Button();
            this.JsonBijlegButton = new System.Windows.Forms.Button();
            this.KorbButton = new System.Windows.Forms.Button();
            this.JsonKorvenButton = new System.Windows.Forms.Button();
            this.checkBoxResetJson = new System.Windows.Forms.CheckBox();
            this.JsonFloorButton = new System.Windows.Forms.Button();
            this.checkBoxVisualizeJson = new System.Windows.Forms.CheckBox();
            this.RadJsonPerFase = new System.Windows.Forms.RadioButton();
            this.RadJsonAlles = new System.Windows.Forms.RadioButton();
            this.VMeshButton = new System.Windows.Forms.Button();
            this.StrMeshButton = new System.Windows.Forms.Button();
            this.JsonHrspButton = new System.Windows.Forms.Button();
            this.JsonStekrekButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Export = new System.Windows.Forms.GroupBox();
            this.RadPerFase = new System.Windows.Forms.RadioButton();
            this.RadAlles = new System.Windows.Forms.RadioButton();
            this.btnBVBS = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CheckBoxSetKleuren = new System.Windows.Forms.CheckBox();
            this.checkBoxReset = new System.Windows.Forms.CheckBox();
            this.checkBoxVisualize = new System.Windows.Forms.CheckBox();
            this.BtnIFC = new System.Windows.Forms.Button();
            this.groupBoxPDF = new System.Windows.Forms.GroupBox();
            this.BtnMergePlotfiles = new System.Windows.Forms.Button();
            this.CbPdfG = new System.Windows.Forms.CheckBox();
            this.CbPdfC = new System.Windows.Forms.CheckBox();
            this.BtnPdf = new System.Windows.Forms.Button();
            this.TxRadbutton = new System.Windows.Forms.TextBox();
            this.lblNummering = new System.Windows.Forms.Label();
            this.TxKlasse = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.BVBS = new System.Windows.Forms.TabPage();
            this.PutButton = new System.Windows.Forms.Button();
            this.RondeKolomButton = new System.Windows.Forms.Button();
            this.ZrackButton = new System.Windows.Forms.Button();
            this.PonsButton = new System.Windows.Forms.Button();
            this.BvbsFundDoorvoer = new System.Windows.Forms.Button();
            this.CbKorven4sn = new System.Windows.Forms.CheckBox();
            this.btnKorf4 = new System.Windows.Forms.Button();
            this.BtnBalk = new System.Windows.Forms.Button();
            this.CbHrspLos = new System.Windows.Forms.CheckBox();
            this.HrspLosButton = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBoxLosseKorf = new System.Windows.Forms.CheckBox();
            this.CbStekrek = new System.Windows.Forms.CheckBox();
            this.btnStekrek = new System.Windows.Forms.Button();
            this.CbZ = new System.Windows.Forms.CheckBox();
            this.BtnZbars = new System.Windows.Forms.Button();
            this.CbBalken3 = new System.Windows.Forms.CheckBox();
            this.btnDekking3snedig = new System.Windows.Forms.Button();
            this.CbBalkenS = new System.Windows.Forms.CheckBox();
            this.btnDekkingSchuin = new System.Windows.Forms.Button();
            this.CbBalkenD = new System.Windows.Forms.CheckBox();
            this.btnBalkenD = new System.Windows.Forms.Button();
            this.CbPoeren = new System.Windows.Forms.CheckBox();
            this.CbMatOpMaat = new System.Windows.Forms.CheckBox();
            this.CbKorvenHrsp = new System.Windows.Forms.CheckBox();
            this.CbNettenSup = new System.Windows.Forms.CheckBox();
            this.CbKorvenLos = new System.Windows.Forms.CheckBox();
            this.CbBalken3sn = new System.Windows.Forms.CheckBox();
            this.CbKorven3sn = new System.Windows.Forms.CheckBox();
            this.CbBalkenSchuin = new System.Windows.Forms.CheckBox();
            this.CbKorvenSchuin = new System.Windows.Forms.CheckBox();
            this.CbBalken = new System.Windows.Forms.CheckBox();
            this.CbKorven = new System.Windows.Forms.CheckBox();
            this.BtnPoer = new System.Windows.Forms.Button();
            this.btnBalken3 = new System.Windows.Forms.Button();
            this.btnKorf3 = new System.Windows.Forms.Button();
            this.btnMatOpMaat = new System.Windows.Forms.Button();
            this.btnNetSup = new System.Windows.Forms.Button();
            this.btnKorfHrsp = new System.Windows.Forms.Button();
            this.btnKorfLos = new System.Windows.Forms.Button();
            this.btnBalkenSchuin = new System.Windows.Forms.Button();
            this.btnBalken = new System.Windows.Forms.Button();
            this.btnKorvenSchuin = new System.Windows.Forms.Button();
            this.btnKorf = new System.Windows.Forms.Button();
            this.MontageExtra = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.piece_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sid_NL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.GetJsonButton = new System.Windows.Forms.Button();
            this.Opschonen = new System.Windows.Forms.TabPage();
            this.checkBoxADrawings = new System.Windows.Forms.CheckBox();
            this.labelRemoved = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.checkBoxGaDrawings = new System.Windows.Forms.CheckBox();
            this.checkBoxSObjGrp = new System.Windows.Forms.CheckBox();
            this.checkBoxCbm = new System.Windows.Forms.CheckBox();
            this.checkBoxLay = new System.Windows.Forms.CheckBox();
            this.buttonCleanUp = new System.Windows.Forms.Button();
            this.buttonVersion = new System.Windows.Forms.Button();
            this.CurrentVersionLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Info = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.PdfModifyButton = new System.Windows.Forms.Button();
            this.lblDatum = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVersie = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.saveLoad2 = new Tekla.Structures.Dialog.UIControls.SaveLoad();
            this.tableLayoutPanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabProject.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.CoverGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Tradional.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.m3GroupBox.SuspendLayout();
            this.groupBoxTraditioneel.SuspendLayout();
            this.tabCUDrawings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.TabExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.JsonGroupBox.SuspendLayout();
            this.Export.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBoxPDF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.BVBS.SuspendLayout();
            this.MontageExtra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Opschonen.SuspendLayout();
            this.Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel, null);
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel, null);
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.tabControl, 0, 1);
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 40, 4, 4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(832, 610);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // tabControl
            // 
            this.structuresExtender.SetAttributeName(this.tabControl, null);
            this.structuresExtender.SetAttributeTypeName(this.tabControl, null);
            this.structuresExtender.SetBindPropertyName(this.tabControl, null);
            this.tabControl.Controls.Add(this.tabProject);
            this.tabControl.Controls.Add(this.Tradional);
            this.tabControl.Controls.Add(this.tabCUDrawings);
            this.tabControl.Controls.Add(this.TabExport);
            this.tabControl.Controls.Add(this.BVBS);
            this.tabControl.Controls.Add(this.MontageExtra);
            this.tabControl.Controls.Add(this.Opschonen);
            this.tabControl.Controls.Add(this.Info);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(4, 14);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4, 14, 4, 4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(824, 592);
            this.tabControl.TabIndex = 18;
            this.tabControl.Click += new System.EventHandler(this.tabControl_Click);
            // 
            // tabProject
            // 
            this.structuresExtender.SetAttributeName(this.tabProject, null);
            this.structuresExtender.SetAttributeTypeName(this.tabProject, null);
            this.tabProject.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.tabProject, null);
            this.tabProject.Controls.Add(this.groupBox7);
            this.tabProject.Controls.Add(this.label7);
            this.tabProject.Controls.Add(this.label6);
            this.tabProject.Controls.Add(this.button2);
            this.tabProject.Controls.Add(this.CoverInfo);
            this.tabProject.Controls.Add(this.CoverGroupBox);
            this.tabProject.Controls.Add(this.pictureBox1);
            this.tabProject.Controls.Add(this.OkButton);
            this.tabProject.Controls.Add(this.CopyTplButton);
            this.tabProject.Controls.Add(this.CustomercheckBox);
            this.tabProject.Controls.Add(this.CustomerComboBox);
            this.tabProject.Controls.Add(this.NummerenButton);
            this.tabProject.Location = new System.Drawing.Point(4, 22);
            this.tabProject.Name = "tabProject";
            this.tabProject.Size = new System.Drawing.Size(816, 566);
            this.tabProject.TabIndex = 8;
            this.tabProject.Text = "Project Intake";
            this.tabProject.UseVisualStyleBackColor = true;
            this.tabProject.Click += new System.EventHandler(this.tabProject_Click);
            // 
            // groupBox7
            // 
            this.structuresExtender.SetAttributeName(this.groupBox7, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox7, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox7, null);
            this.groupBox7.Controls.Add(this.FullLengthButton);
            this.groupBox7.Controls.Add(this.FullLengthTextBox);
            this.groupBox7.Controls.Add(this.checkBox2);
            this.groupBox7.Location = new System.Drawing.Point(40, 384);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(338, 62);
            this.groupBox7.TabIndex = 84;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Project eigenschappen";
            // 
            // FullLengthButton
            // 
            this.structuresExtender.SetAttributeName(this.FullLengthButton, null);
            this.structuresExtender.SetAttributeTypeName(this.FullLengthButton, null);
            this.structuresExtender.SetBindPropertyName(this.FullLengthButton, null);
            this.FullLengthButton.Location = new System.Drawing.Point(271, 28);
            this.FullLengthButton.Name = "FullLengthButton";
            this.FullLengthButton.Size = new System.Drawing.Size(57, 25);
            this.FullLengthButton.TabIndex = 82;
            this.FullLengthButton.Text = "Wijzig";
            this.FullLengthButton.UseVisualStyleBackColor = true;
            this.FullLengthButton.Click += new System.EventHandler(this.FullLengthButton_Click);
            // 
            // FullLengthTextBox
            // 
            this.structuresExtender.SetAttributeName(this.FullLengthTextBox, "FullLength");
            this.structuresExtender.SetAttributeTypeName(this.FullLengthTextBox, "Double");
            this.structuresExtender.SetBindPropertyName(this.FullLengthTextBox, null);
            this.FullLengthTextBox.Location = new System.Drawing.Point(178, 30);
            this.FullLengthTextBox.Name = "FullLengthTextBox";
            this.FullLengthTextBox.Size = new System.Drawing.Size(67, 20);
            this.FullLengthTextBox.TabIndex = 81;
            // 
            // checkBox2
            // 
            this.structuresExtender.SetAttributeName(this.checkBox2, "FullLength");
            this.structuresExtender.SetAttributeTypeName(this.checkBox2, "Boolean");
            this.checkBox2.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox2, null);
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.checkBox2, true);
            this.checkBox2.Location = new System.Drawing.Point(20, 32);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(127, 17);
            this.checkBox2.TabIndex = 80;
            this.checkBox2.Text = "Volledige lengte staaf";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.structuresExtender.SetAttributeName(this.label7, null);
            this.structuresExtender.SetAttributeTypeName(this.label7, null);
            this.label7.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label7, null);
            this.label7.Location = new System.Drawing.Point(727, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 83;
            this.label7.Text = "fases.xlsx";
            // 
            // label6
            // 
            this.structuresExtender.SetAttributeName(this.label6, null);
            this.structuresExtender.SetAttributeTypeName(this.label6, null);
            this.label6.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label6, null);
            this.label6.Location = new System.Drawing.Point(727, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 82;
            this.label6.Text = "Tutorial.docx";
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.structuresExtender.SetAttributeName(this.button2, null);
            this.structuresExtender.SetAttributeTypeName(this.button2, null);
            this.button2.AutoSize = true;
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.structuresExtender.SetBindPropertyName(this.button2, null);
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Image = global::BS_Export.Properties.Resources.i20green;
            this.button2.Location = new System.Drawing.Point(700, 46);
            this.button2.Margin = new System.Windows.Forms.Padding(1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 26);
            this.button2.TabIndex = 81;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CoverInfo
            // 
            this.CoverInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.structuresExtender.SetAttributeName(this.CoverInfo, null);
            this.structuresExtender.SetAttributeTypeName(this.CoverInfo, null);
            this.CoverInfo.AutoSize = true;
            this.CoverInfo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CoverInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.structuresExtender.SetBindPropertyName(this.CoverInfo, null);
            this.CoverInfo.FlatAppearance.BorderSize = 0;
            this.CoverInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CoverInfo.ForeColor = System.Drawing.Color.Transparent;
            this.CoverInfo.Image = global::BS_Export.Properties.Resources.i20;
            this.CoverInfo.Location = new System.Drawing.Point(700, 15);
            this.CoverInfo.Margin = new System.Windows.Forms.Padding(1);
            this.CoverInfo.Name = "CoverInfo";
            this.CoverInfo.Size = new System.Drawing.Size(26, 26);
            this.CoverInfo.TabIndex = 80;
            this.CoverInfo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CoverInfo.UseVisualStyleBackColor = true;
            this.CoverInfo.Click += new System.EventHandler(this.CoverInfo_Click);
            // 
            // CoverGroupBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverGroupBox, null);
            this.structuresExtender.SetAttributeTypeName(this.CoverGroupBox, null);
            this.structuresExtender.SetBindPropertyName(this.CoverGroupBox, null);
            this.CoverGroupBox.Controls.Add(this.CoverInsideComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverInsideCheckBox);
            this.CoverGroupBox.Controls.Add(this.CoverEndComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverEndCheckBox);
            this.CoverGroupBox.Controls.Add(this.CoverSidesComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverSidesCheckBox);
            this.CoverGroupBox.Controls.Add(this.CoverStartComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverStartCheckBox);
            this.CoverGroupBox.Controls.Add(this.CoverTopComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverBottomComboBox);
            this.CoverGroupBox.Controls.Add(this.CoverTopCheckBox);
            this.CoverGroupBox.Controls.Add(this.CoverBottomCheckBox);
            this.CoverGroupBox.Location = new System.Drawing.Point(40, 232);
            this.CoverGroupBox.Name = "CoverGroupBox";
            this.CoverGroupBox.Size = new System.Drawing.Size(339, 118);
            this.CoverGroupBox.TabIndex = 69;
            this.CoverGroupBox.TabStop = false;
            this.CoverGroupBox.Text = "Dekking";
            // 
            // CoverInsideComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverInsideComboBox, "CoverInside");
            this.structuresExtender.SetAttributeTypeName(this.CoverInsideComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverInsideComboBox, "Text");
            this.CoverInsideComboBox.FormattingEnabled = true;
            this.CoverInsideComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverInsideComboBox.Location = new System.Drawing.Point(525, 60);
            this.CoverInsideComboBox.Name = "CoverInsideComboBox";
            this.CoverInsideComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverInsideComboBox.TabIndex = 79;
            this.CoverInsideComboBox.Text = "25";
            this.CoverInsideComboBox.Visible = false;
            // 
            // CoverInsideCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverInsideCheckBox, "CoverInside");
            this.structuresExtender.SetAttributeTypeName(this.CoverInsideCheckBox, "Boolean");
            this.CoverInsideCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverInsideCheckBox, null);
            this.CoverInsideCheckBox.Checked = true;
            this.CoverInsideCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverInsideCheckBox, true);
            this.CoverInsideCheckBox.Location = new System.Drawing.Point(525, 34);
            this.CoverInsideCheckBox.Name = "CoverInsideCheckBox";
            this.CoverInsideCheckBox.Size = new System.Drawing.Size(69, 17);
            this.CoverInsideCheckBox.TabIndex = 78;
            this.CoverInsideCheckBox.Text = "Inwendig";
            this.CoverInsideCheckBox.UseVisualStyleBackColor = true;
            this.CoverInsideCheckBox.Visible = false;
            // 
            // CoverEndComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverEndComboBox, "CoverEnd");
            this.structuresExtender.SetAttributeTypeName(this.CoverEndComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverEndComboBox, "Text");
            this.CoverEndComboBox.FormattingEnabled = true;
            this.CoverEndComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverEndComboBox.Location = new System.Drawing.Point(435, 60);
            this.CoverEndComboBox.Name = "CoverEndComboBox";
            this.CoverEndComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverEndComboBox.TabIndex = 77;
            this.CoverEndComboBox.Text = "25";
            this.CoverEndComboBox.Visible = false;
            // 
            // CoverEndCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverEndCheckBox, "CoverEnd");
            this.structuresExtender.SetAttributeTypeName(this.CoverEndCheckBox, "Boolean");
            this.CoverEndCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverEndCheckBox, null);
            this.CoverEndCheckBox.Checked = true;
            this.CoverEndCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverEndCheckBox, true);
            this.CoverEndCheckBox.Location = new System.Drawing.Point(435, 34);
            this.CoverEndCheckBox.Name = "CoverEndCheckBox";
            this.CoverEndCheckBox.Size = new System.Drawing.Size(69, 17);
            this.CoverEndCheckBox.TabIndex = 76;
            this.CoverEndCheckBox.Text = "Kop Eind";
            this.CoverEndCheckBox.UseVisualStyleBackColor = true;
            this.CoverEndCheckBox.Visible = false;
            // 
            // CoverSidesComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverSidesComboBox, "CoverSides");
            this.structuresExtender.SetAttributeTypeName(this.CoverSidesComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverSidesComboBox, "Text");
            this.CoverSidesComboBox.FormattingEnabled = true;
            this.CoverSidesComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverSidesComboBox.Location = new System.Drawing.Point(200, 60);
            this.CoverSidesComboBox.Name = "CoverSidesComboBox";
            this.CoverSidesComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverSidesComboBox.TabIndex = 75;
            this.CoverSidesComboBox.Text = "25";
            this.CoverSidesComboBox.SelectedIndexChanged += new System.EventHandler(this.CoverSidesComboBox_SelectedIndexChanged);
            // 
            // CoverSidesCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverSidesCheckBox, "CoverSides");
            this.structuresExtender.SetAttributeTypeName(this.CoverSidesCheckBox, "Boolean");
            this.CoverSidesCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverSidesCheckBox, null);
            this.CoverSidesCheckBox.Checked = true;
            this.CoverSidesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverSidesCheckBox, true);
            this.CoverSidesCheckBox.Location = new System.Drawing.Point(200, 34);
            this.CoverSidesCheckBox.Name = "CoverSidesCheckBox";
            this.CoverSidesCheckBox.Size = new System.Drawing.Size(55, 17);
            this.CoverSidesCheckBox.TabIndex = 74;
            this.CoverSidesCheckBox.Text = "Zijden";
            this.CoverSidesCheckBox.UseVisualStyleBackColor = true;
            // 
            // CoverStartComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverStartComboBox, "CoverStart");
            this.structuresExtender.SetAttributeTypeName(this.CoverStartComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverStartComboBox, "Text");
            this.CoverStartComboBox.FormattingEnabled = true;
            this.CoverStartComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverStartComboBox.Location = new System.Drawing.Point(345, 60);
            this.CoverStartComboBox.Name = "CoverStartComboBox";
            this.CoverStartComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverStartComboBox.TabIndex = 73;
            this.CoverStartComboBox.Text = "25";
            this.CoverStartComboBox.Visible = false;
            // 
            // CoverStartCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverStartCheckBox, "CoverStart");
            this.structuresExtender.SetAttributeTypeName(this.CoverStartCheckBox, "Boolean");
            this.CoverStartCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverStartCheckBox, null);
            this.CoverStartCheckBox.Checked = true;
            this.CoverStartCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverStartCheckBox, true);
            this.CoverStartCheckBox.Location = new System.Drawing.Point(345, 34);
            this.CoverStartCheckBox.Name = "CoverStartCheckBox";
            this.CoverStartCheckBox.Size = new System.Drawing.Size(70, 17);
            this.CoverStartCheckBox.TabIndex = 72;
            this.CoverStartCheckBox.Text = "Kop Start";
            this.CoverStartCheckBox.UseVisualStyleBackColor = true;
            this.CoverStartCheckBox.Visible = false;
            // 
            // CoverTopComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverTopComboBox, "CoverTop");
            this.structuresExtender.SetAttributeTypeName(this.CoverTopComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverTopComboBox, "Text");
            this.CoverTopComboBox.FormattingEnabled = true;
            this.CoverTopComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverTopComboBox.Location = new System.Drawing.Point(110, 60);
            this.CoverTopComboBox.Name = "CoverTopComboBox";
            this.CoverTopComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverTopComboBox.TabIndex = 71;
            this.CoverTopComboBox.Text = "25";
            this.CoverTopComboBox.SelectedIndexChanged += new System.EventHandler(this.CoverTopComboBox_SelectedIndexChanged);
            // 
            // CoverBottomComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverBottomComboBox, "CoverBottom");
            this.structuresExtender.SetAttributeTypeName(this.CoverBottomComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CoverBottomComboBox, "Text");
            this.CoverBottomComboBox.FormattingEnabled = true;
            this.CoverBottomComboBox.Items.AddRange(new object[] {
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.CoverBottomComboBox.Location = new System.Drawing.Point(20, 60);
            this.CoverBottomComboBox.Name = "CoverBottomComboBox";
            this.CoverBottomComboBox.Size = new System.Drawing.Size(64, 21);
            this.CoverBottomComboBox.TabIndex = 70;
            this.CoverBottomComboBox.Text = "25";
            this.CoverBottomComboBox.SelectedIndexChanged += new System.EventHandler(this.CoverBottomComboBox_SelectedIndexChanged);
            // 
            // CoverTopCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverTopCheckBox, "CoverTop");
            this.structuresExtender.SetAttributeTypeName(this.CoverTopCheckBox, "Boolean");
            this.CoverTopCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverTopCheckBox, null);
            this.CoverTopCheckBox.Checked = true;
            this.CoverTopCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverTopCheckBox, true);
            this.CoverTopCheckBox.Location = new System.Drawing.Point(110, 34);
            this.CoverTopCheckBox.Name = "CoverTopCheckBox";
            this.CoverTopCheckBox.Size = new System.Drawing.Size(57, 17);
            this.CoverTopCheckBox.TabIndex = 68;
            this.CoverTopCheckBox.Text = "Boven";
            this.CoverTopCheckBox.UseVisualStyleBackColor = true;
            // 
            // CoverBottomCheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CoverBottomCheckBox, "CoverBottom");
            this.structuresExtender.SetAttributeTypeName(this.CoverBottomCheckBox, "Boolean");
            this.CoverBottomCheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CoverBottomCheckBox, null);
            this.CoverBottomCheckBox.Checked = true;
            this.CoverBottomCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CoverBottomCheckBox, true);
            this.CoverBottomCheckBox.Location = new System.Drawing.Point(20, 34);
            this.CoverBottomCheckBox.Name = "CoverBottomCheckBox";
            this.CoverBottomCheckBox.Size = new System.Drawing.Size(55, 17);
            this.CoverBottomCheckBox.TabIndex = 67;
            this.CoverBottomCheckBox.Text = "Onder";
            this.CoverBottomCheckBox.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.structuresExtender.SetAttributeName(this.pictureBox1, null);
            this.structuresExtender.SetAttributeTypeName(this.pictureBox1, null);
            this.structuresExtender.SetBindPropertyName(this.pictureBox1, null);
            this.pictureBox1.Image = global::BS_Export.Properties.Resources.logo_bs_copy;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(550, 422);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(274, 152);
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // OkButton
            // 
            this.structuresExtender.SetAttributeName(this.OkButton, null);
            this.structuresExtender.SetAttributeTypeName(this.OkButton, null);
            this.structuresExtender.SetBindPropertyName(this.OkButton, null);
            this.OkButton.Location = new System.Drawing.Point(4, 532);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(69, 28);
            this.OkButton.TabIndex = 65;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CopyTplButton
            // 
            this.structuresExtender.SetAttributeName(this.CopyTplButton, null);
            this.structuresExtender.SetAttributeTypeName(this.CopyTplButton, null);
            this.structuresExtender.SetBindPropertyName(this.CopyTplButton, null);
            this.CopyTplButton.Location = new System.Drawing.Point(354, 57);
            this.CopyTplButton.Name = "CopyTplButton";
            this.CopyTplButton.Size = new System.Drawing.Size(124, 29);
            this.CopyTplButton.TabIndex = 64;
            this.CopyTplButton.TabStop = false;
            this.CopyTplButton.Text = "Kopieer logo";
            this.CopyTplButton.UseVisualStyleBackColor = true;
            this.CopyTplButton.Visible = false;
            this.CopyTplButton.Click += new System.EventHandler(this.CopyTplButton_Click);
            // 
            // CustomercheckBox
            // 
            this.structuresExtender.SetAttributeName(this.CustomercheckBox, "Customer");
            this.structuresExtender.SetAttributeTypeName(this.CustomercheckBox, "Boolean");
            this.CustomercheckBox.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CustomercheckBox, null);
            this.CustomercheckBox.Checked = true;
            this.CustomercheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.structuresExtender.SetIsFilter(this.CustomercheckBox, true);
            this.CustomercheckBox.Location = new System.Drawing.Point(40, 62);
            this.CustomercheckBox.Name = "CustomercheckBox";
            this.CustomercheckBox.Size = new System.Drawing.Size(53, 17);
            this.CustomercheckBox.TabIndex = 63;
            this.CustomercheckBox.Text = "Klant:";
            this.CustomercheckBox.UseVisualStyleBackColor = true;
            // 
            // CustomerComboBox
            // 
            this.structuresExtender.SetAttributeName(this.CustomerComboBox, "Customer");
            this.structuresExtender.SetAttributeTypeName(this.CustomerComboBox, "String");
            this.structuresExtender.SetBindPropertyName(this.CustomerComboBox, "Text");
            this.CustomerComboBox.FormattingEnabled = true;
            this.CustomerComboBox.Items.AddRange(new object[] {
            "-- Selecteer... --"});
            this.CustomerComboBox.Location = new System.Drawing.Point(131, 60);
            this.CustomerComboBox.Name = "CustomerComboBox";
            this.CustomerComboBox.Size = new System.Drawing.Size(207, 21);
            this.CustomerComboBox.TabIndex = 61;
            this.CustomerComboBox.Text = "-- Selecteer...--";
            this.CustomerComboBox.SelectedIndexChanged += new System.EventHandler(this.CustomerComboBox_SelectedIndexChanged);
            this.CustomerComboBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CustomerComboBox_MouseClick);
            // 
            // NummerenButton
            // 
            this.structuresExtender.SetAttributeName(this.NummerenButton, null);
            this.structuresExtender.SetAttributeTypeName(this.NummerenButton, null);
            this.structuresExtender.SetBindPropertyName(this.NummerenButton, null);
            this.NummerenButton.Location = new System.Drawing.Point(40, 140);
            this.NummerenButton.Name = "NummerenButton";
            this.NummerenButton.Size = new System.Drawing.Size(339, 42);
            this.NummerenButton.TabIndex = 59;
            this.NummerenButton.TabStop = false;
            this.NummerenButton.Text = "Betonstaal nummering en Tekla nummering uitvoeren";
            this.NummerenButton.UseVisualStyleBackColor = true;
            this.NummerenButton.Click += new System.EventHandler(this.NummerenButton_Click_1);
            // 
            // Tradional
            // 
            this.structuresExtender.SetAttributeName(this.Tradional, null);
            this.structuresExtender.SetAttributeTypeName(this.Tradional, null);
            this.structuresExtender.SetBindPropertyName(this.Tradional, null);
            this.Tradional.Controls.Add(this.groupBox1);
            this.Tradional.Controls.Add(this.groupBox5);
            this.Tradional.Controls.Add(this.groupBox4);
            this.Tradional.Controls.Add(this.groupBox3);
            this.Tradional.Controls.Add(this.m3GroupBox);
            this.Tradional.Controls.Add(this.groupBoxTraditioneel);
            this.Tradional.Location = new System.Drawing.Point(4, 22);
            this.Tradional.Name = "Tradional";
            this.Tradional.Size = new System.Drawing.Size(816, 566);
            this.Tradional.TabIndex = 7;
            this.Tradional.Text = "G-Tekeningen";
            this.Tradional.UseVisualStyleBackColor = true;
            this.Tradional.Click += new System.EventHandler(this.Tradional_Click);
            // 
            // groupBox1
            // 
            this.structuresExtender.SetAttributeName(this.groupBox1, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox1, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox1, null);
            this.groupBox1.Controls.Add(this.G0);
            this.groupBox1.Controls.Add(this.G10);
            this.groupBox1.Controls.Add(this.G9);
            this.groupBox1.Controls.Add(this.G8);
            this.groupBox1.Controls.Add(this.G7);
            this.groupBox1.Controls.Add(this.SwitchSelectionButton);
            this.groupBox1.Controls.Add(this.BtnDrawingsNew);
            this.groupBox1.Controls.Add(this.G6);
            this.groupBox1.Controls.Add(this.G1);
            this.groupBox1.Controls.Add(this.G5);
            this.groupBox1.Controls.Add(this.G2);
            this.groupBox1.Controls.Add(this.G4);
            this.groupBox1.Controls.Add(this.G3);
            this.groupBox1.Location = new System.Drawing.Point(4, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 515);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Overzichtstekeningen (G)";
            // 
            // G0
            // 
            this.structuresExtender.SetAttributeName(this.G0, "Bs0");
            this.structuresExtender.SetAttributeTypeName(this.G0, "Integer");
            this.G0.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G0, null);
            this.G0.Checked = true;
            this.G0.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G0.Location = new System.Drawing.Point(5, 28);
            this.G0.Name = "G0";
            this.G0.Size = new System.Drawing.Size(94, 17);
            this.G0.TabIndex = 57;
            this.G0.Text = "bs0 - Voorblad";
            this.G0.UseVisualStyleBackColor = true;
            // 
            // G10
            // 
            this.structuresExtender.SetAttributeName(this.G10, "Bs10");
            this.structuresExtender.SetAttributeTypeName(this.G10, "Integer");
            this.G10.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G10, null);
            this.G10.Checked = true;
            this.G10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G10.Location = new System.Drawing.Point(6, 298);
            this.G10.Name = "G10";
            this.G10.Size = new System.Drawing.Size(178, 17);
            this.G10.TabIndex = 56;
            this.G10.Text = "bs10 - Stekrekken/Haarspelden";
            this.G10.UseVisualStyleBackColor = true;
            // 
            // G9
            // 
            this.structuresExtender.SetAttributeName(this.G9, "Bs9");
            this.structuresExtender.SetAttributeTypeName(this.G9, "Integer");
            this.G9.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G9, null);
            this.G9.Checked = true;
            this.G9.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G9.Location = new System.Drawing.Point(6, 271);
            this.G9.Name = "G9";
            this.G9.Size = new System.Drawing.Size(107, 17);
            this.G9.TabIndex = 55;
            this.G9.Text = "bs9 - Stekrekken";
            this.G9.UseVisualStyleBackColor = true;
            // 
            // G8
            // 
            this.structuresExtender.SetAttributeName(this.G8, "Bs8");
            this.structuresExtender.SetAttributeTypeName(this.G8, "Integer");
            this.G8.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G8, null);
            this.G8.Checked = true;
            this.G8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G8.Location = new System.Drawing.Point(6, 244);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(93, 17);
            this.G8.TabIndex = 54;
            this.G8.Text = "bs8 - Stukslijst";
            this.G8.UseVisualStyleBackColor = true;
            // 
            // G7
            // 
            this.structuresExtender.SetAttributeName(this.G7, "Bs7");
            this.structuresExtender.SetAttributeTypeName(this.G7, "Integer");
            this.G7.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G7, null);
            this.G7.Checked = true;
            this.G7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G7.Location = new System.Drawing.Point(6, 215);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(97, 17);
            this.G7.TabIndex = 53;
            this.G7.Text = "bs7 - Overzicht";
            this.G7.UseVisualStyleBackColor = true;
            // 
            // SwitchSelectionButton
            // 
            this.structuresExtender.SetAttributeName(this.SwitchSelectionButton, null);
            this.structuresExtender.SetAttributeTypeName(this.SwitchSelectionButton, null);
            this.structuresExtender.SetBindPropertyName(this.SwitchSelectionButton, null);
            this.SwitchSelectionButton.Location = new System.Drawing.Point(26, 401);
            this.SwitchSelectionButton.Name = "SwitchSelectionButton";
            this.SwitchSelectionButton.Size = new System.Drawing.Size(164, 37);
            this.SwitchSelectionButton.TabIndex = 52;
            this.SwitchSelectionButton.Text = "Selectie omdraaien";
            this.SwitchSelectionButton.UseVisualStyleBackColor = true;
            this.SwitchSelectionButton.Click += new System.EventHandler(this.SwitchSelectionButton_Click_1);
            // 
            // BtnDrawingsNew
            // 
            this.structuresExtender.SetAttributeName(this.BtnDrawingsNew, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnDrawingsNew, null);
            this.BtnDrawingsNew.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.BtnDrawingsNew, null);
            this.BtnDrawingsNew.Location = new System.Drawing.Point(26, 334);
            this.BtnDrawingsNew.Name = "BtnDrawingsNew";
            this.BtnDrawingsNew.Size = new System.Drawing.Size(164, 52);
            this.BtnDrawingsNew.TabIndex = 44;
            this.BtnDrawingsNew.Text = "G-tekeningen maken";
            this.BtnDrawingsNew.UseVisualStyleBackColor = false;
            this.BtnDrawingsNew.Click += new System.EventHandler(this.BtnDrawingsNew_Click_1);
            // 
            // G6
            // 
            this.structuresExtender.SetAttributeName(this.G6, "Bs6");
            this.structuresExtender.SetAttributeTypeName(this.G6, "Integer");
            this.G6.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G6, null);
            this.G6.Checked = true;
            this.G6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G6.Location = new System.Drawing.Point(6, 189);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(88, 17);
            this.G6.TabIndex = 49;
            this.G6.Text = "bs6 - Vloeren";
            this.G6.UseVisualStyleBackColor = true;
            // 
            // G1
            // 
            this.structuresExtender.SetAttributeName(this.G1, "Bs1");
            this.structuresExtender.SetAttributeTypeName(this.G1, "Integer");
            this.G1.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G1, null);
            this.G1.Checked = true;
            this.G1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G1.Location = new System.Drawing.Point(5, 54);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(120, 17);
            this.G1.TabIndex = 21;
            this.G1.Text = "bs1 - Korven Prefab";
            this.G1.UseVisualStyleBackColor = true;
            // 
            // G5
            // 
            this.structuresExtender.SetAttributeName(this.G5, "Bs5");
            this.structuresExtender.SetAttributeTypeName(this.G5, "Integer");
            this.G5.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G5, null);
            this.G5.Checked = true;
            this.G5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G5.Location = new System.Drawing.Point(6, 162);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(112, 17);
            this.G5.TabIndex = 48;
            this.G5.Text = "bs5 - Haarspelden";
            this.G5.UseVisualStyleBackColor = true;
            // 
            // G2
            // 
            this.structuresExtender.SetAttributeName(this.G2, "Bs2");
            this.structuresExtender.SetAttributeTypeName(this.G2, "Integer");
            this.G2.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G2, null);
            this.G2.Checked = true;
            this.G2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G2.Location = new System.Drawing.Point(5, 81);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(102, 17);
            this.G2.TabIndex = 45;
            this.G2.Text = "bs2 - Korven los";
            this.G2.UseVisualStyleBackColor = true;
            // 
            // G4
            // 
            this.structuresExtender.SetAttributeName(this.G4, "Bs4");
            this.structuresExtender.SetAttributeTypeName(this.G4, "Integer");
            this.G4.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G4, null);
            this.G4.Checked = true;
            this.G4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G4.Location = new System.Drawing.Point(6, 135);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(168, 17);
            this.G4.TabIndex = 47;
            this.G4.Text = "bs4 - Korven Prefab Los Bijleg";
            this.G4.UseVisualStyleBackColor = true;
            // 
            // G3
            // 
            this.structuresExtender.SetAttributeName(this.G3, "Bs3");
            this.structuresExtender.SetAttributeTypeName(this.G3, "Integer");
            this.G3.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G3, null);
            this.G3.Checked = true;
            this.G3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G3.Location = new System.Drawing.Point(5, 106);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(113, 17);
            this.G3.TabIndex = 46;
            this.G3.Text = "bs3 - Korven bijleg";
            this.G3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.structuresExtender.SetAttributeName(this.groupBox5, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox5, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox5, null);
            this.groupBox5.Controls.Add(this.G403);
            this.groupBox5.Controls.Add(this.G402);
            this.groupBox5.Controls.Add(this.BtnGtekStroken);
            this.groupBox5.Controls.Add(this.G401);
            this.groupBox5.Location = new System.Drawing.Point(615, 355);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(193, 195);
            this.groupBox5.TabIndex = 61;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "G-tek Stroken";
            // 
            // G403
            // 
            this.structuresExtender.SetAttributeName(this.G403, "Bs403");
            this.structuresExtender.SetAttributeTypeName(this.G403, "Integer");
            this.G403.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G403, null);
            this.G403.Location = new System.Drawing.Point(11, 92);
            this.G403.Name = "G403";
            this.G403.Size = new System.Drawing.Size(106, 17);
            this.G403.TabIndex = 82;
            this.G403.Text = "bss403 - Stroken";
            this.G403.UseVisualStyleBackColor = true;
            // 
            // G402
            // 
            this.structuresExtender.SetAttributeName(this.G402, "Bs402");
            this.structuresExtender.SetAttributeTypeName(this.G402, "Integer");
            this.G402.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G402, null);
            this.G402.Location = new System.Drawing.Point(11, 67);
            this.G402.Name = "G402";
            this.G402.Size = new System.Drawing.Size(139, 17);
            this.G402.TabIndex = 81;
            this.G402.Text = "bss402 - Stroken boven";
            this.G402.UseVisualStyleBackColor = true;
            // 
            // BtnGtekStroken
            // 
            this.structuresExtender.SetAttributeName(this.BtnGtekStroken, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnGtekStroken, null);
            this.structuresExtender.SetBindPropertyName(this.BtnGtekStroken, null);
            this.BtnGtekStroken.Location = new System.Drawing.Point(21, 146);
            this.BtnGtekStroken.Name = "BtnGtekStroken";
            this.BtnGtekStroken.Size = new System.Drawing.Size(133, 43);
            this.BtnGtekStroken.TabIndex = 80;
            this.BtnGtekStroken.Text = "G-tek maken";
            this.BtnGtekStroken.UseVisualStyleBackColor = true;
            this.BtnGtekStroken.Click += new System.EventHandler(this.BtnGtekStroken_Click);
            // 
            // G401
            // 
            this.structuresExtender.SetAttributeName(this.G401, "Bs401");
            this.structuresExtender.SetAttributeTypeName(this.G401, "Integer");
            this.G401.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G401, null);
            this.G401.Location = new System.Drawing.Point(11, 44);
            this.G401.Name = "G401";
            this.G401.Size = new System.Drawing.Size(136, 17);
            this.G401.TabIndex = 79;
            this.G401.Text = "bss401 - Stroken onder";
            this.G401.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.structuresExtender.SetAttributeName(this.groupBox4, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox4, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox4, null);
            this.groupBox4.Controls.Add(this.G308);
            this.groupBox4.Controls.Add(this.G307);
            this.groupBox4.Controls.Add(this.G306);
            this.groupBox4.Controls.Add(this.G305);
            this.groupBox4.Controls.Add(this.G304);
            this.groupBox4.Controls.Add(this.G303);
            this.groupBox4.Controls.Add(this.G302);
            this.groupBox4.Controls.Add(this.G301);
            this.groupBox4.Controls.Add(this.G300);
            this.groupBox4.Controls.Add(this.BtnGtekBreedplaten);
            this.groupBox4.Location = new System.Drawing.Point(615, 25);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(193, 327);
            this.groupBox4.TabIndex = 60;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "G-tek Breedplaten";
            // 
            // G308
            // 
            this.structuresExtender.SetAttributeName(this.G308, "Bs308");
            this.structuresExtender.SetAttributeTypeName(this.G308, "Integer");
            this.G308.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G308, null);
            this.G308.Location = new System.Drawing.Point(11, 238);
            this.G308.Name = "G308";
            this.G308.Size = new System.Drawing.Size(99, 17);
            this.G308.TabIndex = 79;
            this.G308.Text = "bsb308 - Kolom";
            this.G308.UseVisualStyleBackColor = true;
            // 
            // G307
            // 
            this.structuresExtender.SetAttributeName(this.G307, "Bs307");
            this.structuresExtender.SetAttributeTypeName(this.G307, "Integer");
            this.G307.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G307, null);
            this.G307.Location = new System.Drawing.Point(12, 214);
            this.G307.Name = "G307";
            this.G307.Size = new System.Drawing.Size(91, 17);
            this.G307.TabIndex = 78;
            this.G307.Text = "bsb307 - Balk";
            this.G307.UseVisualStyleBackColor = true;
            // 
            // G306
            // 
            this.structuresExtender.SetAttributeName(this.G306, "Bs306");
            this.structuresExtender.SetAttributeTypeName(this.G306, "Integer");
            this.G306.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G306, null);
            this.G306.Location = new System.Drawing.Point(12, 188);
            this.G306.Name = "G306";
            this.G306.Size = new System.Drawing.Size(120, 17);
            this.G306.TabIndex = 77;
            this.G306.Text = "bsb306 - Net boven";
            this.G306.UseVisualStyleBackColor = true;
            // 
            // G305
            // 
            this.structuresExtender.SetAttributeName(this.G305, "Bs305");
            this.structuresExtender.SetAttributeTypeName(this.G305, "Integer");
            this.G305.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G305, null);
            this.G305.Location = new System.Drawing.Point(12, 162);
            this.G305.Name = "G305";
            this.G305.Size = new System.Drawing.Size(126, 17);
            this.G305.TabIndex = 76;
            this.G305.Text = "bsb305 - Wap boven";
            this.G305.UseVisualStyleBackColor = true;
            // 
            // G304
            // 
            this.structuresExtender.SetAttributeName(this.G304, "Bs304");
            this.structuresExtender.SetAttributeTypeName(this.G304, "Integer");
            this.G304.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G304, null);
            this.G304.Location = new System.Drawing.Point(12, 136);
            this.G304.Name = "G304";
            this.G304.Size = new System.Drawing.Size(133, 17);
            this.G304.TabIndex = 75;
            this.G304.Text = "bsb304 - Hrsp op plaat";
            this.G304.UseVisualStyleBackColor = true;
            // 
            // G303
            // 
            this.structuresExtender.SetAttributeName(this.G303, "Bs303");
            this.structuresExtender.SetAttributeTypeName(this.G303, "Integer");
            this.G303.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G303, null);
            this.G303.Location = new System.Drawing.Point(12, 110);
            this.G303.Name = "G303";
            this.G303.Size = new System.Drawing.Size(134, 17);
            this.G303.TabIndex = 74;
            this.G303.Text = "bsb303 - Wap op plaat";
            this.G303.UseVisualStyleBackColor = true;
            // 
            // G302
            // 
            this.structuresExtender.SetAttributeName(this.G302, "Bs302");
            this.structuresExtender.SetAttributeTypeName(this.G302, "Integer");
            this.G302.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G302, null);
            this.G302.Checked = true;
            this.G302.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G302.Location = new System.Drawing.Point(12, 87);
            this.G302.Name = "G302";
            this.G302.Size = new System.Drawing.Size(128, 17);
            this.G302.TabIndex = 73;
            this.G302.Text = "bsb302 - Net op plaat";
            this.G302.UseVisualStyleBackColor = true;
            // 
            // G301
            // 
            this.structuresExtender.SetAttributeName(this.G301, "Bs301");
            this.structuresExtender.SetAttributeTypeName(this.G301, "Integer");
            this.G301.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G301, null);
            this.G301.Location = new System.Drawing.Point(12, 64);
            this.G301.Name = "G301";
            this.G301.Size = new System.Drawing.Size(121, 17);
            this.G301.TabIndex = 72;
            this.G301.Text = "bsb301 - Breedplaat";
            this.G301.UseVisualStyleBackColor = true;
            // 
            // G300
            // 
            this.structuresExtender.SetAttributeName(this.G300, "Bs300");
            this.structuresExtender.SetAttributeTypeName(this.G300, "Integer");
            this.G300.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G300, null);
            this.G300.Location = new System.Drawing.Point(12, 38);
            this.G300.Name = "G300";
            this.G300.Size = new System.Drawing.Size(112, 17);
            this.G300.TabIndex = 71;
            this.G300.Text = "bsb300 - Voorblad";
            this.G300.UseVisualStyleBackColor = true;
            // 
            // BtnGtekBreedplaten
            // 
            this.structuresExtender.SetAttributeName(this.BtnGtekBreedplaten, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnGtekBreedplaten, null);
            this.structuresExtender.SetBindPropertyName(this.BtnGtekBreedplaten, null);
            this.BtnGtekBreedplaten.Location = new System.Drawing.Point(12, 264);
            this.BtnGtekBreedplaten.Name = "BtnGtekBreedplaten";
            this.BtnGtekBreedplaten.Size = new System.Drawing.Size(133, 43);
            this.BtnGtekBreedplaten.TabIndex = 30;
            this.BtnGtekBreedplaten.Text = "G-tek maken";
            this.BtnGtekBreedplaten.UseVisualStyleBackColor = true;
            this.BtnGtekBreedplaten.Click += new System.EventHandler(this.BtnGtekBreedplaten_Click);
            // 
            // groupBox3
            // 
            this.structuresExtender.SetAttributeName(this.groupBox3, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox3, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox3, null);
            this.groupBox3.Controls.Add(this.G210);
            this.groupBox3.Controls.Add(this.G209);
            this.groupBox3.Controls.Add(this.G208);
            this.groupBox3.Controls.Add(this.G206);
            this.groupBox3.Controls.Add(this.G207);
            this.groupBox3.Controls.Add(this.G205);
            this.groupBox3.Controls.Add(this.btnGtekKelderwanden);
            this.groupBox3.Controls.Add(this.G204);
            this.groupBox3.Controls.Add(this.G200);
            this.groupBox3.Controls.Add(this.G201);
            this.groupBox3.Controls.Add(this.G203);
            this.groupBox3.Controls.Add(this.G202);
            this.groupBox3.Location = new System.Drawing.Point(425, 25);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(184, 397);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "G-tek Kelder";
            // 
            // G210
            // 
            this.structuresExtender.SetAttributeName(this.G210, "Bs210");
            this.structuresExtender.SetAttributeTypeName(this.G210, "Integer");
            this.G210.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G210, null);
            this.G210.Location = new System.Drawing.Point(13, 288);
            this.G210.Name = "G210";
            this.G210.Size = new System.Drawing.Size(106, 17);
            this.G210.TabIndex = 70;
            this.G210.Text = "bsk210 - Stuklijst";
            this.G210.UseVisualStyleBackColor = true;
            // 
            // G209
            // 
            this.structuresExtender.SetAttributeName(this.G209, "Bs209");
            this.structuresExtender.SetAttributeTypeName(this.G209, "Integer");
            this.G209.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G209, null);
            this.G209.Location = new System.Drawing.Point(13, 263);
            this.G209.Name = "G209";
            this.G209.Size = new System.Drawing.Size(119, 17);
            this.G209.TabIndex = 69;
            this.G209.Text = "bsk209 - Vloer Hrsp";
            this.G209.UseVisualStyleBackColor = true;
            // 
            // G208
            // 
            this.structuresExtender.SetAttributeName(this.G208, "Bs208");
            this.structuresExtender.SetAttributeTypeName(this.G208, "Integer");
            this.G208.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G208, null);
            this.G208.Location = new System.Drawing.Point(13, 238);
            this.G208.Name = "G208";
            this.G208.Size = new System.Drawing.Size(113, 17);
            this.G208.TabIndex = 68;
            this.G208.Text = "bsk208 - Dekvloer";
            this.G208.UseVisualStyleBackColor = true;
            // 
            // G206
            // 
            this.structuresExtender.SetAttributeName(this.G206, "Bs206");
            this.structuresExtender.SetAttributeTypeName(this.G206, "Integer");
            this.G206.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G206, null);
            this.G206.Checked = true;
            this.G206.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G206.Location = new System.Drawing.Point(13, 186);
            this.G206.Name = "G206";
            this.G206.Size = new System.Drawing.Size(124, 17);
            this.G206.TabIndex = 67;
            this.G206.Text = "bsk206 - Wand Hrsp";
            this.G206.UseVisualStyleBackColor = true;
            // 
            // G207
            // 
            this.structuresExtender.SetAttributeName(this.G207, "Bs207");
            this.structuresExtender.SetAttributeTypeName(this.G207, "Integer");
            this.G207.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G207, null);
            this.G207.Checked = true;
            this.G207.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G207.Location = new System.Drawing.Point(13, 212);
            this.G207.Name = "G207";
            this.G207.Size = new System.Drawing.Size(175, 17);
            this.G207.TabIndex = 66;
            this.G207.Text = "bsk207 - Wand koppeling Vloer";
            this.G207.UseVisualStyleBackColor = true;
            // 
            // G205
            // 
            this.structuresExtender.SetAttributeName(this.G205, "Bs205");
            this.structuresExtender.SetAttributeTypeName(this.G205, "Integer");
            this.G205.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G205, null);
            this.G205.Checked = true;
            this.G205.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G205.Location = new System.Drawing.Point(13, 160);
            this.G205.Name = "G205";
            this.G205.Size = new System.Drawing.Size(135, 17);
            this.G205.TabIndex = 65;
            this.G205.Text = "bsk205 - Wand Matten";
            this.G205.UseVisualStyleBackColor = true;
            // 
            // btnGtekKelderwanden
            // 
            this.structuresExtender.SetAttributeName(this.btnGtekKelderwanden, null);
            this.structuresExtender.SetAttributeTypeName(this.btnGtekKelderwanden, null);
            this.structuresExtender.SetBindPropertyName(this.btnGtekKelderwanden, null);
            this.btnGtekKelderwanden.Location = new System.Drawing.Point(17, 330);
            this.btnGtekKelderwanden.Name = "btnGtekKelderwanden";
            this.btnGtekKelderwanden.Size = new System.Drawing.Size(132, 43);
            this.btnGtekKelderwanden.TabIndex = 29;
            this.btnGtekKelderwanden.Text = "G-tek maken";
            this.btnGtekKelderwanden.UseVisualStyleBackColor = true;
            this.btnGtekKelderwanden.Click += new System.EventHandler(this.btnGtekKelderwanden_Click);
            // 
            // G204
            // 
            this.structuresExtender.SetAttributeName(this.G204, "Bs204");
            this.structuresExtender.SetAttributeTypeName(this.G204, "Integer");
            this.G204.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G204, null);
            this.G204.Checked = true;
            this.G204.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G204.Location = new System.Drawing.Point(13, 134);
            this.G204.Name = "G204";
            this.G204.Size = new System.Drawing.Size(122, 17);
            this.G204.TabIndex = 64;
            this.G204.Text = "bsk204 - Vloer Bijleg";
            this.G204.UseVisualStyleBackColor = true;
            // 
            // G200
            // 
            this.structuresExtender.SetAttributeName(this.G200, "Bs200");
            this.structuresExtender.SetAttributeTypeName(this.G200, "Integer");
            this.G200.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G200, null);
            this.G200.Checked = true;
            this.G200.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G200.Location = new System.Drawing.Point(13, 36);
            this.G200.Name = "G200";
            this.G200.Size = new System.Drawing.Size(112, 17);
            this.G200.TabIndex = 63;
            this.G200.Text = "bsk200 - Voorblad";
            this.G200.UseVisualStyleBackColor = true;
            // 
            // G201
            // 
            this.structuresExtender.SetAttributeName(this.G201, "Bs201");
            this.structuresExtender.SetAttributeTypeName(this.G201, "Integer");
            this.G201.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G201, null);
            this.G201.Checked = true;
            this.G201.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G201.Location = new System.Drawing.Point(13, 62);
            this.G201.Name = "G201";
            this.G201.Size = new System.Drawing.Size(123, 17);
            this.G201.TabIndex = 60;
            this.G201.Text = "bsk201 - Keldervloer";
            this.G201.UseVisualStyleBackColor = true;
            // 
            // G203
            // 
            this.structuresExtender.SetAttributeName(this.G203, "Bs203");
            this.structuresExtender.SetAttributeTypeName(this.G203, "Integer");
            this.G203.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G203, null);
            this.G203.Checked = true;
            this.G203.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G203.Location = new System.Drawing.Point(13, 108);
            this.G203.Name = "G203";
            this.G203.Size = new System.Drawing.Size(178, 17);
            this.G203.TabIndex = 62;
            this.G203.Text = "bsk203 - Vloer  koppeling Wand";
            this.G203.UseVisualStyleBackColor = true;
            // 
            // G202
            // 
            this.structuresExtender.SetAttributeName(this.G202, "Bs202");
            this.structuresExtender.SetAttributeTypeName(this.G202, "Integer");
            this.G202.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G202, null);
            this.G202.Checked = true;
            this.G202.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G202.Location = new System.Drawing.Point(13, 85);
            this.G202.Name = "G202";
            this.G202.Size = new System.Drawing.Size(119, 17);
            this.G202.TabIndex = 61;
            this.G202.Text = "bsk202 - Vloer Hrsp";
            this.G202.UseVisualStyleBackColor = true;
            // 
            // m3GroupBox
            // 
            this.structuresExtender.SetAttributeName(this.m3GroupBox, null);
            this.structuresExtender.SetAttributeTypeName(this.m3GroupBox, null);
            this.structuresExtender.SetBindPropertyName(this.m3GroupBox, null);
            this.m3GroupBox.Controls.Add(this.btnGtekm3);
            this.m3GroupBox.Controls.Add(this.G500);
            this.m3GroupBox.Location = new System.Drawing.Point(233, 399);
            this.m3GroupBox.Name = "m3GroupBox";
            this.m3GroupBox.Size = new System.Drawing.Size(184, 145);
            this.m3GroupBox.TabIndex = 58;
            this.m3GroupBox.TabStop = false;
            this.m3GroupBox.Text = "m3";
            // 
            // btnGtekm3
            // 
            this.structuresExtender.SetAttributeName(this.btnGtekm3, null);
            this.structuresExtender.SetAttributeTypeName(this.btnGtekm3, null);
            this.structuresExtender.SetBindPropertyName(this.btnGtekm3, null);
            this.btnGtekm3.Location = new System.Drawing.Point(18, 66);
            this.btnGtekm3.Name = "btnGtekm3";
            this.btnGtekm3.Size = new System.Drawing.Size(128, 43);
            this.btnGtekm3.TabIndex = 29;
            this.btnGtekm3.Text = "G-tek maken";
            this.btnGtekm3.UseVisualStyleBackColor = true;
            this.btnGtekm3.Click += new System.EventHandler(this.btnGtekm3_Click);
            // 
            // G500
            // 
            this.structuresExtender.SetAttributeName(this.G500, "Bs500");
            this.structuresExtender.SetAttributeTypeName(this.G500, "Integer");
            this.G500.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G500, null);
            this.G500.Checked = true;
            this.G500.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G500.Location = new System.Drawing.Point(13, 31);
            this.G500.Name = "G500";
            this.G500.Size = new System.Drawing.Size(126, 17);
            this.G500.TabIndex = 29;
            this.G500.Text = "bst500 - Voorblad m3";
            this.G500.UseVisualStyleBackColor = true;
            // 
            // groupBoxTraditioneel
            // 
            this.structuresExtender.SetAttributeName(this.groupBoxTraditioneel, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBoxTraditioneel, null);
            this.structuresExtender.SetBindPropertyName(this.groupBoxTraditioneel, null);
            this.groupBoxTraditioneel.Controls.Add(this.G109);
            this.groupBoxTraditioneel.Controls.Add(this.G108);
            this.groupBoxTraditioneel.Controls.Add(this.G107);
            this.groupBoxTraditioneel.Controls.Add(this.G106);
            this.groupBoxTraditioneel.Controls.Add(this.G105);
            this.groupBoxTraditioneel.Controls.Add(this.G104);
            this.groupBoxTraditioneel.Controls.Add(this.G100);
            this.groupBoxTraditioneel.Controls.Add(this.btnGtekTraditioneel);
            this.groupBoxTraditioneel.Controls.Add(this.G103);
            this.groupBoxTraditioneel.Controls.Add(this.G102);
            this.groupBoxTraditioneel.Controls.Add(this.G101);
            this.groupBoxTraditioneel.Location = new System.Drawing.Point(231, 25);
            this.groupBoxTraditioneel.Name = "groupBoxTraditioneel";
            this.groupBoxTraditioneel.Size = new System.Drawing.Size(186, 356);
            this.groupBoxTraditioneel.TabIndex = 57;
            this.groupBoxTraditioneel.TabStop = false;
            this.groupBoxTraditioneel.Text = "G-tek Traditioneel";
            // 
            // G109
            // 
            this.structuresExtender.SetAttributeName(this.G109, "Bs109");
            this.structuresExtender.SetAttributeTypeName(this.G109, "Integer");
            this.G109.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G109, null);
            this.G109.Checked = true;
            this.G109.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G109.Location = new System.Drawing.Point(19, 260);
            this.G109.Name = "G109";
            this.G109.Size = new System.Drawing.Size(110, 17);
            this.G109.TabIndex = 32;
            this.G109.Text = "bst109 - Koekoek";
            this.G109.UseVisualStyleBackColor = true;
            // 
            // G108
            // 
            this.structuresExtender.SetAttributeName(this.G108, "Bs108");
            this.structuresExtender.SetAttributeTypeName(this.G108, "Integer");
            this.G108.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G108, null);
            this.G108.Checked = true;
            this.G108.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G108.Location = new System.Drawing.Point(19, 237);
            this.G108.Name = "G108";
            this.G108.Size = new System.Drawing.Size(108, 17);
            this.G108.TabIndex = 31;
            this.G108.Text = "bst108 - Wanden";
            this.G108.UseVisualStyleBackColor = true;
            // 
            // G107
            // 
            this.structuresExtender.SetAttributeName(this.G107, "Bs107");
            this.structuresExtender.SetAttributeTypeName(this.G107, "Integer");
            this.G107.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G107, null);
            this.G107.Checked = true;
            this.G107.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G107.Location = new System.Drawing.Point(19, 214);
            this.G107.Name = "G107";
            this.G107.Size = new System.Drawing.Size(83, 17);
            this.G107.TabIndex = 30;
            this.G107.Text = "bst107 - Put";
            this.G107.UseVisualStyleBackColor = true;
            // 
            // G106
            // 
            this.structuresExtender.SetAttributeName(this.G106, "Bs106");
            this.structuresExtender.SetAttributeTypeName(this.G106, "Integer");
            this.G106.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G106, null);
            this.G106.Checked = true;
            this.G106.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G106.Location = new System.Drawing.Point(19, 188);
            this.G106.Name = "G106";
            this.G106.Size = new System.Drawing.Size(107, 17);
            this.G106.TabIndex = 29;
            this.G106.Text = "bst106 - Stekken";
            this.G106.UseVisualStyleBackColor = true;
            // 
            // G105
            // 
            this.structuresExtender.SetAttributeName(this.G105, "Bs105");
            this.structuresExtender.SetAttributeTypeName(this.G105, "Integer");
            this.G105.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G105, null);
            this.G105.Checked = true;
            this.G105.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G105.Location = new System.Drawing.Point(19, 162);
            this.G105.Name = "G105";
            this.G105.Size = new System.Drawing.Size(92, 17);
            this.G105.TabIndex = 28;
            this.G105.Text = "bst105 - Bijleg";
            this.G105.UseVisualStyleBackColor = true;
            // 
            // G104
            // 
            this.structuresExtender.SetAttributeName(this.G104, "Bs104");
            this.structuresExtender.SetAttributeTypeName(this.G104, "Integer");
            this.G104.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G104, null);
            this.G104.Checked = true;
            this.G104.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G104.Location = new System.Drawing.Point(19, 136);
            this.G104.Name = "G104";
            this.G104.Size = new System.Drawing.Size(124, 17);
            this.G104.TabIndex = 27;
            this.G104.Text = "bst104 - Inkassingen";
            this.G104.UseVisualStyleBackColor = true;
            // 
            // G100
            // 
            this.structuresExtender.SetAttributeName(this.G100, "Bs100");
            this.structuresExtender.SetAttributeTypeName(this.G100, "Integer");
            this.G100.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G100, null);
            this.G100.Checked = true;
            this.G100.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G100.Location = new System.Drawing.Point(19, 38);
            this.G100.Name = "G100";
            this.G100.Size = new System.Drawing.Size(109, 17);
            this.G100.TabIndex = 26;
            this.G100.Text = "bst100 - Voorblad";
            this.G100.UseVisualStyleBackColor = true;
            // 
            // btnGtekTraditioneel
            // 
            this.structuresExtender.SetAttributeName(this.btnGtekTraditioneel, null);
            this.structuresExtender.SetAttributeTypeName(this.btnGtekTraditioneel, null);
            this.structuresExtender.SetBindPropertyName(this.btnGtekTraditioneel, null);
            this.btnGtekTraditioneel.Location = new System.Drawing.Point(18, 290);
            this.btnGtekTraditioneel.Name = "btnGtekTraditioneel";
            this.btnGtekTraditioneel.Size = new System.Drawing.Size(148, 43);
            this.btnGtekTraditioneel.TabIndex = 25;
            this.btnGtekTraditioneel.Text = "G-tek maken";
            this.btnGtekTraditioneel.UseVisualStyleBackColor = true;
            this.btnGtekTraditioneel.Click += new System.EventHandler(this.btnGtekTraditioneel_Click_1);
            // 
            // G103
            // 
            this.structuresExtender.SetAttributeName(this.G103, "Bs103");
            this.structuresExtender.SetAttributeTypeName(this.G103, "Integer");
            this.G103.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G103, null);
            this.G103.Checked = true;
            this.G103.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G103.Location = new System.Drawing.Point(19, 110);
            this.G103.Name = "G103";
            this.G103.Size = new System.Drawing.Size(114, 17);
            this.G103.TabIndex = 24;
            this.G103.Text = "bst103 - Fundering";
            this.G103.UseVisualStyleBackColor = true;
            // 
            // G102
            // 
            this.structuresExtender.SetAttributeName(this.G102, "Bs102");
            this.structuresExtender.SetAttributeTypeName(this.G102, "Integer");
            this.G102.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G102, null);
            this.G102.Checked = true;
            this.G102.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G102.Location = new System.Drawing.Point(19, 87);
            this.G102.Name = "G102";
            this.G102.Size = new System.Drawing.Size(121, 17);
            this.G102.TabIndex = 23;
            this.G102.Text = "bst102 - Borgstaven";
            this.G102.UseVisualStyleBackColor = true;
            // 
            // G101
            // 
            this.structuresExtender.SetAttributeName(this.G101, "Bs101");
            this.structuresExtender.SetAttributeTypeName(this.G101, "Integer");
            this.G101.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.G101, null);
            this.G101.Checked = true;
            this.G101.CheckState = System.Windows.Forms.CheckState.Checked;
            this.G101.Location = new System.Drawing.Point(19, 64);
            this.G101.Name = "G101";
            this.G101.Size = new System.Drawing.Size(91, 17);
            this.G101.TabIndex = 22;
            this.G101.Text = "bst101 - Vloer";
            this.G101.UseVisualStyleBackColor = true;
            // 
            // tabCUDrawings
            // 
            this.structuresExtender.SetAttributeName(this.tabCUDrawings, null);
            this.structuresExtender.SetAttributeTypeName(this.tabCUDrawings, null);
            this.structuresExtender.SetBindPropertyName(this.tabCUDrawings, null);
            this.tabCUDrawings.Controls.Add(this.DrawingStatus);
            this.tabCUDrawings.Controls.Add(this.groupBox2);
            this.tabCUDrawings.Location = new System.Drawing.Point(4, 22);
            this.tabCUDrawings.Name = "tabCUDrawings";
            this.tabCUDrawings.Size = new System.Drawing.Size(816, 566);
            this.tabCUDrawings.TabIndex = 9;
            this.tabCUDrawings.Text = "C-Tekeningen";
            this.tabCUDrawings.UseVisualStyleBackColor = true;
            // 
            // DrawingStatus
            // 
            this.structuresExtender.SetAttributeName(this.DrawingStatus, null);
            this.structuresExtender.SetAttributeTypeName(this.DrawingStatus, null);
            this.DrawingStatus.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.DrawingStatus, null);
            this.DrawingStatus.Location = new System.Drawing.Point(65, 269);
            this.DrawingStatus.Name = "DrawingStatus";
            this.DrawingStatus.Size = new System.Drawing.Size(0, 13);
            this.DrawingStatus.TabIndex = 55;
            // 
            // groupBox2
            // 
            this.structuresExtender.SetAttributeName(this.groupBox2, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox2, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox2, null);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.BtnDrawingBalkLos);
            this.groupBox2.Controls.Add(this.WizardButton);
            this.groupBox2.Controls.Add(this.BtnDrawingsBalk);
            this.groupBox2.Location = new System.Drawing.Point(29, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(458, 230);
            this.groupBox2.TabIndex = 54;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Betontekeningen (C)";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // button3
            // 
            this.structuresExtender.SetAttributeName(this.button3, null);
            this.structuresExtender.SetAttributeTypeName(this.button3, null);
            this.structuresExtender.SetBindPropertyName(this.button3, null);
            this.button3.Location = new System.Drawing.Point(251, 107);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(191, 43);
            this.button3.TabIndex = 55;
            this.button3.Text = "Naam aanpassen";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.MouseHover += new System.EventHandler(this.button3_MouseHover);
            // 
            // BtnDrawingBalkLos
            // 
            this.structuresExtender.SetAttributeName(this.BtnDrawingBalkLos, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnDrawingBalkLos, null);
            this.structuresExtender.SetBindPropertyName(this.BtnDrawingBalkLos, null);
            this.BtnDrawingBalkLos.Location = new System.Drawing.Point(251, 64);
            this.BtnDrawingBalkLos.Name = "BtnDrawingBalkLos";
            this.BtnDrawingBalkLos.Size = new System.Drawing.Size(191, 37);
            this.BtnDrawingBalkLos.TabIndex = 54;
            this.BtnDrawingBalkLos.Text = "Korf Los";
            this.BtnDrawingBalkLos.UseVisualStyleBackColor = true;
            this.BtnDrawingBalkLos.Click += new System.EventHandler(this.BtnDrawingBalkLos_Click_1);
            // 
            // WizardButton
            // 
            this.structuresExtender.SetAttributeName(this.WizardButton, null);
            this.structuresExtender.SetAttributeTypeName(this.WizardButton, null);
            this.structuresExtender.SetBindPropertyName(this.WizardButton, null);
            this.WizardButton.Location = new System.Drawing.Point(28, 22);
            this.WizardButton.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.WizardButton.Name = "WizardButton";
            this.WizardButton.Size = new System.Drawing.Size(191, 169);
            this.WizardButton.TabIndex = 53;
            this.WizardButton.Text = "- Korf\r\n- Hrsp\r\n- Stek\r\n- Zrek\r\n- Poer\r\n- Stiep\r\n- Nok\r\n- Ronde kolom\r\n- Mat op m" +
    "aat";
            this.WizardButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.WizardButton.UseVisualStyleBackColor = true;
            this.WizardButton.Click += new System.EventHandler(this.WizardButton_Click_1);
            // 
            // BtnDrawingsBalk
            // 
            this.structuresExtender.SetAttributeName(this.BtnDrawingsBalk, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnDrawingsBalk, null);
            this.structuresExtender.SetBindPropertyName(this.BtnDrawingsBalk, null);
            this.BtnDrawingsBalk.Location = new System.Drawing.Point(251, 22);
            this.BtnDrawingsBalk.Name = "BtnDrawingsBalk";
            this.BtnDrawingsBalk.Size = new System.Drawing.Size(191, 36);
            this.BtnDrawingsBalk.TabIndex = 52;
            this.BtnDrawingsBalk.Text = "Balk (samenstellingen)";
            this.BtnDrawingsBalk.UseVisualStyleBackColor = true;
            this.BtnDrawingsBalk.Click += new System.EventHandler(this.BtnDrawingsBalk_Click_1);
            // 
            // TabExport
            // 
            this.structuresExtender.SetAttributeName(this.TabExport, null);
            this.structuresExtender.SetAttributeTypeName(this.TabExport, null);
            this.structuresExtender.SetBindPropertyName(this.TabExport, null);
            this.TabExport.Controls.Add(this.EmptyPhaseButton);
            this.TabExport.Controls.Add(this.TxRadJsonbutton);
            this.TabExport.Controls.Add(this.buttonOK);
            this.TabExport.Controls.Add(this.progressBar1);
            this.TabExport.Controls.Add(this.pictureBox2);
            this.TabExport.Controls.Add(this.ExportButton);
            this.TabExport.Controls.Add(this.JsonGroupBox);
            this.TabExport.Controls.Add(this.Export);
            this.TabExport.Controls.Add(this.groupBox6);
            this.TabExport.Controls.Add(this.groupBoxPDF);
            this.TabExport.Controls.Add(this.TxRadbutton);
            this.TabExport.Controls.Add(this.lblNummering);
            this.TabExport.Controls.Add(this.TxKlasse);
            this.TabExport.Controls.Add(this.pictureBox4);
            this.TabExport.Location = new System.Drawing.Point(4, 22);
            this.TabExport.Margin = new System.Windows.Forms.Padding(4);
            this.TabExport.Name = "TabExport";
            this.TabExport.Padding = new System.Windows.Forms.Padding(4);
            this.TabExport.Size = new System.Drawing.Size(816, 566);
            this.TabExport.TabIndex = 2;
            this.TabExport.Text = "Exporteren";
            this.TabExport.UseVisualStyleBackColor = true;
            this.TabExport.Click += new System.EventHandler(this.ParametersTabPage_Click);
            // 
            // EmptyPhaseButton
            // 
            this.structuresExtender.SetAttributeName(this.EmptyPhaseButton, null);
            this.structuresExtender.SetAttributeTypeName(this.EmptyPhaseButton, null);
            this.EmptyPhaseButton.BackColor = System.Drawing.Color.LightGray;
            this.structuresExtender.SetBindPropertyName(this.EmptyPhaseButton, null);
            this.EmptyPhaseButton.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.EmptyPhaseButton.FlatAppearance.BorderSize = 5;
            this.EmptyPhaseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.EmptyPhaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EmptyPhaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmptyPhaseButton.ForeColor = System.Drawing.Color.Black;
            this.EmptyPhaseButton.Location = new System.Drawing.Point(676, 224);
            this.EmptyPhaseButton.Name = "EmptyPhaseButton";
            this.EmptyPhaseButton.Size = new System.Drawing.Size(99, 56);
            this.EmptyPhaseButton.TabIndex = 70;
            this.EmptyPhaseButton.Text = "Verwijder lege fases";
            this.EmptyPhaseButton.UseVisualStyleBackColor = false;
            this.EmptyPhaseButton.Click += new System.EventHandler(this.EmptyPhaseButton_Click);
            // 
            // TxRadJsonbutton
            // 
            this.structuresExtender.SetAttributeName(this.TxRadJsonbutton, "RadJsonbutton");
            this.structuresExtender.SetAttributeTypeName(this.TxRadJsonbutton, "String");
            this.structuresExtender.SetBindPropertyName(this.TxRadJsonbutton, null);
            this.TxRadJsonbutton.Location = new System.Drawing.Point(10, 46);
            this.TxRadJsonbutton.Name = "TxRadJsonbutton";
            this.TxRadJsonbutton.Size = new System.Drawing.Size(27, 20);
            this.TxRadJsonbutton.TabIndex = 69;
            this.TxRadJsonbutton.Text = "P";
            this.TxRadJsonbutton.TextChanged += new System.EventHandler(this.TxRadJsonbutton_TextChanged);
            // 
            // buttonOK
            // 
            this.structuresExtender.SetAttributeName(this.buttonOK, null);
            this.structuresExtender.SetAttributeTypeName(this.buttonOK, null);
            this.structuresExtender.SetBindPropertyName(this.buttonOK, null);
            this.buttonOK.Location = new System.Drawing.Point(7, 529);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(101, 34);
            this.buttonOK.TabIndex = 61;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // progressBar1
            // 
            this.structuresExtender.SetAttributeName(this.progressBar1, null);
            this.structuresExtender.SetAttributeTypeName(this.progressBar1, null);
            this.structuresExtender.SetBindPropertyName(this.progressBar1, null);
            this.progressBar1.Location = new System.Drawing.Point(572, 482);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBar1.Size = new System.Drawing.Size(54, 17);
            this.progressBar1.Step = 5;
            this.progressBar1.TabIndex = 62;
            this.progressBar1.Visible = false;
            // 
            // pictureBox2
            // 
            this.structuresExtender.SetAttributeName(this.pictureBox2, null);
            this.structuresExtender.SetAttributeTypeName(this.pictureBox2, null);
            this.structuresExtender.SetBindPropertyName(this.pictureBox2, null);
            this.pictureBox2.Image = global::BS_Export.Properties.Resources.accolade1;
            this.pictureBox2.Location = new System.Drawing.Point(513, 157);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 342);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 61;
            this.pictureBox2.TabStop = false;
            // 
            // ExportButton
            // 
            this.structuresExtender.SetAttributeName(this.ExportButton, null);
            this.structuresExtender.SetAttributeTypeName(this.ExportButton, null);
            this.structuresExtender.SetBindPropertyName(this.ExportButton, null);
            this.ExportButton.Location = new System.Drawing.Point(571, 157);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(56, 342);
            this.ExportButton.TabIndex = 58;
            this.ExportButton.Text = "Export          BVBS   IFC   PDF";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // JsonGroupBox
            // 
            this.structuresExtender.SetAttributeName(this.JsonGroupBox, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonGroupBox, null);
            this.structuresExtender.SetBindPropertyName(this.JsonGroupBox, null);
            this.JsonGroupBox.Controls.Add(this.CustomMeshButton);
            this.JsonGroupBox.Controls.Add(this.JsonBijlegButton);
            this.JsonGroupBox.Controls.Add(this.KorbButton);
            this.JsonGroupBox.Controls.Add(this.JsonKorvenButton);
            this.JsonGroupBox.Controls.Add(this.checkBoxResetJson);
            this.JsonGroupBox.Controls.Add(this.JsonFloorButton);
            this.JsonGroupBox.Controls.Add(this.checkBoxVisualizeJson);
            this.JsonGroupBox.Controls.Add(this.RadJsonPerFase);
            this.JsonGroupBox.Controls.Add(this.RadJsonAlles);
            this.JsonGroupBox.Controls.Add(this.VMeshButton);
            this.JsonGroupBox.Controls.Add(this.StrMeshButton);
            this.JsonGroupBox.Controls.Add(this.JsonHrspButton);
            this.JsonGroupBox.Controls.Add(this.JsonStekrekButton);
            this.JsonGroupBox.Controls.Add(this.button1);
            this.JsonGroupBox.Location = new System.Drawing.Point(60, 11);
            this.JsonGroupBox.Name = "JsonGroupBox";
            this.JsonGroupBox.Size = new System.Drawing.Size(749, 126);
            this.JsonGroupBox.TabIndex = 60;
            this.JsonGroupBox.TabStop = false;
            this.JsonGroupBox.Text = "JSON (van geselecteerde onderdelen)";
            // 
            // CustomMeshButton
            // 
            this.structuresExtender.SetAttributeName(this.CustomMeshButton, null);
            this.structuresExtender.SetAttributeTypeName(this.CustomMeshButton, null);
            this.structuresExtender.SetBindPropertyName(this.CustomMeshButton, null);
            this.CustomMeshButton.Location = new System.Drawing.Point(501, 66);
            this.CustomMeshButton.Name = "CustomMeshButton";
            this.CustomMeshButton.Size = new System.Drawing.Size(75, 39);
            this.CustomMeshButton.TabIndex = 65;
            this.CustomMeshButton.Text = "Custom mesh";
            this.CustomMeshButton.UseVisualStyleBackColor = true;
            this.CustomMeshButton.Click += new System.EventHandler(this.CustomMeshButton_Click);
            // 
            // JsonBijlegButton
            // 
            this.structuresExtender.SetAttributeName(this.JsonBijlegButton, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonBijlegButton, null);
            this.structuresExtender.SetBindPropertyName(this.JsonBijlegButton, null);
            this.JsonBijlegButton.Location = new System.Drawing.Point(663, 66);
            this.JsonBijlegButton.Name = "JsonBijlegButton";
            this.JsonBijlegButton.Size = new System.Drawing.Size(75, 39);
            this.JsonBijlegButton.TabIndex = 64;
            this.JsonBijlegButton.Text = "Bijleg";
            this.JsonBijlegButton.UseVisualStyleBackColor = true;
            this.JsonBijlegButton.Click += new System.EventHandler(this.JsonBijlegButton_Click);
            // 
            // KorbButton
            // 
            this.structuresExtender.SetAttributeName(this.KorbButton, null);
            this.structuresExtender.SetAttributeTypeName(this.KorbButton, null);
            this.structuresExtender.SetBindPropertyName(this.KorbButton, null);
            this.KorbButton.Location = new System.Drawing.Point(15, 66);
            this.KorbButton.Name = "KorbButton";
            this.KorbButton.Size = new System.Drawing.Size(75, 39);
            this.KorbButton.TabIndex = 63;
            this.KorbButton.Text = "Korb";
            this.KorbButton.UseVisualStyleBackColor = true;
            this.KorbButton.Click += new System.EventHandler(this.KorbButton_Click);
            // 
            // JsonKorvenButton
            // 
            this.structuresExtender.SetAttributeName(this.JsonKorvenButton, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonKorvenButton, null);
            this.structuresExtender.SetBindPropertyName(this.JsonKorvenButton, null);
            this.JsonKorvenButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.JsonKorvenButton.Location = new System.Drawing.Point(663, 18);
            this.JsonKorvenButton.Name = "JsonKorvenButton";
            this.JsonKorvenButton.Size = new System.Drawing.Size(75, 39);
            this.JsonKorvenButton.TabIndex = 62;
            this.JsonKorvenButton.Text = "Test";
            this.JsonKorvenButton.UseVisualStyleBackColor = true;
            this.JsonKorvenButton.Click += new System.EventHandler(this.JsonKorvenButton_Click);
            // 
            // checkBoxResetJson
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxResetJson, "ResetJson");
            this.structuresExtender.SetAttributeTypeName(this.checkBoxResetJson, "Integer");
            this.checkBoxResetJson.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxResetJson, null);
            this.checkBoxResetJson.Checked = true;
            this.checkBoxResetJson.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxResetJson.Location = new System.Drawing.Point(484, 39);
            this.checkBoxResetJson.Name = "checkBoxResetJson";
            this.checkBoxResetJson.Size = new System.Drawing.Size(101, 17);
            this.checkBoxResetJson.TabIndex = 59;
            this.checkBoxResetJson.Text = "Reset na export";
            this.checkBoxResetJson.UseVisualStyleBackColor = true;
            // 
            // JsonFloorButton
            // 
            this.structuresExtender.SetAttributeName(this.JsonFloorButton, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonFloorButton, null);
            this.structuresExtender.SetBindPropertyName(this.JsonFloorButton, null);
            this.JsonFloorButton.Location = new System.Drawing.Point(582, 66);
            this.JsonFloorButton.Name = "JsonFloorButton";
            this.JsonFloorButton.Size = new System.Drawing.Size(75, 39);
            this.JsonFloorButton.TabIndex = 61;
            this.JsonFloorButton.Text = "Floor";
            this.JsonFloorButton.UseVisualStyleBackColor = true;
            this.JsonFloorButton.Click += new System.EventHandler(this.JsonFloorButton_Click);
            // 
            // checkBoxVisualizeJson
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxVisualizeJson, "VisualizeJson");
            this.structuresExtender.SetAttributeTypeName(this.checkBoxVisualizeJson, "Integer");
            this.checkBoxVisualizeJson.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxVisualizeJson, null);
            this.checkBoxVisualizeJson.Checked = true;
            this.checkBoxVisualizeJson.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVisualizeJson.Location = new System.Drawing.Point(484, 16);
            this.checkBoxVisualizeJson.Name = "checkBoxVisualizeJson";
            this.checkBoxVisualizeJson.Size = new System.Drawing.Size(102, 17);
            this.checkBoxVisualizeJson.TabIndex = 58;
            this.checkBoxVisualizeJson.Text = "Visualizeer wap.";
            this.checkBoxVisualizeJson.UseVisualStyleBackColor = true;
            this.checkBoxVisualizeJson.CheckedChanged += new System.EventHandler(this.checkBoxVisualizeJson_CheckedChanged);
            // 
            // RadJsonPerFase
            // 
            this.structuresExtender.SetAttributeName(this.RadJsonPerFase, null);
            this.structuresExtender.SetAttributeTypeName(this.RadJsonPerFase, null);
            this.RadJsonPerFase.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadJsonPerFase, null);
            this.RadJsonPerFase.Checked = true;
            this.RadJsonPerFase.Location = new System.Drawing.Point(210, 27);
            this.RadJsonPerFase.Name = "RadJsonPerFase";
            this.RadJsonPerFase.Size = new System.Drawing.Size(125, 17);
            this.RadJsonPerFase.TabIndex = 53;
            this.RadJsonPerFase.TabStop = true;
            this.RadJsonPerFase.Text = "Per fase ��n submap";
            this.RadJsonPerFase.UseVisualStyleBackColor = true;
            this.RadJsonPerFase.CheckedChanged += new System.EventHandler(this.RadJsonPerFase_CheckedChanged);
            // 
            // RadJsonAlles
            // 
            this.structuresExtender.SetAttributeName(this.RadJsonAlles, null);
            this.structuresExtender.SetAttributeTypeName(this.RadJsonAlles, null);
            this.RadJsonAlles.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadJsonAlles, null);
            this.RadJsonAlles.Location = new System.Drawing.Point(78, 27);
            this.RadJsonAlles.Name = "RadJsonAlles";
            this.RadJsonAlles.Size = new System.Drawing.Size(102, 17);
            this.RadJsonAlles.TabIndex = 52;
            this.RadJsonAlles.Text = "Alles in ��n map";
            this.RadJsonAlles.UseVisualStyleBackColor = true;
            this.RadJsonAlles.CheckedChanged += new System.EventHandler(this.RadJsonAlles_CheckedChanged);
            // 
            // VMeshButton
            // 
            this.structuresExtender.SetAttributeName(this.VMeshButton, null);
            this.structuresExtender.SetAttributeTypeName(this.VMeshButton, null);
            this.structuresExtender.SetBindPropertyName(this.VMeshButton, null);
            this.VMeshButton.Location = new System.Drawing.Point(420, 66);
            this.VMeshButton.Name = "VMeshButton";
            this.VMeshButton.Size = new System.Drawing.Size(75, 39);
            this.VMeshButton.TabIndex = 60;
            this.VMeshButton.Text = "V-mesh";
            this.VMeshButton.UseVisualStyleBackColor = true;
            this.VMeshButton.Click += new System.EventHandler(this.VMeshButton_Click);
            // 
            // StrMeshButton
            // 
            this.structuresExtender.SetAttributeName(this.StrMeshButton, null);
            this.structuresExtender.SetAttributeTypeName(this.StrMeshButton, null);
            this.structuresExtender.SetBindPropertyName(this.StrMeshButton, null);
            this.StrMeshButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.StrMeshButton.Location = new System.Drawing.Point(339, 66);
            this.StrMeshButton.Name = "StrMeshButton";
            this.StrMeshButton.Size = new System.Drawing.Size(75, 39);
            this.StrMeshButton.TabIndex = 59;
            this.StrMeshButton.Text = "STR-mesh";
            this.StrMeshButton.UseVisualStyleBackColor = true;
            this.StrMeshButton.Click += new System.EventHandler(this.StrMeshButton_Click);
            // 
            // JsonHrspButton
            // 
            this.structuresExtender.SetAttributeName(this.JsonHrspButton, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonHrspButton, null);
            this.structuresExtender.SetBindPropertyName(this.JsonHrspButton, null);
            this.JsonHrspButton.Location = new System.Drawing.Point(177, 66);
            this.JsonHrspButton.Name = "JsonHrspButton";
            this.JsonHrspButton.Size = new System.Drawing.Size(75, 39);
            this.JsonHrspButton.TabIndex = 58;
            this.JsonHrspButton.Text = "Haarspeld";
            this.JsonHrspButton.UseVisualStyleBackColor = true;
            this.JsonHrspButton.Click += new System.EventHandler(this.JsonHrspButton_Click);
            // 
            // JsonStekrekButton
            // 
            this.structuresExtender.SetAttributeName(this.JsonStekrekButton, null);
            this.structuresExtender.SetAttributeTypeName(this.JsonStekrekButton, null);
            this.structuresExtender.SetBindPropertyName(this.JsonStekrekButton, null);
            this.JsonStekrekButton.Location = new System.Drawing.Point(96, 66);
            this.JsonStekrekButton.Name = "JsonStekrekButton";
            this.JsonStekrekButton.Size = new System.Drawing.Size(75, 39);
            this.JsonStekrekButton.TabIndex = 57;
            this.JsonStekrekButton.Text = "Stekrekken";
            this.JsonStekrekButton.UseVisualStyleBackColor = true;
            this.JsonStekrekButton.Click += new System.EventHandler(this.JsonStekrekButton_Click);
            // 
            // button1
            // 
            this.structuresExtender.SetAttributeName(this.button1, null);
            this.structuresExtender.SetAttributeTypeName(this.button1, null);
            this.structuresExtender.SetBindPropertyName(this.button1, null);
            this.button1.Location = new System.Drawing.Point(258, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 39);
            this.button1.TabIndex = 56;
            this.button1.Text = "Wandnetten";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Export
            // 
            this.structuresExtender.SetAttributeName(this.Export, null);
            this.structuresExtender.SetAttributeTypeName(this.Export, null);
            this.structuresExtender.SetBindPropertyName(this.Export, null);
            this.Export.Controls.Add(this.RadPerFase);
            this.Export.Controls.Add(this.RadAlles);
            this.Export.Controls.Add(this.btnBVBS);
            this.Export.Location = new System.Drawing.Point(60, 143);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(447, 117);
            this.Export.TabIndex = 59;
            this.Export.TabStop = false;
            this.Export.Text = "BVBS";
            // 
            // RadPerFase
            // 
            this.structuresExtender.SetAttributeName(this.RadPerFase, null);
            this.structuresExtender.SetAttributeTypeName(this.RadPerFase, null);
            this.RadPerFase.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadPerFase, null);
            this.RadPerFase.Checked = true;
            this.RadPerFase.Location = new System.Drawing.Point(215, 22);
            this.RadPerFase.Name = "RadPerFase";
            this.RadPerFase.Size = new System.Drawing.Size(125, 17);
            this.RadPerFase.TabIndex = 15;
            this.RadPerFase.TabStop = true;
            this.RadPerFase.Text = "Per fase ��n submap";
            this.RadPerFase.UseVisualStyleBackColor = true;
            this.RadPerFase.CheckedChanged += new System.EventHandler(this.RadPerFase_CheckedChanged);
            // 
            // RadAlles
            // 
            this.structuresExtender.SetAttributeName(this.RadAlles, null);
            this.structuresExtender.SetAttributeTypeName(this.RadAlles, null);
            this.RadAlles.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadAlles, null);
            this.RadAlles.Location = new System.Drawing.Point(83, 22);
            this.RadAlles.Name = "RadAlles";
            this.RadAlles.Size = new System.Drawing.Size(102, 17);
            this.RadAlles.TabIndex = 14;
            this.RadAlles.Text = "Alles in ��n map";
            this.RadAlles.UseVisualStyleBackColor = true;
            this.RadAlles.CheckedChanged += new System.EventHandler(this.RadAlles_CheckedChanged);
            // 
            // btnBVBS
            // 
            this.structuresExtender.SetAttributeName(this.btnBVBS, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBVBS, null);
            this.structuresExtender.SetBindPropertyName(this.btnBVBS, null);
            this.btnBVBS.Location = new System.Drawing.Point(38, 51);
            this.btnBVBS.Name = "btnBVBS";
            this.btnBVBS.Size = new System.Drawing.Size(330, 54);
            this.btnBVBS.TabIndex = 1;
            this.btnBVBS.Text = "BVBS exporteren (van geselecteerde onderdelen)";
            this.btnBVBS.UseVisualStyleBackColor = true;
            this.btnBVBS.Click += new System.EventHandler(this.BtnBVBS_Click);
            // 
            // groupBox6
            // 
            this.structuresExtender.SetAttributeName(this.groupBox6, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBox6, null);
            this.structuresExtender.SetBindPropertyName(this.groupBox6, null);
            this.groupBox6.Controls.Add(this.CheckBoxSetKleuren);
            this.groupBox6.Controls.Add(this.checkBoxReset);
            this.groupBox6.Controls.Add(this.checkBoxVisualize);
            this.groupBox6.Controls.Add(this.BtnIFC);
            this.groupBox6.Location = new System.Drawing.Point(60, 281);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(447, 101);
            this.groupBox6.TabIndex = 58;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "IFC ";
            // 
            // CheckBoxSetKleuren
            // 
            this.structuresExtender.SetAttributeName(this.CheckBoxSetKleuren, "Colors");
            this.structuresExtender.SetAttributeTypeName(this.CheckBoxSetKleuren, "Integer");
            this.CheckBoxSetKleuren.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CheckBoxSetKleuren, null);
            this.CheckBoxSetKleuren.Checked = true;
            this.CheckBoxSetKleuren.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxSetKleuren.Location = new System.Drawing.Point(276, 21);
            this.CheckBoxSetKleuren.Name = "CheckBoxSetKleuren";
            this.CheckBoxSetKleuren.Size = new System.Drawing.Size(81, 17);
            this.CheckBoxSetKleuren.TabIndex = 58;
            this.CheckBoxSetKleuren.Text = "Set Kleuren";
            this.CheckBoxSetKleuren.UseVisualStyleBackColor = true;
            // 
            // checkBoxReset
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxReset, "Reset");
            this.structuresExtender.SetAttributeTypeName(this.checkBoxReset, "Integer");
            this.checkBoxReset.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxReset, null);
            this.checkBoxReset.Checked = true;
            this.checkBoxReset.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxReset.Location = new System.Drawing.Point(276, 69);
            this.checkBoxReset.Name = "checkBoxReset";
            this.checkBoxReset.Size = new System.Drawing.Size(127, 17);
            this.checkBoxReset.TabIndex = 57;
            this.checkBoxReset.Text = "Reset wap. na export";
            this.checkBoxReset.UseVisualStyleBackColor = true;
            // 
            // checkBoxVisualize
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxVisualize, "Visualize");
            this.structuresExtender.SetAttributeTypeName(this.checkBoxVisualize, "Integer");
            this.checkBoxVisualize.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxVisualize, null);
            this.checkBoxVisualize.Checked = true;
            this.checkBoxVisualize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVisualize.Location = new System.Drawing.Point(276, 46);
            this.checkBoxVisualize.Name = "checkBoxVisualize";
            this.checkBoxVisualize.Size = new System.Drawing.Size(102, 17);
            this.checkBoxVisualize.TabIndex = 22;
            this.checkBoxVisualize.Text = "Visualizeer wap.";
            this.checkBoxVisualize.UseVisualStyleBackColor = true;
            // 
            // BtnIFC
            // 
            this.structuresExtender.SetAttributeName(this.BtnIFC, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnIFC, null);
            this.structuresExtender.SetBindPropertyName(this.BtnIFC, null);
            this.BtnIFC.Location = new System.Drawing.Point(38, 28);
            this.BtnIFC.Name = "BtnIFC";
            this.BtnIFC.Size = new System.Drawing.Size(223, 46);
            this.BtnIFC.TabIndex = 20;
            this.BtnIFC.Text = "IFC Exporteren";
            this.BtnIFC.UseVisualStyleBackColor = true;
            this.BtnIFC.Click += new System.EventHandler(this.BtnIFC_Click);
            // 
            // groupBoxPDF
            // 
            this.structuresExtender.SetAttributeName(this.groupBoxPDF, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBoxPDF, null);
            this.structuresExtender.SetBindPropertyName(this.groupBoxPDF, null);
            this.groupBoxPDF.Controls.Add(this.BtnMergePlotfiles);
            this.groupBoxPDF.Controls.Add(this.CbPdfG);
            this.groupBoxPDF.Controls.Add(this.CbPdfC);
            this.groupBoxPDF.Controls.Add(this.BtnPdf);
            this.groupBoxPDF.Location = new System.Drawing.Point(60, 406);
            this.groupBoxPDF.Name = "groupBoxPDF";
            this.groupBoxPDF.Size = new System.Drawing.Size(447, 108);
            this.groupBoxPDF.TabIndex = 22;
            this.groupBoxPDF.TabStop = false;
            this.groupBoxPDF.Text = "PDF Plotfiles";
            // 
            // BtnMergePlotfiles
            // 
            this.structuresExtender.SetAttributeName(this.BtnMergePlotfiles, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnMergePlotfiles, null);
            this.structuresExtender.SetBindPropertyName(this.BtnMergePlotfiles, null);
            this.BtnMergePlotfiles.Location = new System.Drawing.Point(280, 46);
            this.BtnMergePlotfiles.Name = "BtnMergePlotfiles";
            this.BtnMergePlotfiles.Size = new System.Drawing.Size(93, 47);
            this.BtnMergePlotfiles.TabIndex = 21;
            this.BtnMergePlotfiles.Text = "Merge Plotfiles";
            this.BtnMergePlotfiles.UseVisualStyleBackColor = true;
            this.BtnMergePlotfiles.Click += new System.EventHandler(this.BtnMergePlotfiles_Click);
            // 
            // CbPdfG
            // 
            this.structuresExtender.SetAttributeName(this.CbPdfG, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPdfG, null);
            this.CbPdfG.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPdfG, null);
            this.CbPdfG.Checked = true;
            this.CbPdfG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPdfG.Location = new System.Drawing.Point(220, 20);
            this.CbPdfG.Name = "CbPdfG";
            this.CbPdfG.Size = new System.Drawing.Size(90, 17);
            this.CbPdfG.TabIndex = 20;
            this.CbPdfG.Text = "G-tekeningen";
            this.CbPdfG.UseVisualStyleBackColor = true;
            // 
            // CbPdfC
            // 
            this.structuresExtender.SetAttributeName(this.CbPdfC, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPdfC, null);
            this.CbPdfC.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPdfC, null);
            this.CbPdfC.Checked = true;
            this.CbPdfC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPdfC.Location = new System.Drawing.Point(107, 20);
            this.CbPdfC.Name = "CbPdfC";
            this.CbPdfC.Size = new System.Drawing.Size(89, 17);
            this.CbPdfC.TabIndex = 19;
            this.CbPdfC.Text = "C-tekeningen";
            this.CbPdfC.UseVisualStyleBackColor = true;
            // 
            // BtnPdf
            // 
            this.structuresExtender.SetAttributeName(this.BtnPdf, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnPdf, null);
            this.structuresExtender.SetBindPropertyName(this.BtnPdf, null);
            this.BtnPdf.Location = new System.Drawing.Point(38, 46);
            this.BtnPdf.Name = "BtnPdf";
            this.BtnPdf.Size = new System.Drawing.Size(222, 47);
            this.BtnPdf.TabIndex = 18;
            this.BtnPdf.Text = "Maak PDF Plotfiles";
            this.BtnPdf.UseVisualStyleBackColor = true;
            this.BtnPdf.Click += new System.EventHandler(this.BtnPdf_Click);
            // 
            // TxRadbutton
            // 
            this.structuresExtender.SetAttributeName(this.TxRadbutton, "Radbutton");
            this.structuresExtender.SetAttributeTypeName(this.TxRadbutton, "String");
            this.structuresExtender.SetBindPropertyName(this.TxRadbutton, null);
            this.TxRadbutton.Location = new System.Drawing.Point(7, 171);
            this.TxRadbutton.Name = "TxRadbutton";
            this.TxRadbutton.Size = new System.Drawing.Size(27, 20);
            this.TxRadbutton.TabIndex = 16;
            this.TxRadbutton.Text = "A";
            this.TxRadbutton.TextChanged += new System.EventHandler(this.TxRadbutton_TextChanged);
            // 
            // lblNummering
            // 
            this.structuresExtender.SetAttributeName(this.lblNummering, null);
            this.structuresExtender.SetAttributeTypeName(this.lblNummering, null);
            this.lblNummering.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblNummering, null);
            this.lblNummering.Location = new System.Drawing.Point(306, 29);
            this.lblNummering.Name = "lblNummering";
            this.lblNummering.Size = new System.Drawing.Size(0, 13);
            this.lblNummering.TabIndex = 12;
            // 
            // TxKlasse
            // 
            this.structuresExtender.SetAttributeName(this.TxKlasse, "Klasse");
            this.structuresExtender.SetAttributeTypeName(this.TxKlasse, "String");
            this.structuresExtender.SetBindPropertyName(this.TxKlasse, null);
            this.TxKlasse.Location = new System.Drawing.Point(7, 145);
            this.TxKlasse.Name = "TxKlasse";
            this.TxKlasse.Size = new System.Drawing.Size(27, 20);
            this.TxKlasse.TabIndex = 0;
            this.TxKlasse.Text = "4";
            this.TxKlasse.TextChanged += new System.EventHandler(this.TxKlasse_TextChanged);
            // 
            // pictureBox4
            // 
            this.structuresExtender.SetAttributeName(this.pictureBox4, null);
            this.structuresExtender.SetAttributeTypeName(this.pictureBox4, null);
            this.structuresExtender.SetBindPropertyName(this.pictureBox4, null);
            this.pictureBox4.Image = global::BS_Export.Properties.Resources.logo_bs_copy;
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(569, 437);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(274, 152);
            this.pictureBox4.TabIndex = 68;
            this.pictureBox4.TabStop = false;
            // 
            // BVBS
            // 
            this.structuresExtender.SetAttributeName(this.BVBS, null);
            this.structuresExtender.SetAttributeTypeName(this.BVBS, null);
            this.structuresExtender.SetBindPropertyName(this.BVBS, null);
            this.BVBS.Controls.Add(this.PutButton);
            this.BVBS.Controls.Add(this.RondeKolomButton);
            this.BVBS.Controls.Add(this.ZrackButton);
            this.BVBS.Controls.Add(this.PonsButton);
            this.BVBS.Controls.Add(this.BvbsFundDoorvoer);
            this.BVBS.Controls.Add(this.CbKorven4sn);
            this.BVBS.Controls.Add(this.btnKorf4);
            this.BVBS.Controls.Add(this.BtnBalk);
            this.BVBS.Controls.Add(this.CbHrspLos);
            this.BVBS.Controls.Add(this.HrspLosButton);
            this.BVBS.Controls.Add(this.checkBox1);
            this.BVBS.Controls.Add(this.button4);
            this.BVBS.Controls.Add(this.checkBoxLosseKorf);
            this.BVBS.Controls.Add(this.CbStekrek);
            this.BVBS.Controls.Add(this.btnStekrek);
            this.BVBS.Controls.Add(this.CbZ);
            this.BVBS.Controls.Add(this.BtnZbars);
            this.BVBS.Controls.Add(this.CbBalken3);
            this.BVBS.Controls.Add(this.btnDekking3snedig);
            this.BVBS.Controls.Add(this.CbBalkenS);
            this.BVBS.Controls.Add(this.btnDekkingSchuin);
            this.BVBS.Controls.Add(this.CbBalkenD);
            this.BVBS.Controls.Add(this.btnBalkenD);
            this.BVBS.Controls.Add(this.CbPoeren);
            this.BVBS.Controls.Add(this.CbMatOpMaat);
            this.BVBS.Controls.Add(this.CbKorvenHrsp);
            this.BVBS.Controls.Add(this.CbNettenSup);
            this.BVBS.Controls.Add(this.CbKorvenLos);
            this.BVBS.Controls.Add(this.CbBalken3sn);
            this.BVBS.Controls.Add(this.CbKorven3sn);
            this.BVBS.Controls.Add(this.CbBalkenSchuin);
            this.BVBS.Controls.Add(this.CbKorvenSchuin);
            this.BVBS.Controls.Add(this.CbBalken);
            this.BVBS.Controls.Add(this.CbKorven);
            this.BVBS.Controls.Add(this.BtnPoer);
            this.BVBS.Controls.Add(this.btnBalken3);
            this.BVBS.Controls.Add(this.btnKorf3);
            this.BVBS.Controls.Add(this.btnMatOpMaat);
            this.BVBS.Controls.Add(this.btnNetSup);
            this.BVBS.Controls.Add(this.btnKorfHrsp);
            this.BVBS.Controls.Add(this.btnKorfLos);
            this.BVBS.Controls.Add(this.btnBalkenSchuin);
            this.BVBS.Controls.Add(this.btnBalken);
            this.BVBS.Controls.Add(this.btnKorvenSchuin);
            this.BVBS.Controls.Add(this.btnKorf);
            this.BVBS.Location = new System.Drawing.Point(4, 22);
            this.BVBS.Name = "BVBS";
            this.BVBS.Padding = new System.Windows.Forms.Padding(3);
            this.BVBS.Size = new System.Drawing.Size(816, 566);
            this.BVBS.TabIndex = 5;
            this.BVBS.Text = "BVBS per Groep";
            this.BVBS.UseVisualStyleBackColor = true;
            this.BVBS.Click += new System.EventHandler(this.BVBS_Click);
            // 
            // PutButton
            // 
            this.structuresExtender.SetAttributeName(this.PutButton, null);
            this.structuresExtender.SetAttributeTypeName(this.PutButton, null);
            this.structuresExtender.SetBindPropertyName(this.PutButton, null);
            this.PutButton.Location = new System.Drawing.Point(56, 475);
            this.PutButton.Name = "PutButton";
            this.PutButton.Size = new System.Drawing.Size(147, 32);
            this.PutButton.TabIndex = 89;
            this.PutButton.Text = "Put";
            this.PutButton.UseVisualStyleBackColor = true;
            this.PutButton.Visible = false;
            // 
            // RondeKolomButton
            // 
            this.structuresExtender.SetAttributeName(this.RondeKolomButton, null);
            this.structuresExtender.SetAttributeTypeName(this.RondeKolomButton, null);
            this.structuresExtender.SetBindPropertyName(this.RondeKolomButton, null);
            this.RondeKolomButton.Location = new System.Drawing.Point(319, 358);
            this.RondeKolomButton.Name = "RondeKolomButton";
            this.RondeKolomButton.Size = new System.Drawing.Size(171, 32);
            this.RondeKolomButton.TabIndex = 88;
            this.RondeKolomButton.Text = "Ronde Kolom + Nok + Put";
            this.RondeKolomButton.UseVisualStyleBackColor = true;
            this.RondeKolomButton.Visible = false;
            this.RondeKolomButton.Click += new System.EventHandler(this.RondeKolomButton_Click);
            // 
            // ZrackButton
            // 
            this.structuresExtender.SetAttributeName(this.ZrackButton, null);
            this.structuresExtender.SetAttributeTypeName(this.ZrackButton, null);
            this.structuresExtender.SetBindPropertyName(this.ZrackButton, null);
            this.ZrackButton.Location = new System.Drawing.Point(480, 475);
            this.ZrackButton.Name = "ZrackButton";
            this.ZrackButton.Size = new System.Drawing.Size(148, 32);
            this.ZrackButton.TabIndex = 87;
            this.ZrackButton.Text = "Z Rack";
            this.ZrackButton.UseVisualStyleBackColor = true;
            this.ZrackButton.Visible = false;
            this.ZrackButton.Click += new System.EventHandler(this.ZrackButton_Click);
            // 
            // PonsButton
            // 
            this.structuresExtender.SetAttributeName(this.PonsButton, null);
            this.structuresExtender.SetAttributeTypeName(this.PonsButton, null);
            this.structuresExtender.SetBindPropertyName(this.PonsButton, null);
            this.PonsButton.Location = new System.Drawing.Point(319, 475);
            this.PonsButton.Name = "PonsButton";
            this.PonsButton.Size = new System.Drawing.Size(148, 32);
            this.PonsButton.TabIndex = 86;
            this.PonsButton.Text = "Pons";
            this.PonsButton.UseVisualStyleBackColor = true;
            this.PonsButton.Visible = false;
            this.PonsButton.Click += new System.EventHandler(this.PonsButton_Click);
            // 
            // BvbsFundDoorvoer
            // 
            this.structuresExtender.SetAttributeName(this.BvbsFundDoorvoer, null);
            this.structuresExtender.SetAttributeTypeName(this.BvbsFundDoorvoer, null);
            this.structuresExtender.SetBindPropertyName(this.BvbsFundDoorvoer, null);
            this.BvbsFundDoorvoer.Location = new System.Drawing.Point(57, 437);
            this.BvbsFundDoorvoer.Name = "BvbsFundDoorvoer";
            this.BvbsFundDoorvoer.Size = new System.Drawing.Size(147, 32);
            this.BvbsFundDoorvoer.TabIndex = 85;
            this.BvbsFundDoorvoer.Text = "Fund. Doorvoer";
            this.BvbsFundDoorvoer.UseVisualStyleBackColor = true;
            this.BvbsFundDoorvoer.Visible = false;
            this.BvbsFundDoorvoer.Click += new System.EventHandler(this.BvbsFundDoorvoer_Click);
            // 
            // CbKorven4sn
            // 
            this.structuresExtender.SetAttributeName(this.CbKorven4sn, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorven4sn, null);
            this.CbKorven4sn.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorven4sn, null);
            this.CbKorven4sn.Checked = true;
            this.CbKorven4sn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorven4sn.Location = new System.Drawing.Point(568, 106);
            this.CbKorven4sn.Name = "CbKorven4sn";
            this.CbKorven4sn.Size = new System.Drawing.Size(15, 14);
            this.CbKorven4sn.TabIndex = 84;
            this.CbKorven4sn.UseVisualStyleBackColor = true;
            this.CbKorven4sn.Visible = false;
            // 
            // btnKorf4
            // 
            this.structuresExtender.SetAttributeName(this.btnKorf4, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorf4, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorf4, null);
            this.btnKorf4.Location = new System.Drawing.Point(590, 98);
            this.btnKorf4.Name = "btnKorf4";
            this.btnKorf4.Size = new System.Drawing.Size(151, 32);
            this.btnKorf4.TabIndex = 83;
            this.btnKorf4.Text = "K4S - Korf 4-snedig";
            this.btnKorf4.UseVisualStyleBackColor = true;
            this.btnKorf4.Click += new System.EventHandler(this.btnKorf4_Click);
            // 
            // BtnBalk
            // 
            this.structuresExtender.SetAttributeName(this.BtnBalk, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnBalk, null);
            this.structuresExtender.SetBindPropertyName(this.BtnBalk, null);
            this.BtnBalk.Location = new System.Drawing.Point(319, 437);
            this.BtnBalk.Name = "BtnBalk";
            this.BtnBalk.Size = new System.Drawing.Size(148, 32);
            this.BtnBalk.TabIndex = 82;
            this.BtnBalk.Text = "Balken";
            this.BtnBalk.UseVisualStyleBackColor = true;
            this.BtnBalk.Visible = false;
            this.BtnBalk.Click += new System.EventHandler(this.BtnBalk_Click);
            // 
            // CbHrspLos
            // 
            this.structuresExtender.SetAttributeName(this.CbHrspLos, null);
            this.structuresExtender.SetAttributeTypeName(this.CbHrspLos, null);
            this.CbHrspLos.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbHrspLos, null);
            this.CbHrspLos.Checked = true;
            this.CbHrspLos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbHrspLos.Location = new System.Drawing.Point(299, 236);
            this.CbHrspLos.Name = "CbHrspLos";
            this.CbHrspLos.Size = new System.Drawing.Size(15, 14);
            this.CbHrspLos.TabIndex = 81;
            this.CbHrspLos.UseVisualStyleBackColor = true;
            this.CbHrspLos.Visible = false;
            // 
            // HrspLosButton
            // 
            this.structuresExtender.SetAttributeName(this.HrspLosButton, null);
            this.structuresExtender.SetAttributeTypeName(this.HrspLosButton, null);
            this.structuresExtender.SetBindPropertyName(this.HrspLosButton, null);
            this.HrspLosButton.Location = new System.Drawing.Point(319, 228);
            this.HrspLosButton.Name = "HrspLosButton";
            this.HrspLosButton.Size = new System.Drawing.Size(148, 32);
            this.HrspLosButton.TabIndex = 80;
            this.HrspLosButton.Text = "Hrsp Los";
            this.HrspLosButton.UseVisualStyleBackColor = true;
            this.HrspLosButton.Click += new System.EventHandler(this.HrspLosButton_Click);
            // 
            // checkBox1
            // 
            this.structuresExtender.SetAttributeName(this.checkBox1, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBox1, null);
            this.checkBox1.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox1, null);
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(299, 107);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 79;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // button4
            // 
            this.structuresExtender.SetAttributeName(this.button4, null);
            this.structuresExtender.SetAttributeTypeName(this.button4, null);
            this.structuresExtender.SetBindPropertyName(this.button4, null);
            this.button4.Location = new System.Drawing.Point(319, 98);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(148, 32);
            this.button4.TabIndex = 78;
            this.button4.Text = "Stekrek Los";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // checkBoxLosseKorf
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxLosseKorf, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxLosseKorf, null);
            this.checkBoxLosseKorf.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxLosseKorf, null);
            this.checkBoxLosseKorf.ForeColor = System.Drawing.Color.Red;
            this.checkBoxLosseKorf.Location = new System.Drawing.Point(56, 20);
            this.checkBoxLosseKorf.Name = "checkBoxLosseKorf";
            this.checkBoxLosseKorf.Size = new System.Drawing.Size(491, 17);
            this.checkBoxLosseKorf.TabIndex = 77;
            this.checkBoxLosseKorf.Text = "BVBS exporteren als losse korf (voor de KR, KS en K3S wordt een \'L\' geplaatst in " +
    "het abs bestand)";
            this.checkBoxLosseKorf.UseVisualStyleBackColor = true;
            // 
            // CbStekrek
            // 
            this.structuresExtender.SetAttributeName(this.CbStekrek, null);
            this.structuresExtender.SetAttributeTypeName(this.CbStekrek, null);
            this.CbStekrek.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbStekrek, null);
            this.CbStekrek.Checked = true;
            this.CbStekrek.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbStekrek.Location = new System.Drawing.Point(36, 107);
            this.CbStekrek.Name = "CbStekrek";
            this.CbStekrek.Size = new System.Drawing.Size(15, 14);
            this.CbStekrek.TabIndex = 76;
            this.CbStekrek.UseVisualStyleBackColor = true;
            this.CbStekrek.Visible = false;
            // 
            // btnStekrek
            // 
            this.structuresExtender.SetAttributeName(this.btnStekrek, null);
            this.structuresExtender.SetAttributeTypeName(this.btnStekrek, null);
            this.structuresExtender.SetBindPropertyName(this.btnStekrek, null);
            this.btnStekrek.Location = new System.Drawing.Point(56, 98);
            this.btnStekrek.Name = "btnStekrek";
            this.btnStekrek.Size = new System.Drawing.Size(148, 32);
            this.btnStekrek.TabIndex = 75;
            this.btnStekrek.Text = "Stekrek";
            this.btnStekrek.UseVisualStyleBackColor = true;
            this.btnStekrek.Click += new System.EventHandler(this.btnStekrek_Click);
            // 
            // CbZ
            // 
            this.structuresExtender.SetAttributeName(this.CbZ, null);
            this.structuresExtender.SetAttributeTypeName(this.CbZ, null);
            this.CbZ.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbZ, null);
            this.CbZ.Checked = true;
            this.CbZ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbZ.Location = new System.Drawing.Point(37, 406);
            this.CbZ.Name = "CbZ";
            this.CbZ.Size = new System.Drawing.Size(15, 14);
            this.CbZ.TabIndex = 74;
            this.CbZ.UseVisualStyleBackColor = true;
            this.CbZ.Visible = false;
            // 
            // BtnZbars
            // 
            this.structuresExtender.SetAttributeName(this.BtnZbars, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnZbars, null);
            this.structuresExtender.SetBindPropertyName(this.BtnZbars, null);
            this.BtnZbars.Location = new System.Drawing.Point(56, 397);
            this.BtnZbars.Name = "BtnZbars";
            this.BtnZbars.Size = new System.Drawing.Size(147, 32);
            this.BtnZbars.TabIndex = 73;
            this.BtnZbars.Text = "Z - Los";
            this.BtnZbars.UseVisualStyleBackColor = true;
            this.BtnZbars.Visible = false;
            this.BtnZbars.Click += new System.EventHandler(this.BtnZbars_Click_1);
            // 
            // CbBalken3
            // 
            this.structuresExtender.SetAttributeName(this.CbBalken3, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalken3, null);
            this.CbBalken3.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalken3, null);
            this.CbBalken3.Checked = true;
            this.CbBalken3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalken3.Location = new System.Drawing.Point(568, 189);
            this.CbBalken3.Name = "CbBalken3";
            this.CbBalken3.Size = new System.Drawing.Size(15, 14);
            this.CbBalken3.TabIndex = 72;
            this.CbBalken3.UseVisualStyleBackColor = true;
            this.CbBalken3.Visible = false;
            // 
            // btnDekking3snedig
            // 
            this.structuresExtender.SetAttributeName(this.btnDekking3snedig, null);
            this.structuresExtender.SetAttributeTypeName(this.btnDekking3snedig, null);
            this.structuresExtender.SetBindPropertyName(this.btnDekking3snedig, null);
            this.btnDekking3snedig.Location = new System.Drawing.Point(590, 181);
            this.btnDekking3snedig.Name = "btnDekking3snedig";
            this.btnDekking3snedig.Size = new System.Drawing.Size(151, 32);
            this.btnDekking3snedig.TabIndex = 71;
            this.btnDekking3snedig.Text = "D3S - Dekking 3-sn.";
            this.btnDekking3snedig.UseVisualStyleBackColor = true;
            this.btnDekking3snedig.Click += new System.EventHandler(this.btnDekking3snedig_Click_1);
            // 
            // CbBalkenS
            // 
            this.structuresExtender.SetAttributeName(this.CbBalkenS, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalkenS, null);
            this.CbBalkenS.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalkenS, null);
            this.CbBalkenS.Checked = true;
            this.CbBalkenS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalkenS.Location = new System.Drawing.Point(299, 188);
            this.CbBalkenS.Name = "CbBalkenS";
            this.CbBalkenS.Size = new System.Drawing.Size(15, 14);
            this.CbBalkenS.TabIndex = 70;
            this.CbBalkenS.UseVisualStyleBackColor = true;
            this.CbBalkenS.Visible = false;
            // 
            // btnDekkingSchuin
            // 
            this.structuresExtender.SetAttributeName(this.btnDekkingSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.btnDekkingSchuin, null);
            this.structuresExtender.SetBindPropertyName(this.btnDekkingSchuin, null);
            this.btnDekkingSchuin.Location = new System.Drawing.Point(319, 181);
            this.btnDekkingSchuin.Name = "btnDekkingSchuin";
            this.btnDekkingSchuin.Size = new System.Drawing.Size(148, 32);
            this.btnDekkingSchuin.TabIndex = 69;
            this.btnDekkingSchuin.Text = "DS - Dekking Schuin";
            this.btnDekkingSchuin.UseVisualStyleBackColor = true;
            this.btnDekkingSchuin.Click += new System.EventHandler(this.btnDekkingSchuin_Click_1);
            // 
            // CbBalkenD
            // 
            this.structuresExtender.SetAttributeName(this.CbBalkenD, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalkenD, null);
            this.CbBalkenD.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalkenD, null);
            this.CbBalkenD.Checked = true;
            this.CbBalkenD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalkenD.Location = new System.Drawing.Point(37, 190);
            this.CbBalkenD.Name = "CbBalkenD";
            this.CbBalkenD.Size = new System.Drawing.Size(15, 14);
            this.CbBalkenD.TabIndex = 68;
            this.CbBalkenD.UseVisualStyleBackColor = true;
            this.CbBalkenD.Visible = false;
            // 
            // btnBalkenD
            // 
            this.structuresExtender.SetAttributeName(this.btnBalkenD, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalkenD, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalkenD, null);
            this.btnBalkenD.Location = new System.Drawing.Point(56, 182);
            this.btnBalkenD.Name = "btnBalkenD";
            this.btnBalkenD.Size = new System.Drawing.Size(147, 32);
            this.btnBalkenD.TabIndex = 67;
            this.btnBalkenD.Text = "DR - Dekking Recht";
            this.btnBalkenD.UseVisualStyleBackColor = true;
            this.btnBalkenD.Click += new System.EventHandler(this.btnBalkenD_Click_1);
            // 
            // CbPoeren
            // 
            this.structuresExtender.SetAttributeName(this.CbPoeren, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPoeren, null);
            this.CbPoeren.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPoeren, null);
            this.CbPoeren.Checked = true;
            this.CbPoeren.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPoeren.Location = new System.Drawing.Point(299, 407);
            this.CbPoeren.Name = "CbPoeren";
            this.CbPoeren.Size = new System.Drawing.Size(15, 14);
            this.CbPoeren.TabIndex = 66;
            this.CbPoeren.UseVisualStyleBackColor = true;
            this.CbPoeren.Visible = false;
            // 
            // CbMatOpMaat
            // 
            this.structuresExtender.SetAttributeName(this.CbMatOpMaat, null);
            this.structuresExtender.SetAttributeTypeName(this.CbMatOpMaat, null);
            this.CbMatOpMaat.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbMatOpMaat, null);
            this.CbMatOpMaat.Checked = true;
            this.CbMatOpMaat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbMatOpMaat.Location = new System.Drawing.Point(570, 404);
            this.CbMatOpMaat.Name = "CbMatOpMaat";
            this.CbMatOpMaat.Size = new System.Drawing.Size(15, 14);
            this.CbMatOpMaat.TabIndex = 65;
            this.CbMatOpMaat.UseVisualStyleBackColor = true;
            this.CbMatOpMaat.Visible = false;
            // 
            // CbKorvenHrsp
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenHrsp, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenHrsp, null);
            this.CbKorvenHrsp.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenHrsp, null);
            this.CbKorvenHrsp.Checked = true;
            this.CbKorvenHrsp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenHrsp.Location = new System.Drawing.Point(36, 238);
            this.CbKorvenHrsp.Name = "CbKorvenHrsp";
            this.CbKorvenHrsp.Size = new System.Drawing.Size(15, 14);
            this.CbKorvenHrsp.TabIndex = 64;
            this.CbKorvenHrsp.UseVisualStyleBackColor = true;
            this.CbKorvenHrsp.Visible = false;
            // 
            // CbNettenSup
            // 
            this.structuresExtender.SetAttributeName(this.CbNettenSup, null);
            this.structuresExtender.SetAttributeTypeName(this.CbNettenSup, null);
            this.CbNettenSup.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbNettenSup, null);
            this.CbNettenSup.Checked = true;
            this.CbNettenSup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbNettenSup.Location = new System.Drawing.Point(37, 368);
            this.CbNettenSup.Name = "CbNettenSup";
            this.CbNettenSup.Size = new System.Drawing.Size(15, 14);
            this.CbNettenSup.TabIndex = 63;
            this.CbNettenSup.UseVisualStyleBackColor = true;
            this.CbNettenSup.Visible = false;
            // 
            // CbKorvenLos
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenLos, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenLos, null);
            this.CbKorvenLos.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenLos, null);
            this.CbKorvenLos.Checked = true;
            this.CbKorvenLos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenLos.Location = new System.Drawing.Point(37, 321);
            this.CbKorvenLos.Name = "CbKorvenLos";
            this.CbKorvenLos.Size = new System.Drawing.Size(15, 14);
            this.CbKorvenLos.TabIndex = 62;
            this.CbKorvenLos.UseVisualStyleBackColor = true;
            this.CbKorvenLos.Visible = false;
            // 
            // CbBalken3sn
            // 
            this.structuresExtender.SetAttributeName(this.CbBalken3sn, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalken3sn, null);
            this.CbBalken3sn.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalken3sn, null);
            this.CbBalken3sn.Location = new System.Drawing.Point(687, 333);
            this.CbBalken3sn.Name = "CbBalken3sn";
            this.CbBalken3sn.Size = new System.Drawing.Size(15, 14);
            this.CbBalken3sn.TabIndex = 61;
            this.CbBalken3sn.UseVisualStyleBackColor = true;
            this.CbBalken3sn.Visible = false;
            // 
            // CbKorven3sn
            // 
            this.structuresExtender.SetAttributeName(this.CbKorven3sn, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorven3sn, null);
            this.CbKorven3sn.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorven3sn, null);
            this.CbKorven3sn.Checked = true;
            this.CbKorven3sn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorven3sn.Location = new System.Drawing.Point(568, 64);
            this.CbKorven3sn.Name = "CbKorven3sn";
            this.CbKorven3sn.Size = new System.Drawing.Size(15, 14);
            this.CbKorven3sn.TabIndex = 60;
            this.CbKorven3sn.UseVisualStyleBackColor = true;
            this.CbKorven3sn.Visible = false;
            // 
            // CbBalkenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.CbBalkenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalkenSchuin, null);
            this.CbBalkenSchuin.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalkenSchuin, null);
            this.CbBalkenSchuin.Location = new System.Drawing.Point(613, 330);
            this.CbBalkenSchuin.Name = "CbBalkenSchuin";
            this.CbBalkenSchuin.Size = new System.Drawing.Size(15, 14);
            this.CbBalkenSchuin.TabIndex = 59;
            this.CbBalkenSchuin.UseVisualStyleBackColor = true;
            this.CbBalkenSchuin.Visible = false;
            // 
            // CbKorvenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenSchuin, null);
            this.CbKorvenSchuin.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenSchuin, null);
            this.CbKorvenSchuin.Checked = true;
            this.CbKorvenSchuin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenSchuin.Location = new System.Drawing.Point(300, 63);
            this.CbKorvenSchuin.Name = "CbKorvenSchuin";
            this.CbKorvenSchuin.Size = new System.Drawing.Size(15, 14);
            this.CbKorvenSchuin.TabIndex = 58;
            this.CbKorvenSchuin.UseVisualStyleBackColor = true;
            this.CbKorvenSchuin.Visible = false;
            // 
            // CbBalken
            // 
            this.structuresExtender.SetAttributeName(this.CbBalken, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalken, null);
            this.CbBalken.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalken, null);
            this.CbBalken.Checked = true;
            this.CbBalken.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalken.Location = new System.Drawing.Point(37, 148);
            this.CbBalken.Name = "CbBalken";
            this.CbBalken.Size = new System.Drawing.Size(15, 14);
            this.CbBalken.TabIndex = 57;
            this.CbBalken.UseVisualStyleBackColor = true;
            this.CbBalken.Visible = false;
            // 
            // CbKorven
            // 
            this.structuresExtender.SetAttributeName(this.CbKorven, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorven, null);
            this.CbKorven.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorven, null);
            this.CbKorven.Checked = true;
            this.CbKorven.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorven.Location = new System.Drawing.Point(37, 64);
            this.CbKorven.Name = "CbKorven";
            this.CbKorven.Size = new System.Drawing.Size(15, 14);
            this.CbKorven.TabIndex = 56;
            this.CbKorven.UseVisualStyleBackColor = true;
            this.CbKorven.Visible = false;
            // 
            // BtnPoer
            // 
            this.structuresExtender.SetAttributeName(this.BtnPoer, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnPoer, null);
            this.structuresExtender.SetBindPropertyName(this.BtnPoer, null);
            this.BtnPoer.Location = new System.Drawing.Point(319, 399);
            this.BtnPoer.Name = "BtnPoer";
            this.BtnPoer.Size = new System.Drawing.Size(171, 32);
            this.BtnPoer.TabIndex = 54;
            this.BtnPoer.Text = "Poeren + Stiepen";
            this.BtnPoer.UseVisualStyleBackColor = true;
            this.BtnPoer.Visible = false;
            this.BtnPoer.Click += new System.EventHandler(this.BtnPoer_Click_1);
            // 
            // btnBalken3
            // 
            this.structuresExtender.SetAttributeName(this.btnBalken3, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalken3, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalken3, null);
            this.btnBalken3.Location = new System.Drawing.Point(675, 350);
            this.btnBalken3.Name = "btnBalken3";
            this.btnBalken3.Size = new System.Drawing.Size(57, 32);
            this.btnBalken3.TabIndex = 53;
            this.btnBalken3.Text = "L3S - Los 3-snedig";
            this.btnBalken3.UseVisualStyleBackColor = true;
            this.btnBalken3.Visible = false;
            this.btnBalken3.Click += new System.EventHandler(this.btnBalken3_Click_1);
            // 
            // btnKorf3
            // 
            this.structuresExtender.SetAttributeName(this.btnKorf3, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorf3, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorf3, null);
            this.btnKorf3.Location = new System.Drawing.Point(590, 56);
            this.btnKorf3.Name = "btnKorf3";
            this.btnKorf3.Size = new System.Drawing.Size(151, 32);
            this.btnKorf3.TabIndex = 52;
            this.btnKorf3.Text = "K3S - Korf 3-snedig";
            this.btnKorf3.UseVisualStyleBackColor = true;
            this.btnKorf3.Click += new System.EventHandler(this.btnKorf3_Click_1);
            // 
            // btnMatOpMaat
            // 
            this.structuresExtender.SetAttributeName(this.btnMatOpMaat, null);
            this.structuresExtender.SetAttributeTypeName(this.btnMatOpMaat, null);
            this.structuresExtender.SetBindPropertyName(this.btnMatOpMaat, null);
            this.btnMatOpMaat.Enabled = false;
            this.btnMatOpMaat.Location = new System.Drawing.Point(590, 396);
            this.btnMatOpMaat.Name = "btnMatOpMaat";
            this.btnMatOpMaat.Size = new System.Drawing.Size(148, 32);
            this.btnMatOpMaat.TabIndex = 51;
            this.btnMatOpMaat.Text = "Mat op Maat";
            this.btnMatOpMaat.UseVisualStyleBackColor = true;
            this.btnMatOpMaat.Visible = false;
            this.btnMatOpMaat.Click += new System.EventHandler(this.btnMatOpMaat_Click);
            // 
            // btnNetSup
            // 
            this.structuresExtender.SetAttributeName(this.btnNetSup, null);
            this.structuresExtender.SetAttributeTypeName(this.btnNetSup, null);
            this.structuresExtender.SetBindPropertyName(this.btnNetSup, null);
            this.btnNetSup.Location = new System.Drawing.Point(56, 359);
            this.btnNetSup.Name = "btnNetSup";
            this.btnNetSup.Size = new System.Drawing.Size(147, 32);
            this.btnNetSup.TabIndex = 50;
            this.btnNetSup.Text = "Netten en Sup";
            this.btnNetSup.UseVisualStyleBackColor = true;
            this.btnNetSup.Visible = false;
            this.btnNetSup.Click += new System.EventHandler(this.btnNetSup_Click_1);
            // 
            // btnKorfHrsp
            // 
            this.structuresExtender.SetAttributeName(this.btnKorfHrsp, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorfHrsp, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorfHrsp, null);
            this.btnKorfHrsp.Location = new System.Drawing.Point(56, 229);
            this.btnKorfHrsp.Name = "btnKorfHrsp";
            this.btnKorfHrsp.Size = new System.Drawing.Size(148, 32);
            this.btnKorfHrsp.TabIndex = 49;
            this.btnKorfHrsp.Text = "Korven Hrsp";
            this.btnKorfHrsp.UseVisualStyleBackColor = true;
            this.btnKorfHrsp.Click += new System.EventHandler(this.btnKorfHrsp_Click_1);
            // 
            // btnKorfLos
            // 
            this.structuresExtender.SetAttributeName(this.btnKorfLos, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorfLos, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorfLos, null);
            this.btnKorfLos.Enabled = false;
            this.btnKorfLos.Location = new System.Drawing.Point(56, 312);
            this.btnKorfLos.Name = "btnKorfLos";
            this.btnKorfLos.Size = new System.Drawing.Size(147, 32);
            this.btnKorfLos.TabIndex = 48;
            this.btnKorfLos.Text = "Korven Los";
            this.btnKorfLos.UseVisualStyleBackColor = true;
            this.btnKorfLos.Visible = false;
            this.btnKorfLos.Click += new System.EventHandler(this.btnKorfLos_Click_1);
            // 
            // btnBalkenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.btnBalkenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalkenSchuin, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalkenSchuin, null);
            this.btnBalkenSchuin.Location = new System.Drawing.Point(601, 350);
            this.btnBalkenSchuin.Name = "btnBalkenSchuin";
            this.btnBalkenSchuin.Size = new System.Drawing.Size(44, 32);
            this.btnBalkenSchuin.TabIndex = 47;
            this.btnBalkenSchuin.Text = "LS - Los Schuin";
            this.btnBalkenSchuin.UseVisualStyleBackColor = true;
            this.btnBalkenSchuin.Visible = false;
            this.btnBalkenSchuin.Click += new System.EventHandler(this.btnBalkenSchuin_Click_1);
            // 
            // btnBalken
            // 
            this.structuresExtender.SetAttributeName(this.btnBalken, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalken, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalken, null);
            this.btnBalken.Location = new System.Drawing.Point(56, 140);
            this.btnBalken.Name = "btnBalken";
            this.btnBalken.Size = new System.Drawing.Size(685, 32);
            this.btnBalken.TabIndex = 46;
            this.btnBalken.Text = "Balken LR - Los Recht    |    LS - Los Schuin    |    L3S - Los 3-snedig  |  L4S " +
    "- Los 4-snedig";
            this.btnBalken.UseVisualStyleBackColor = true;
            this.btnBalken.Click += new System.EventHandler(this.btnBalken_Click_1);
            // 
            // btnKorvenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.btnKorvenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorvenSchuin, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorvenSchuin, null);
            this.btnKorvenSchuin.Location = new System.Drawing.Point(319, 55);
            this.btnKorvenSchuin.Name = "btnKorvenSchuin";
            this.btnKorvenSchuin.Size = new System.Drawing.Size(148, 32);
            this.btnKorvenSchuin.TabIndex = 45;
            this.btnKorvenSchuin.Text = "KS - Korf Schuin";
            this.btnKorvenSchuin.UseVisualStyleBackColor = true;
            this.btnKorvenSchuin.Click += new System.EventHandler(this.btnKorvenSchuin_Click_1);
            // 
            // btnKorf
            // 
            this.structuresExtender.SetAttributeName(this.btnKorf, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorf, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorf, null);
            this.btnKorf.Location = new System.Drawing.Point(56, 56);
            this.btnKorf.Name = "btnKorf";
            this.btnKorf.Size = new System.Drawing.Size(147, 32);
            this.btnKorf.TabIndex = 44;
            this.btnKorf.Text = "KR - Korf Recht";
            this.btnKorf.UseVisualStyleBackColor = true;
            this.btnKorf.Click += new System.EventHandler(this.btnKorf_Click_1);
            // 
            // MontageExtra
            // 
            this.structuresExtender.SetAttributeName(this.MontageExtra, null);
            this.structuresExtender.SetAttributeTypeName(this.MontageExtra, null);
            this.structuresExtender.SetBindPropertyName(this.MontageExtra, null);
            this.MontageExtra.Controls.Add(this.dataGridView1);
            this.MontageExtra.Controls.Add(this.button5);
            this.MontageExtra.Controls.Add(this.GetJsonButton);
            this.MontageExtra.Location = new System.Drawing.Point(4, 22);
            this.MontageExtra.Name = "MontageExtra";
            this.MontageExtra.Padding = new System.Windows.Forms.Padding(3);
            this.MontageExtra.Size = new System.Drawing.Size(816, 566);
            this.MontageExtra.TabIndex = 10;
            this.MontageExtra.Text = "Montage Extra";
            this.MontageExtra.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.structuresExtender.SetAttributeName(this.dataGridView1, null);
            this.structuresExtender.SetAttributeTypeName(this.dataGridView1, null);
            this.structuresExtender.SetBindPropertyName(this.dataGridView1, null);
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.piece_count,
            this.reference,
            this.fase,
            this.sid_NL});
            this.dataGridView1.Location = new System.Drawing.Point(80, 83);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(566, 464);
            this.dataGridView1.TabIndex = 2;
            // 
            // piece_count
            // 
            this.piece_count.HeaderText = "piece_count";
            this.piece_count.Name = "piece_count";
            this.piece_count.Width = 80;
            // 
            // reference
            // 
            this.reference.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.reference.HeaderText = "reference";
            this.reference.Name = "reference";
            // 
            // fase
            // 
            this.fase.HeaderText = "fase";
            this.fase.Name = "fase";
            this.fase.Width = 80;
            // 
            // sid_NL
            // 
            this.sid_NL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sid_NL.HeaderText = "sid_NL";
            this.sid_NL.Name = "sid_NL";
            // 
            // button5
            // 
            this.structuresExtender.SetAttributeName(this.button5, null);
            this.structuresExtender.SetAttributeTypeName(this.button5, null);
            this.structuresExtender.SetBindPropertyName(this.button5, null);
            this.button5.Location = new System.Drawing.Point(210, 21);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(436, 40);
            this.button5.TabIndex = 1;
            this.button5.Text = "Maak Json van geselecteerde rijen";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // GetJsonButton
            // 
            this.structuresExtender.SetAttributeName(this.GetJsonButton, null);
            this.structuresExtender.SetAttributeTypeName(this.GetJsonButton, null);
            this.structuresExtender.SetBindPropertyName(this.GetJsonButton, null);
            this.GetJsonButton.Location = new System.Drawing.Point(80, 21);
            this.GetJsonButton.Name = "GetJsonButton";
            this.GetJsonButton.Size = new System.Drawing.Size(97, 40);
            this.GetJsonButton.TabIndex = 0;
            this.GetJsonButton.Text = "Inlezen Json";
            this.GetJsonButton.UseVisualStyleBackColor = true;
            this.GetJsonButton.Click += new System.EventHandler(this.GetJsonButton_Click);
            // 
            // Opschonen
            // 
            this.structuresExtender.SetAttributeName(this.Opschonen, null);
            this.structuresExtender.SetAttributeTypeName(this.Opschonen, null);
            this.structuresExtender.SetBindPropertyName(this.Opschonen, null);
            this.Opschonen.Controls.Add(this.checkBoxADrawings);
            this.Opschonen.Controls.Add(this.labelRemoved);
            this.Opschonen.Controls.Add(this.listBox1);
            this.Opschonen.Controls.Add(this.checkBoxGaDrawings);
            this.Opschonen.Controls.Add(this.checkBoxSObjGrp);
            this.Opschonen.Controls.Add(this.checkBoxCbm);
            this.Opschonen.Controls.Add(this.checkBoxLay);
            this.Opschonen.Controls.Add(this.buttonCleanUp);
            this.Opschonen.Controls.Add(this.buttonVersion);
            this.Opschonen.Controls.Add(this.CurrentVersionLabel);
            this.Opschonen.Controls.Add(this.label5);
            this.Opschonen.Controls.Add(this.label4);
            this.Opschonen.Controls.Add(this.label3);
            this.Opschonen.Location = new System.Drawing.Point(4, 22);
            this.Opschonen.Name = "Opschonen";
            this.Opschonen.Size = new System.Drawing.Size(816, 566);
            this.Opschonen.TabIndex = 6;
            this.Opschonen.Text = "Opschonen";
            this.Opschonen.UseVisualStyleBackColor = true;
            // 
            // checkBoxADrawings
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxADrawings, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxADrawings, null);
            this.checkBoxADrawings.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxADrawings, null);
            this.checkBoxADrawings.Location = new System.Drawing.Point(74, 394);
            this.checkBoxADrawings.Name = "checkBoxADrawings";
            this.checkBoxADrawings.Size = new System.Drawing.Size(130, 17);
            this.checkBoxADrawings.TabIndex = 12;
            this.checkBoxADrawings.Text = "*.ad (merktekeningen)";
            this.checkBoxADrawings.UseVisualStyleBackColor = true;
            // 
            // labelRemoved
            // 
            this.structuresExtender.SetAttributeName(this.labelRemoved, null);
            this.structuresExtender.SetAttributeTypeName(this.labelRemoved, null);
            this.labelRemoved.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.labelRemoved, null);
            this.labelRemoved.Location = new System.Drawing.Point(70, 542);
            this.labelRemoved.Name = "labelRemoved";
            this.labelRemoved.Size = new System.Drawing.Size(0, 13);
            this.labelRemoved.TabIndex = 11;
            // 
            // listBox1
            // 
            this.structuresExtender.SetAttributeName(this.listBox1, null);
            this.structuresExtender.SetAttributeTypeName(this.listBox1, null);
            this.structuresExtender.SetBindPropertyName(this.listBox1, null);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(73, 506);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(658, 4);
            this.listBox1.TabIndex = 10;
            this.listBox1.Visible = false;
            // 
            // checkBoxGaDrawings
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxGaDrawings, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxGaDrawings, null);
            this.checkBoxGaDrawings.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxGaDrawings, null);
            this.checkBoxGaDrawings.Checked = true;
            this.checkBoxGaDrawings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGaDrawings.Location = new System.Drawing.Point(73, 371);
            this.checkBoxGaDrawings.Name = "checkBoxGaDrawings";
            this.checkBoxGaDrawings.Size = new System.Drawing.Size(155, 17);
            this.checkBoxGaDrawings.TabIndex = 9;
            this.checkBoxGaDrawings.Text = "*.gd (overzichtstekeningen)";
            this.checkBoxGaDrawings.UseVisualStyleBackColor = true;
            // 
            // checkBoxSObjGrp
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxSObjGrp, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxSObjGrp, null);
            this.checkBoxSObjGrp.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxSObjGrp, null);
            this.checkBoxSObjGrp.Location = new System.Drawing.Point(73, 348);
            this.checkBoxSObjGrp.Name = "checkBoxSObjGrp";
            this.checkBoxSObjGrp.Size = new System.Drawing.Size(152, 17);
            this.checkBoxSObjGrp.TabIndex = 8;
            this.checkBoxSObjGrp.Text = "*.SObjGrp (selecteer filters)";
            this.checkBoxSObjGrp.UseVisualStyleBackColor = true;
            // 
            // checkBoxCbm
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxCbm, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxCbm, null);
            this.checkBoxCbm.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxCbm, null);
            this.checkBoxCbm.Checked = true;
            this.checkBoxCbm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCbm.Location = new System.Drawing.Point(73, 325);
            this.checkBoxCbm.Name = "checkBoxCbm";
            this.checkBoxCbm.Size = new System.Drawing.Size(124, 17);
            this.checkBoxCbm.TabIndex = 7;
            this.checkBoxCbm.Text = "*.cbm (beton balken)";
            this.checkBoxCbm.UseVisualStyleBackColor = true;
            // 
            // checkBoxLay
            // 
            this.structuresExtender.SetAttributeName(this.checkBoxLay, null);
            this.structuresExtender.SetAttributeTypeName(this.checkBoxLay, null);
            this.checkBoxLay.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBoxLay, null);
            this.checkBoxLay.Checked = true;
            this.checkBoxLay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLay.Location = new System.Drawing.Point(73, 302);
            this.checkBoxLay.Name = "checkBoxLay";
            this.checkBoxLay.Size = new System.Drawing.Size(93, 17);
            this.checkBoxLay.TabIndex = 6;
            this.checkBoxLay.Text = "*.lay (opmaak)";
            this.checkBoxLay.UseVisualStyleBackColor = true;
            // 
            // buttonCleanUp
            // 
            this.structuresExtender.SetAttributeName(this.buttonCleanUp, null);
            this.structuresExtender.SetAttributeTypeName(this.buttonCleanUp, null);
            this.structuresExtender.SetBindPropertyName(this.buttonCleanUp, null);
            this.buttonCleanUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCleanUp.Location = new System.Drawing.Point(73, 427);
            this.buttonCleanUp.Name = "buttonCleanUp";
            this.buttonCleanUp.Size = new System.Drawing.Size(300, 56);
            this.buttonCleanUp.TabIndex = 5;
            this.buttonCleanUp.Text = "Verwijder bestanden";
            this.buttonCleanUp.UseVisualStyleBackColor = true;
            this.buttonCleanUp.Click += new System.EventHandler(this.buttonCleanUp_Click);
            // 
            // buttonVersion
            // 
            this.structuresExtender.SetAttributeName(this.buttonVersion, null);
            this.structuresExtender.SetAttributeTypeName(this.buttonVersion, null);
            this.structuresExtender.SetBindPropertyName(this.buttonVersion, null);
            this.buttonVersion.Location = new System.Drawing.Point(530, 267);
            this.buttonVersion.Name = "buttonVersion";
            this.buttonVersion.Size = new System.Drawing.Size(113, 26);
            this.buttonVersion.TabIndex = 4;
            this.buttonVersion.Text = "Versie";
            this.buttonVersion.UseVisualStyleBackColor = true;
            this.buttonVersion.Visible = false;
            this.buttonVersion.Click += new System.EventHandler(this.buttonVersion_Click);
            // 
            // CurrentVersionLabel
            // 
            this.structuresExtender.SetAttributeName(this.CurrentVersionLabel, null);
            this.structuresExtender.SetAttributeTypeName(this.CurrentVersionLabel, null);
            this.CurrentVersionLabel.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CurrentVersionLabel, null);
            this.CurrentVersionLabel.Location = new System.Drawing.Point(194, 267);
            this.CurrentVersionLabel.Name = "CurrentVersionLabel";
            this.CurrentVersionLabel.Size = new System.Drawing.Size(0, 13);
            this.CurrentVersionLabel.TabIndex = 3;
            // 
            // label5
            // 
            this.structuresExtender.SetAttributeName(this.label5, null);
            this.structuresExtender.SetAttributeTypeName(this.label5, null);
            this.label5.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label5, null);
            this.label5.Location = new System.Drawing.Point(70, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Huidige Tekla versie:";
            // 
            // label4
            // 
            this.structuresExtender.SetAttributeName(this.label4, null);
            this.structuresExtender.SetAttributeTypeName(this.label4, null);
            this.label4.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label4, null);
            this.label4.Location = new System.Drawing.Point(70, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(513, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Verwijderen van alle overbodige opmaken, filters en profielinstellingen uit de Ne" +
    "therlands Environment folder";
            // 
            // label3
            // 
            this.structuresExtender.SetAttributeName(this.label3, null);
            this.structuresExtender.SetAttributeTypeName(this.label3, null);
            this.label3.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label3, null);
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(70, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Opschonen Tekla installatie.";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Info
            // 
            this.structuresExtender.SetAttributeName(this.Info, null);
            this.structuresExtender.SetAttributeTypeName(this.Info, null);
            this.structuresExtender.SetBindPropertyName(this.Info, null);
            this.Info.Controls.Add(this.pictureBox3);
            this.Info.Controls.Add(this.PdfModifyButton);
            this.Info.Controls.Add(this.lblDatum);
            this.Info.Controls.Add(this.label2);
            this.Info.Controls.Add(this.lblVersie);
            this.Info.Controls.Add(this.lblVersion);
            this.Info.Location = new System.Drawing.Point(4, 22);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(816, 566);
            this.Info.TabIndex = 4;
            this.Info.Text = "Info";
            this.Info.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.structuresExtender.SetAttributeName(this.pictureBox3, null);
            this.structuresExtender.SetAttributeTypeName(this.pictureBox3, null);
            this.structuresExtender.SetBindPropertyName(this.pictureBox3, null);
            this.pictureBox3.Image = global::BS_Export.Properties.Resources.logo_bs_copy;
            this.pictureBox3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.InitialImage")));
            this.pictureBox3.Location = new System.Drawing.Point(550, 422);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(274, 152);
            this.pictureBox3.TabIndex = 67;
            this.pictureBox3.TabStop = false;
            // 
            // PdfModifyButton
            // 
            this.structuresExtender.SetAttributeName(this.PdfModifyButton, null);
            this.structuresExtender.SetAttributeTypeName(this.PdfModifyButton, null);
            this.structuresExtender.SetBindPropertyName(this.PdfModifyButton, null);
            this.PdfModifyButton.Location = new System.Drawing.Point(119, 503);
            this.PdfModifyButton.Name = "PdfModifyButton";
            this.PdfModifyButton.Size = new System.Drawing.Size(278, 32);
            this.PdfModifyButton.TabIndex = 5;
            this.PdfModifyButton.Text = "Test Tekst toevoegen op pdf bestanden";
            this.PdfModifyButton.UseVisualStyleBackColor = true;
            this.PdfModifyButton.Visible = false;
            // 
            // lblDatum
            // 
            this.structuresExtender.SetAttributeName(this.lblDatum, null);
            this.structuresExtender.SetAttributeTypeName(this.lblDatum, null);
            this.lblDatum.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblDatum, null);
            this.lblDatum.Location = new System.Drawing.Point(69, 63);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(61, 13);
            this.lblDatum.TabIndex = 4;
            this.lblDatum.Text = "05-11-2024";
            // 
            // label2
            // 
            this.structuresExtender.SetAttributeName(this.label2, null);
            this.structuresExtender.SetAttributeTypeName(this.label2, null);
            this.label2.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label2, null);
            this.label2.Location = new System.Drawing.Point(24, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Datum:";
            // 
            // lblVersie
            // 
            this.structuresExtender.SetAttributeName(this.lblVersie, null);
            this.structuresExtender.SetAttributeTypeName(this.lblVersie, null);
            this.lblVersie.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblVersie, null);
            this.lblVersie.Location = new System.Drawing.Point(67, 36);
            this.lblVersie.Name = "lblVersie";
            this.lblVersie.Size = new System.Drawing.Size(37, 13);
            this.lblVersie.TabIndex = 1;
            this.lblVersie.Text = " 2.242";
            // 
            // lblVersion
            // 
            this.structuresExtender.SetAttributeName(this.lblVersion, null);
            this.structuresExtender.SetAttributeTypeName(this.lblVersion, null);
            this.lblVersion.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblVersion, null);
            this.lblVersion.Location = new System.Drawing.Point(24, 37);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(39, 13);
            this.lblVersion.TabIndex = 0;
            this.lblVersion.Text = "Versie:";
            // 
            // saveLoad2
            // 
            this.structuresExtender.SetAttributeName(this.saveLoad2, null);
            this.structuresExtender.SetAttributeTypeName(this.saveLoad2, null);
            this.saveLoad2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.saveLoad2, null);
            this.saveLoad2.Dock = System.Windows.Forms.DockStyle.Top;
            this.saveLoad2.HelpFileType = Tekla.Structures.Dialog.UIControls.SaveLoad.HelpFileTypeEnum.General;
            this.saveLoad2.HelpKeyword = "";
            this.saveLoad2.HelpUrl = "";
            this.saveLoad2.Location = new System.Drawing.Point(0, 0);
            this.saveLoad2.Margin = new System.Windows.Forms.Padding(4);
            this.saveLoad2.Name = "saveLoad2";
            this.saveLoad2.SaveAsText = "";
            this.saveLoad2.Size = new System.Drawing.Size(832, 53);
            this.saveLoad2.TabIndex = 1;
            this.saveLoad2.UserDefinedHelpFilePath = null;
            // 
            // MainForm
            // 
            this.structuresExtender.SetAttributeName(this, null);
            this.structuresExtender.SetAttributeTypeName(this, null);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.structuresExtender.SetBindPropertyName(this, null);
            this.ClientSize = new System.Drawing.Size(832, 653);
            this.Controls.Add(this.saveLoad2);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Betonstaal Exporter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabProject.ResumeLayout(false);
            this.tabProject.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.CoverGroupBox.ResumeLayout(false);
            this.CoverGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Tradional.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.m3GroupBox.ResumeLayout(false);
            this.m3GroupBox.PerformLayout();
            this.groupBoxTraditioneel.ResumeLayout(false);
            this.groupBoxTraditioneel.PerformLayout();
            this.tabCUDrawings.ResumeLayout(false);
            this.tabCUDrawings.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.TabExport.ResumeLayout(false);
            this.TabExport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.JsonGroupBox.ResumeLayout(false);
            this.JsonGroupBox.PerformLayout();
            this.Export.ResumeLayout(false);
            this.Export.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBoxPDF.ResumeLayout(false);
            this.groupBoxPDF.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.BVBS.ResumeLayout(false);
            this.BVBS.PerformLayout();
            this.MontageExtra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Opschonen.ResumeLayout(false);
            this.Opschonen.PerformLayout();
            this.Info.ResumeLayout(false);
            this.Info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage TabExport;
        private System.Windows.Forms.TextBox TxKlasse;
        private System.Windows.Forms.Button btnBVBS;
        private System.Windows.Forms.Label lblNummering;
        private System.Windows.Forms.RadioButton RadPerFase;
        private System.Windows.Forms.RadioButton RadAlles;
        private System.Windows.Forms.TextBox TxRadbutton;
        private System.Windows.Forms.Button BtnPdf;
        private System.Windows.Forms.Button BtnIFC;
        private System.Windows.Forms.TabPage Info;
        private System.Windows.Forms.Label lblVersie;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxPDF;
        private System.Windows.Forms.CheckBox CbPdfG;
        private System.Windows.Forms.CheckBox CbPdfC;
        private Tekla.Structures.Dialog.UIControls.SaveLoad saveLoad2;
        private System.Windows.Forms.TabPage BVBS;
        private System.Windows.Forms.CheckBox CbZ;
        private System.Windows.Forms.Button BtnZbars;
        private System.Windows.Forms.CheckBox CbBalken3;
        private System.Windows.Forms.Button btnDekking3snedig;
        private System.Windows.Forms.CheckBox CbBalkenS;
        private System.Windows.Forms.Button btnDekkingSchuin;
        private System.Windows.Forms.CheckBox CbBalkenD;
        private System.Windows.Forms.Button btnBalkenD;
        private System.Windows.Forms.CheckBox CbPoeren;
        private System.Windows.Forms.CheckBox CbMatOpMaat;
        private System.Windows.Forms.CheckBox CbKorvenHrsp;
        private System.Windows.Forms.CheckBox CbNettenSup;
        private System.Windows.Forms.CheckBox CbKorvenLos;
        private System.Windows.Forms.CheckBox CbBalken3sn;
        private System.Windows.Forms.CheckBox CbKorven3sn;
        private System.Windows.Forms.CheckBox CbBalkenSchuin;
        private System.Windows.Forms.CheckBox CbKorvenSchuin;
        private System.Windows.Forms.CheckBox CbBalken;
        private System.Windows.Forms.CheckBox CbKorven;
        private System.Windows.Forms.Button BtnPoer;
        private System.Windows.Forms.Button btnBalken3;
        private System.Windows.Forms.Button btnKorf3;
        private System.Windows.Forms.Button btnMatOpMaat;
        private System.Windows.Forms.Button btnNetSup;
        private System.Windows.Forms.Button btnKorfHrsp;
        private System.Windows.Forms.Button btnKorfLos;
        private System.Windows.Forms.Button btnBalkenSchuin;
        private System.Windows.Forms.Button btnBalken;
        private System.Windows.Forms.Button btnKorvenSchuin;
        private System.Windows.Forms.Button btnKorf;
        private System.Windows.Forms.TabPage Opschonen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonVersion;
        private System.Windows.Forms.Label CurrentVersionLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxCbm;
        private System.Windows.Forms.CheckBox checkBoxLay;
        private System.Windows.Forms.Button buttonCleanUp;
        private System.Windows.Forms.CheckBox checkBoxSObjGrp;
        private System.Windows.Forms.CheckBox checkBoxGaDrawings;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label labelRemoved;
        private System.Windows.Forms.CheckBox CbStekrek;
        private System.Windows.Forms.Button btnStekrek;
        private System.Windows.Forms.CheckBox checkBoxLosseKorf;
        private System.Windows.Forms.Button BtnMergePlotfiles;
        private System.Windows.Forms.TabPage Tradional;
        private System.Windows.Forms.GroupBox groupBoxTraditioneel;
        private System.Windows.Forms.CheckBox G105;
        private System.Windows.Forms.CheckBox G104;
        private System.Windows.Forms.CheckBox G100;
        private System.Windows.Forms.Button btnGtekTraditioneel;
        private System.Windows.Forms.CheckBox G103;
        private System.Windows.Forms.CheckBox G102;
        private System.Windows.Forms.CheckBox G101;
        private System.Windows.Forms.GroupBox m3GroupBox;
        private System.Windows.Forms.Button btnGtekm3;
        private System.Windows.Forms.CheckBox G500;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox G208;
        private System.Windows.Forms.CheckBox G206;
        private System.Windows.Forms.CheckBox G207;
        private System.Windows.Forms.CheckBox G205;
        private System.Windows.Forms.Button btnGtekKelderwanden;
        private System.Windows.Forms.CheckBox G204;
        private System.Windows.Forms.CheckBox G200;
        private System.Windows.Forms.CheckBox G201;
        private System.Windows.Forms.CheckBox G203;
        private System.Windows.Forms.CheckBox G202;
        private System.Windows.Forms.CheckBox G210;
        private System.Windows.Forms.CheckBox G209;
        private System.Windows.Forms.CheckBox checkBoxADrawings;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox G307;
        private System.Windows.Forms.CheckBox G306;
        private System.Windows.Forms.CheckBox G305;
        private System.Windows.Forms.CheckBox G304;
        private System.Windows.Forms.CheckBox G303;
        private System.Windows.Forms.CheckBox G302;
        private System.Windows.Forms.CheckBox G301;
        private System.Windows.Forms.CheckBox G300;
        private System.Windows.Forms.Button BtnGtekBreedplaten;
        private System.Windows.Forms.Button PdfModifyButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button BtnGtekStroken;
        private System.Windows.Forms.CheckBox G401;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox G107;
        private System.Windows.Forms.CheckBox G106;
        private System.Windows.Forms.CheckBox G402;
        private System.Windows.Forms.CheckBox checkBoxVisualize;
        private System.Windows.Forms.CheckBox checkBoxReset;
        private System.Windows.Forms.CheckBox G403;
        private System.Windows.Forms.CheckBox G109;
        private System.Windows.Forms.CheckBox G108;
        private System.Windows.Forms.TabPage tabProject;
        private System.Windows.Forms.TabPage tabCUDrawings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button BtnDrawingBalkLos;
        private System.Windows.Forms.Button WizardButton;
        private System.Windows.Forms.Button BtnDrawingsBalk;
        private System.Windows.Forms.Button NummerenButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox G0;
        private System.Windows.Forms.CheckBox G10;
        private System.Windows.Forms.CheckBox G9;
        private System.Windows.Forms.CheckBox G8;
        private System.Windows.Forms.CheckBox G7;
        private System.Windows.Forms.Button SwitchSelectionButton;
        private System.Windows.Forms.Button BtnDrawingsNew;
        private System.Windows.Forms.CheckBox G6;
        private System.Windows.Forms.CheckBox G1;
        private System.Windows.Forms.CheckBox G5;
        private System.Windows.Forms.CheckBox G2;
        private System.Windows.Forms.CheckBox G4;
        private System.Windows.Forms.CheckBox G3;
        private System.Windows.Forms.Button CopyTplButton;
        private System.Windows.Forms.CheckBox CustomercheckBox;
        private System.Windows.Forms.ComboBox CustomerComboBox;
        private System.Windows.Forms.GroupBox Export;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox CoverGroupBox;
        private System.Windows.Forms.ComboBox CoverInsideComboBox;
        private System.Windows.Forms.CheckBox CoverInsideCheckBox;
        private System.Windows.Forms.ComboBox CoverEndComboBox;
        private System.Windows.Forms.CheckBox CoverEndCheckBox;
        private System.Windows.Forms.ComboBox CoverSidesComboBox;
        private System.Windows.Forms.CheckBox CoverSidesCheckBox;
        private System.Windows.Forms.ComboBox CoverStartComboBox;
        private System.Windows.Forms.CheckBox CoverStartCheckBox;
        private System.Windows.Forms.ComboBox CoverTopComboBox;
        private System.Windows.Forms.ComboBox CoverBottomComboBox;
        private System.Windows.Forms.CheckBox CoverTopCheckBox;
        private System.Windows.Forms.CheckBox CoverBottomCheckBox;
        private System.Windows.Forms.Button CoverInfo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox G308;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.GroupBox JsonGroupBox;
        private System.Windows.Forms.Button JsonStekrekButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button JsonHrspButton;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button VMeshButton;
        private System.Windows.Forms.Button StrMeshButton;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox TxRadJsonbutton;
        private System.Windows.Forms.RadioButton RadJsonPerFase;
        private System.Windows.Forms.RadioButton RadJsonAlles;
        private System.Windows.Forms.Button JsonFloorButton;
        private System.Windows.Forms.CheckBox checkBoxResetJson;
        private System.Windows.Forms.CheckBox checkBoxVisualizeJson;
        private System.Windows.Forms.Button JsonKorvenButton;
        private System.Windows.Forms.CheckBox CheckBoxSetKleuren;
        private System.Windows.Forms.Button KorbButton;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox CbHrspLos;
        private System.Windows.Forms.Button HrspLosButton;
        private System.Windows.Forms.Button BtnBalk;
        private System.Windows.Forms.Button EmptyPhaseButton;
        private System.Windows.Forms.Button JsonBijlegButton;
        private System.Windows.Forms.CheckBox CbKorven4sn;
        private System.Windows.Forms.Button btnKorf4;
        private System.Windows.Forms.Button BvbsFundDoorvoer;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox FullLengthTextBox;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button FullLengthButton;
        private System.Windows.Forms.Button PonsButton;
        private System.Windows.Forms.Button ZrackButton;
        private System.Windows.Forms.Button CustomMeshButton;
        private System.Windows.Forms.Button RondeKolomButton;
        private System.Windows.Forms.Label DrawingStatus;
        private System.Windows.Forms.TabPage MontageExtra;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button GetJsonButton;
        private System.Windows.Forms.Button PutButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn piece_count;
        private System.Windows.Forms.DataGridViewTextBoxColumn reference;
        private System.Windows.Forms.DataGridViewTextBoxColumn fase;
        private System.Windows.Forms.DataGridViewTextBoxColumn sid_NL;
    }
}