using System;
using System.Windows.Forms;
using TSM = Tekla.Structures.Model;
using TSG = Tekla.Structures.Geometry3d;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using BS_Export;
using JsonExport.BaseExtensions;
using JsonExport.JsonExports.Additional;
using JsonExport.JsonExports.Floor;
using JsonExport.JsonExports.Hairpin;
using JsonExport.JsonExports.Korb;
using JsonExport.JsonExports.Korb.Creators.CoverCreators;
using JsonExport.JsonExports.LShaped;
using JsonExport.JsonExports.LShaped.LooseRebars;
using JsonExport.JsonExports.StripMesh;
using JsonExport.JsonExports.VMesh;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using Tekla.Structures;
using Tekla.Structures.Drawing;
using FileInfo = System.IO.FileInfo;
using Operation = Tekla.Structures.Model.Operations.Operation;
using JsonExport.JsonExports.WallMesh;
using JsonExport.NewJsonExports;
using Tekla.Structures.Model.UI;
using Color = System.Drawing.Color;
using View = Tekla.Structures.Model.UI.View;
using Formatting = Newtonsoft.Json.Formatting;
using JsonExport.JsonExports.AdditionalV2;
using JsonExport.JsonExports.CustomMesh;
using Tekla.Structures.Dialog.UIControls;

namespace BVBS_Export  // naam namespace niet aanpassen!
{
    public partial class MainForm : Tekla.Structures.Dialog.PluginFormBase
    {
        [Obsolete]
        public MainForm()
        {
            InitializeComponent();

            TxKlasse.Width = 0;
            TxRadbutton.Width = 0;
            TxRadJsonbutton.Width = 0;
        }

        private static readonly TSM.Model Model = new TSM.Model();
        public static readonly string Pad = Model.GetInfo().ModelPath;
        private static readonly string Modelnaam = Model.GetInfo().ModelName;

        private void TxKlasse_TextChanged(object sender, EventArgs e)
        {
            //  this.SetAttributeValue(TxKlasse, TxKlasse.Text);
        }


        private void BtnBVBS_Click(object sender, EventArgs e)
        {
            //btnKorf_Click_1(sender, e); //KR
            // btnBalken_Click_1(sender, e);
            // btnBalkenD_Click_1(sender, e);
            // BtnKorvenSchuin_Click(sender, e);
            // BtnBalkenSchuin_Click(sender, e); //in de btnBalken
            //btnDekkingSchuin_Click_1(sender, e);
            //BtnKorf3_Click(sender, e);
            //  BtnBalken3_Click(sender, e);  //in de btnBalken
            //  btnDekking3snedig_Click_1(sender, e);
            //z staven, netten, korven, poer
            BtnZbars_Click(sender, e);
            BtnNetSup_Click(sender, e);
            // BtnKorfHrsp_Click(sender, e);
            BtnPoer_Click_1(sender, e);
            BtnBalk_Click(sender, e);
            BvbsFundDoorvoer_Click(sender, e);
            PonsButton_Click(sender, e);
            ZrackButton_Click(sender, e);
            // btnStekrek_Click(sender, e);
            //los?
            RondeKolomButton_Click(sender, e);

            Operation.DisplayPrompt("Alle BVBS exporten gereed");
        }

        private void RadAlles_CheckedChanged(object sender, EventArgs e)
        {
            TxRadbutton.Text = "A";
        }

        private void RadPerFase_CheckedChanged(object sender, EventArgs e)
        {
            TxRadbutton.Text = "P";
        }

        private void TxRadbutton_TextChanged(object sender, EventArgs e)
        {
            //switch (TxRadbutton.Text.ToString())
            //{
            //    //case "A":
            //    //    RadAlles.Checked = true;
            //    //    break;
            //    //case "P":
            //    //    RadPerFase.Checked = true;
            //    //    break;
            //}
        }

        private void ParametersTabPage_Click(object sender, EventArgs e)
        {

        }


        private void BtnKorvenSchuin_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korvenS = new ArrayList();
                var wapKorvenS = new ArrayList();
                var selectedObjects = new ArrayList();
                var map = string.Empty;


                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);

                        if (part.Name == "KORF_SCHUIN")
                        {
                            var _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                var _id = part.Identifier.ID;
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            korvenS.Add(part);
                        }
                    }

                }

                Operation.DisplayPrompt(korvenS.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";

                foreach (TSM.Part export in korvenS)
                {
                    wapKorvenS.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;

                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    if (bars.NumberingSeries.Prefix != "LSO" || bars.NumberingSeries.Prefix != "LSB" || bars.NumberingSeries.Prefix != "LSF")
                                    {
                                        wapKorvenS.Add(bars);
                                    }

                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    if (bar.NumberingSeries.Prefix != "LSO" || bar.NumberingSeries.Prefix != "LSB" || bar.NumberingSeries.Prefix != "LSF")
                                    {
                                        wapKorvenS.Add(bar);
                                    }

                                    if (bar.NumberingSeries.Prefix != "D")
                                    {
                                        wapKorvenS.Add(bar);
                                    }

                                    break;
                                }
                        }
                    }
                    //}

                    //gefilterde staven
                    modelObjectSelector.Select(wapKorvenS);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var bestandsnaamFase = string.Empty;
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        bestandsnaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }

        }


        private void BtnBalkenSchuin_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balkenSchuin = new ArrayList();
                var wapBalkenLS = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var naam = "";
                        part.GetReportProperty("NAME", ref naam);

                        if (naam == "BALK_SCHUIN")
                        {
                            balkenSchuin.Add(part);
                        }

                    }
                }

                Operation.DisplayPrompt(balkenSchuin.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                foreach (TSM.Part export in balkenSchuin)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    if (bars.NumberingSeries.Prefix == "LSO" || bars.NumberingSeries.Prefix == "LSB" || bars.NumberingSeries.Prefix == "LSF" || bars.NumberingSeries.Prefix == "LSH")
                                    {
                                        wapBalkenLS.Add(bars);
                                    }

                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    if (bar.NumberingSeries.Prefix == "LSO" || bar.NumberingSeries.Prefix == "LSB" || bar.NumberingSeries.Prefix == "LSF" || bar.NumberingSeries.Prefix == "LSH")
                                    {
                                        wapBalkenLS.Add(bar);
                                    }

                                    break;
                                }
                        }
                    }
                }

                //gefilterde staven
                if (RadAlles.Checked)
                {
                    modelObjectSelector.Select(wapBalkenLS);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("FileName", ".\\BVBS\\LS1.abs");
                    component.Insert();
                }

                if (RadPerFase.Checked)
                {
                    //geselecteerde filteren per fase
                    var faseList = new List<int>();

                    var wapBalkenLs1 = new ArrayList();
                    var wapBalkenLs2 = new ArrayList();
                    var wapBalkenLs3 = new ArrayList();

                    var fase = 0;

                    foreach (var rebarItem in wapBalkenLS)
                    {
                        switch (rebarItem)
                        {
                            case TSM.SingleRebar bar:
                                {
                                    bar.GetReportProperty("PHASE", ref fase);
                                    if (fase == 1)
                                        wapBalkenLs1.Add(rebarItem);
                                    if (fase == 2)
                                        wapBalkenLs2.Add(rebarItem);
                                    if (fase == 3)
                                        wapBalkenLs3.Add(rebarItem);
                                    break;
                                }
                            case TSM.RebarGroup bars:
                                {
                                    bars.GetReportProperty("PHASE", ref fase);
                                    if (fase == 1)
                                        wapBalkenLs1.Add(rebarItem);
                                    if (fase == 2)
                                        wapBalkenLs2.Add(rebarItem);
                                    if (fase == 3)
                                        wapBalkenLs3.Add(rebarItem);
                                    break;
                                }
                        }
                    }


                    //LS
                    if (wapBalkenLs1.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalkenLs1);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 1"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 1");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 1");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 1\\fase_1-LS1.abs");
                        component.Insert();
                    }

                    if (wapBalkenLs2.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalkenLs2);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 2"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 2");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 2");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 2\\fase_2-LS1.abs");
                        component.Insert();
                    }

                    if (wapBalkenLs3.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalkenLs3);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 3"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 3");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 3");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 3\\fase_3-LS1.abs");
                        component.Insert();
                    }
                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }


        private void BtnKorfLos_Click(object sender, EventArgs e)
        {

        }

        private void BtnKorfHrsp_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korven = new ArrayList();
                var wapKorven = new ArrayList();
                var selectedObjects = new ArrayList();
                string map;

                int acn;
                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(moe.Current);
                        //var passtuk = string.Empty;
                        //part.GetReportProperty("BS_PASSTUK", ref passtuk);
                        // if (part.Name == "KORF_HRSP" && passtuk != "J")
                        if (part.Name == "KORF_HRSP")
                        {
                            acn = 0;
                            part.GetUserProperty("ACN", ref acn);

                            if (acn == 0)
                            {
                                var id = part.Identifier.ID;
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            korven.Add(part);
                        }
                    }
                }

                Operation.DisplayPrompt(korven.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                acn = 0;
                string bestandsnaam;

                foreach (TSM.Part export in korven)
                {
                    wapKorven.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                wapKorven.Add(bars);
                                break;
                            case TSM.SingleRebar bar:
                                wapKorven.Add(bar);
                                wapKorven.Add(bar);
                                break;
                        }
                    }

                    //gefilterde staven
                    modelObjectSelector.Select(wapKorven);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var bestandsnaamFase = "";
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        bestandsnaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();
                }

                Operation.DisplayPrompt("BVBS export gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void BtnZbars_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balkenBij = new ArrayList();
                var wapBalkenZ = new ArrayList();
                balkenBij.Clear();
                wapBalkenZ.Clear();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (!(moe.Current is TSM.Part part))
                        continue;
                    selectedObjects.Add(part);
                    var naam = "";
                    part.GetReportProperty("NAME", ref naam);

                    if (naam.Contains("KORF_BIJLEG") || naam.Contains("HAAK_LOS")) //STEK_LOS verwijderd 28mrt2022 - HRSP_LOS verwijderd 28april2022
                    {
                        balkenBij.Add(part);
                    }
                }

                Operation.DisplayPrompt(balkenBij.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balkenBij)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }

                var prefixZ = prefixWapenings.Where(s => s.Prefix.StartsWith("L")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                    {
                        {"Z", prefixZ}
                    }
                    ;

                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }

                Operation.DisplayPrompt("BVBS export Z-staven is gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void BtnBalken3_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            var balken3 = new ArrayList();
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                //string map = "";
                var wapBalken3LR = new ArrayList();
                var wapBalken3Dekking = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (!(moe.Current is TSM.Part part))
                        continue;
                    selectedObjects.Add(part);
                    var naam = "";
                    part.GetReportProperty("NAME", ref naam);

                    if (naam == "3_SNEDIGE_BALK")
                    {
                        balken3.Add(part);
                    }
                }

                Operation.DisplayPrompt(balken3.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                foreach (TSM.Part export in balken3)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    if (bars.NumberingSeries.Prefix == "L3SO" || bars.NumberingSeries.Prefix == "L3SB" || bars.NumberingSeries.Prefix == "L3SF" || bars.NumberingSeries.Prefix == "L3SH")
                                    {
                                        wapBalken3LR.Add(bars);
                                    }

                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    if (bar.NumberingSeries.Prefix == "L3SO" || bar.NumberingSeries.Prefix == "L3SB" || bar.NumberingSeries.Prefix == "L3SF" || bar.NumberingSeries.Prefix == "L3SH")
                                    {
                                        wapBalken3LR.Add(bar);
                                    }

                                    break;
                                }
                        }
                    }
                }

                //gefilterde staven
                if (RadAlles.Checked)
                {
                    modelObjectSelector.Select(wapBalken3LR);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("FileName", ".\\BVBS\\L3S1.abs");
                    component.Insert();
                }

                if (RadPerFase.Checked)
                {
                    //geselecteerde filteren per fase
                    var faseList = new List<int>();

                    var wapBalken3LR1 = new ArrayList();
                    var wapBalken3LR2 = new ArrayList();
                    var wapBalken3LR3 = new ArrayList();
                    // wapBalkenDekking1 = wapBalkenDekking(x => x.f);
                    var fase = 0;

                    foreach (var rebarItem in wapBalken3LR)
                    {
                        switch (rebarItem)
                        {
                            case TSM.SingleRebar bar:
                                {
                                    bar.GetReportProperty("PHASE", ref fase);
                                    if (fase == 1)
                                        wapBalken3LR1.Add(rebarItem);
                                    if (fase == 2)
                                        wapBalken3LR2.Add(rebarItem);
                                    if (fase == 3)
                                        wapBalken3LR3.Add(rebarItem);
                                    break;
                                }
                            case TSM.RebarGroup bars:
                                {
                                    bars.GetReportProperty("PHASE", ref fase);
                                    if (fase == 1)
                                        wapBalken3LR1.Add(rebarItem);
                                    if (fase == 2)
                                        wapBalken3LR2.Add(rebarItem);
                                    if (fase == 3)
                                        wapBalken3LR3.Add(rebarItem);
                                    break;
                                }
                        }
                    }


                    //L3S
                    if (wapBalken3LR1.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalken3LR1);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 1"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 1");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 1");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 1\\fase_1-L3S.abs");
                        component.Insert();
                    }

                    if (wapBalken3LR2.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalken3LR2);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 2"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 2");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 2");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 2\\fase_2-L3S.abs");
                        component.Insert();
                    }

                    if (wapBalken3LR3.Count > 0)
                    {
                        modelObjectSelector.Select(wapBalken3LR3);
                        component.LoadAttributesFromFile("bs_standaard");
                        if (!Directory.Exists("BVBS\\fase 3"))
                        {
                            Directory.CreateDirectory("BVBS\\fase 3");
                        }

                        component.SetAttribute("FolderName", "BVBS\\fase 3");
                        component.SetAttribute("FileName", ".\\BVBS\\fase 3\\fase_3-L3S.abs");
                        component.Insert();
                    }
                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void BtnIFC_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists("IFC"))
            {
                Directory.CreateDirectory("IFC");
            }

            var model = new TSM.Model();

            if (checkBoxVisualize.Checked)
            {
                var mos = new ModelObjectSelector();
                var moeOrg = mos.GetSelectedObjects();
                var hoofdOnderdeel = new List<TSM.Part>();
                var subbies = new List<TSM.Beam>();
                var selectedArray = new ArrayList();

                while (moeOrg.MoveNext())
                {
                    if (moeOrg.Current is TSM.Part part)
                    {
                        selectedArray.Add(part);
                        var mainPart = 0;

                        if (part.GetReportProperty("MAIN_PART", ref mainPart))
                        {
                            if (mainPart == 1 && part.Name == "MAT_STD")
                            {
                                hoofdOnderdeel.Add(part);
                            }
                        }
                    }
                }



                var pluginList = new List<TSM.Component>();
                var moe = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.COMPONENT);
                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Component plugin && (plugin.Name == "StripMesh" || plugin.Name == "RebarFloor"))
                    {
                        pluginList.Add(plugin);
                    }
                }

                foreach (var plugin in pluginList)
                {
                    SetJsonAttribute(plugin, "Visualize", true);
                }

                model.CommitChanges();
                TeklaStructures.CommonTasks.PerformNumbering(false);

                //afstandshouders toevoegen aan selectie
                foreach (var p in hoofdOnderdeel)
                {
                    var subs = p.GetAssembly().GetSubAssemblies();

                    foreach (TSM.Assembly ass in subs)
                    {
                        if (ass.GetMainPart() is TSM.Beam mainPart && mainPart.Name.StartsWith("GR"))
                        {
                            subbies.Add(mainPart);
                        }
                    }
                    selectedArray.AddRange(subbies);

                }

                //nieuwe selectie met afstandhouders
                var newSelection = new ModelObjectSelector();
                newSelection.Select(selectedArray);

            }

            var viewNames = new List<string>();
            var viewList = new List<View>();
            var views = ViewHandler.GetVisibleViews();

            if (CheckBoxSetKleuren.Checked)
            {
                //var views = ViewHandler.GetSelectedViews(); // geen venster geselecteerd, want onderdelen geselecteerd
                while (views.MoveNext())
                {
                    if (views.Current is View view)
                    {
                        viewNames.Add(view.CurrentRepresentation);
                        viewList.Add(view);
                        view.CurrentRepresentation = "kleuren";
                        view.Modify();
                    }
                }
            }

            var modelName = string.Empty;

            var start = new TSG.Point(0, 0, 0);
            var eind = new TSG.Point(100, 0, 0);
            var dummy = new TSM.Beam(start, eind)
            {
                Profile =
                {
                    ProfileString = "D10"
                }
            };
            dummy.Insert();
            dummy.GetReportProperty("PROJECT.PROJECT.MODEL", ref modelName);
            modelName = modelName.Replace(" ", "_");
            var dateTimeNow = DateTime.Now.ToString("yyyyMMddHHmmss");
            var file = ".\\IFC\\" + modelName + "_" + dateTimeNow;
            var componentInput = new TSM.ComponentInput();
            componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
            var comp = new TSM.Component(componentInput)
            {
                Name = "ExportIFC",
                Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
            };
            comp.LoadAttributesFromFile("standard");
            comp.SetAttribute("CreateAll", 0); // 0 to export selected objects
            comp.SetAttribute("OutputFile", file);
            comp.SetAttribute("ExportType", 1); // oppervlaktegeometrie
            comp.SetAttribute("ViewColors", 1);
            comp.Insert();

            if (CheckBoxSetKleuren.Checked)
            {
                if (viewNames.Any())
                {
                    var index = 0;
                    foreach (var view in viewList)
                    {
                        view.CurrentRepresentation = viewNames[index];
                        view.Modify();
                        index++;
                    }

                    model.CommitChanges();
                }
            }

            if (checkBoxReset.Checked)
            {
                var pluginList = new List<TSM.Component>();
                var moe = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.COMPONENT);
                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Component plugin && (plugin.Name == "StripMesh" || plugin.Name == "RebarFloor"))
                    {
                        pluginList.Add(plugin);
                    }
                }

                foreach (var plugin in pluginList)
                {
                    SetJsonAttribute(plugin, "Visualize", false);
                }


                model.CommitChanges();
            }

            Operation.DisplayPrompt("IFC export gereed");
            dummy.Delete();

        }

        private static void SetJsonAttribute(TSM.Component plugin, string attribute, bool value)
        {
            var dynamicString = "DynamicViewModel";

            var existingViewModel = string.Empty;
            plugin.GetDynamicStringProperty(dynamicString, ref existingViewModel);

            if (string.IsNullOrEmpty(existingViewModel))
            {
                plugin.GetDynamicStringProperty("VIEW_MODEL", ref existingViewModel);
                dynamicString = "VIEW_MODEL";
            }

            if (string.IsNullOrEmpty(existingViewModel))
            {
                return;
            }

            var o2 = JObject.Parse(existingViewModel);

            if (o2 == null)
                return;
            o2["General"][attribute] = value;

            var newViewModel = o2.ToString();
            plugin.SetDynamicStringProperty(dynamicString, newViewModel);

            plugin.Modify();
        }


        private void BtnNetSup_Click(object sender, EventArgs e)
        {

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var platen = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    var part = moe.Current as TSM.Part;
                    selectedObjects.Add(part);

                    if (part == null)
                        continue;
                    var naam = "";
                    part.GetReportProperty("NAME", ref naam);

                    if (naam == "PLAAT")
                    {
                        platen.Add(part);
                    }
                }

                Operation.DisplayPrompt(platen.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };

                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in platen)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }

                var prefixMv = prefixWapenings.Where(s => (s.Prefix.StartsWith("T") || (s.Prefix.StartsWith("M")))).Select(w => w.Wapening).ToList();
                var prefixMd = prefixWapenings.Where(s => s.Prefix.StartsWith("D")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                {
                    {"MV", prefixMv},
                    {"DV", prefixMd}, // was MD. Arie whatsapp 8sep20: md1.abs kunnen we niets mee moet zijn dv1.abs
                };


                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }



                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }
        

        private void BtnPdf_Click(object sender, EventArgs e)
        {
            new System.Threading.Tasks.Task(delegate
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    var model = new TSM.Model();
                    var drawingHandler = new DrawingHandler();

                    var printAttributes = GetPrintAttributes();

                    var drawingEnumerator = drawingHandler.GetDrawings();
                    // var drawingEnumerator = drawingHandler.GetDrawingSelector().GetSelected();

                    while (drawingEnumerator.MoveNext())
                    {
                        var currentDrawing = drawingEnumerator.Current;
                        if (currentDrawing != null)
                        {
                            if (currentDrawing is CastUnitDrawing || currentDrawing is GADrawing)
                            {
                                if (currentDrawing is GADrawing && CbPdfG.Checked)
                                {
                                    var gaDrawing = (GADrawing)currentDrawing;
                                    var adn = string.Empty;
                                    var naam = currentDrawing.Name;
                                    var titel1 = currentDrawing.Title1;
                                    var mark = currentDrawing.Mark.Replace('[', ' ').Replace(']', ' ');

                                    gaDrawing.GetUserProperty("ADN", ref adn);
                                    if (adn.Length > 0)
                                    {
                                        if (titel1.Length > 0)
                                        {
                                            printAttributes.OutputFileName = "Plotfiles//" + adn + "_" + naam + "-" + titel1 + "-" + mark + ".pdf";
                                        }
                                        else
                                        {
                                            printAttributes.OutputFileName = "Plotfiles//" + adn + "_" + naam + "-" + mark + ".pdf";
                                        }
                                    }
                                    else
                                    {
                                        if (titel1.Length > 0)
                                        {
                                            printAttributes.OutputFileName = "Plotfiles//" + naam + "-" + titel1 + "-" + mark + ".pdf";
                                        }
                                        else
                                        {
                                            printAttributes.OutputFileName = "Plotfiles//" + naam + "-" + mark + ".pdf";
                                        }
                                    }

                                    drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                }

                                if (currentDrawing is CastUnitDrawing && CbPdfC.Checked)
                                {
                                    var objectList = currentDrawing.GetSheet().GetAllObjects(typeof(ModelObject));
                                    while (objectList.MoveNext())
                                    {
                                        if (!(objectList.Current is ModelObject currentObject))
                                            continue;
                                        if (!(model.SelectModelObject(currentObject.ModelIdentifier) is TSM.Part myPart))
                                            continue;
                                        var castUnitPos = "";
                                        myPart.GetReportProperty("CAST_UNIT_POS", ref castUnitPos);


                                        //A == vloer dan eerst de naam + pos
                                        if (castUnitPos.Contains("A"))
                                        {
                                            printAttributes.OutputFileName = "Plotfiles//" + currentDrawing.Name + "_" + castUnitPos + ".pdf";
                                            drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                        }
                                        else
                                        {
                                            var splitOnStripes = currentDrawing.Name.Split('-');
                                            var numberOfStripes = splitOnStripes.Length;
                                            if (numberOfStripes == 2) // geen ref
                                            {
                                                printAttributes.OutputFileName = "Plotfiles//" + castUnitPos + " - " + currentDrawing.Name + ".pdf";
                                                drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                            }

                                            if (numberOfStripes == 3) // wel ref
                                            {
                                                printAttributes.OutputFileName = "Plotfiles//" + castUnitPos + "_" + currentDrawing.Name + ".pdf";
                                                drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                            }

                                            if (numberOfStripes < 2 || numberOfStripes > 3) //wat het was tot v2.107
                                            {
                                                printAttributes.OutputFileName = "Plotfiles//" + castUnitPos + "_" + currentDrawing.Name + ".pdf";
                                                drawingHandler.PrintDrawing(currentDrawing, printAttributes);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                    drawingHandler.CloseActiveDrawing();
                    Cursor.Current = Cursors.Default;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Melding PDF: " + ex);
                }

            }).Start();

            
        }


        private static DPMPrinterAttributes GetPrintAttributes()
        {
            var printAttributes = new DPMPrinterAttributes();
            printAttributes.ColorMode = DotPrintColor.Color;
            printAttributes.NumberOfCopies = 1;
            printAttributes.OpenFileWhenFinished = false;
            printAttributes.Orientation = DotPrintOrientationType.Landscape;
            printAttributes.OutputType = DotPrintOutputType.PDF;
            printAttributes.PaperSize = DotPrintPaperSize.A4;
            printAttributes.PrinterName = "Microsoft Print to PDF"; // Must use local PDF printer name
            printAttributes.PrintToMultipleSheet = DotPrintToMultipleSheet.Off;
            printAttributes.ScaleFactor = 1.0;
            printAttributes.ScalingMethod = DotPrintScalingType.Auto;

            return printAttributes;
        }

        public static void CreateDrawings<T>(string filter, string name, string setting, string drawingName = "BALK - ") where T : TSM.Part
        {
            var model = new TSM.Model();
            var moe = model.GetModelObjectSelector().GetObjectsByFilterName(filter);

            while (moe.MoveNext())
            {
                if (moe.Current is T part && part.Name == name)
                {
                    var myDrawing = new CastUnitDrawing(part.GetAssembly().Identifier, setting);

                    var mainPartCastUnitPos = "";
                    ((T)part.GetAssembly().GetMainPart()).GetReportProperty("CAST_UNIT.CAST_UNIT.CAST_UNIT_POS", ref mainPartCastUnitPos);

                    myDrawing.Name = drawingName + mainPartCastUnitPos;

                    myDrawing.Insert();
                }
            }
        }

        private void BtnDrawingsNew_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (G0.Checked)
                    Operation.RunMacro("Voorblad.cs");
                if (G1.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.1.cs");
                if (G2.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.2.cs");
                if (G3.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.3.cs");
                if (G4.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.4.cs");
                if (G5.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.5.cs");
                if (G6.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.6.cs");
                // if (G7.Checked) Operation.RunMacro("Voorblad.cs");
                if (G7.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.7.cs");
                if (G8.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.8.cs");
                if (G9.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.9.cs");
                if (G10.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.10.cs");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }
        }

        private void btnGtekTraditioneel_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (G100.Checked) Operation.RunMacro("Voorblad100.cs");
                if (G101.Checked) Operation.RunMacro("#BS_Tekeningen_2.101.cs");
                if (G102.Checked) Operation.RunMacro("#BS_Tekeningen_2.102.cs");
                if (G103.Checked) Operation.RunMacro("#BS_Tekeningen_2.103.cs");
                if (G104.Checked) Operation.RunMacro("#BS_Tekeningen_2.104.cs");
                if (G105.Checked) Operation.RunMacro("#BS_Tekeningen_2.105.cs");
                if (G106.Checked) Operation.RunMacro("#BS_Tekeningen_2.106.cs");
                if (G107.Checked) Operation.RunMacro("#BS_Tekeningen_2.107.cs");
                if (G108.Checked) Operation.RunMacro("#BS_Tekeningen_2.108.cs");
                if (G109.Checked) Operation.RunMacro("#BS_Tekeningen_2.109.cs");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }
        }

        private void btnGtekm3_Click(object sender, EventArgs e)
        {
            try
            {
                if (G500.Checked) Operation.RunMacro("Voorblad500.cs");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (!Operation.IsNumberingUpToDateAll())
            {
                var mbox = MessageBox.Show("Nummering is niet up-to-date" + Environment.NewLine + Environment.NewLine + "Betonstaal- �n Teklanummering uitvoeren?", "Nummering", MessageBoxButtons.YesNo);

                if (mbox == DialogResult.Yes)
                {
                    NummerenButton_Click_1(sender, e);
                    TeklaStructures.CommonTasks.PerformNumbering(true);
                }
                else
                {

                }
            }

        }

        private void SwitchSelectionButton_Click_1(object sender, EventArgs e)
        {
            G0.Checked = !G0.Checked;
            G1.Checked = !G1.Checked;
            G2.Checked = !G2.Checked;
            G3.Checked = !G3.Checked;
            G4.Checked = !G4.Checked;
            G5.Checked = !G5.Checked;
            G6.Checked = !G6.Checked;
            G7.Checked = !G7.Checked;
            G8.Checked = !G8.Checked;
            G9.Checked = !G9.Checked;
            G10.Checked = !G10.Checked;

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private string _versie;

        private void buttonVersion_Click(object sender, EventArgs e)
        {
            var version = TeklaStructuresInfo.GetCurrentProgramVersion();
            CurrentVersionLabel.Text = version;

            if (version.Contains("2019"))
            {
                if (version.Contains("2019.1"))
                {
                    _versie = version.Split(' ')[0];
                }
                else
                {
                    _versie = version.Split(' ')[0] + ".0";
                }
            }


            if (version.Contains("2020"))
            {
                if (version.Contains("2020.1"))
                {
                    _versie = version.Split(' ')[0];
                }
                else
                {
                    _versie = version.Split(' ')[0] + ".0";
                }
            }

        }

        private void tabControl_Click(object sender, EventArgs e)
        {
            buttonVersion_Click(sender, e);
        }


        private void buttonCleanUp_Click(object sender, EventArgs e)
        {
            var mappie = @"C:\TeklaStructures\" + _versie + "\\Environments\\netherlands";
            var mappieSteel = @"C:\TeklaStructures\" + _versie + "\\Environments\\netherlands\\Steel";
            var counter = 0;

            var ext = new List<string>();
            var steel = new List<string>();
            var removedFiles = new List<string>();

            if (checkBoxLay.Checked)
                ext.Add(".lay");
            if (checkBoxCbm.Checked)
                ext.Add(".cbm");
            if (checkBoxGaDrawings.Checked)
                ext.Add(".gd");
            if (checkBoxADrawings.Checked)
            {
                ext.Add(".ad");
                steel.Add(".vf");
                steel.Add(".copt");
            }

            if (checkBoxSObjGrp.Checked)
                ext.Add(".SObjGrp");

            var fileArray = Directory.GetFiles(mappie, "*.*", SearchOption.AllDirectories)
                .Where(f => ext.IndexOf(Path.GetExtension(f)) >= 0).ToArray();

            var steelFileArray = Directory.GetFiles(mappieSteel, "*.*", SearchOption.AllDirectories)
                .Where(g => steel.IndexOf(Path.GetExtension(g)) >= 0).ToArray();


            if (fileArray.Any())
            {
                foreach (var i in fileArray)
                {
                    var j = new FileInfo(i);
                    if (!(j.Name.Contains("standard")))
                    {
                        removedFiles.Add(j.FullName);
                        j.Delete();
                        counter++;
                    }
                }
            }


            if (steelFileArray.Any())
            {
                foreach (var i in steelFileArray)
                {
                    var j = new FileInfo(i);
                    if (!(j.Name.Contains("standard")))
                    {
                        removedFiles.Add(j.FullName);
                        j.Delete();
                        counter++;
                    }
                }
            }

            listBox1.Visible = true;
            listBox1.DataSource = removedFiles;
            labelRemoved.Text = $"Er zijn {counter} bestanden verwijderd.";
        }

        private void btnKorf_Click_1(object sender, EventArgs e)
        {
            //alleen losse staven, rest uitzetten Gena 220315
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korven = new ArrayList();
                var wapKorven = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(moe.Current);

                        if (part.Name == "KORF")
                        {
                            var _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                var _id = part.Identifier.ID;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            korven.Add(part);
                        }
                    }
                }

                Operation.DisplayPrompt(korven.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;

                foreach (TSM.Part export in korven)
                {
                    wapKorven.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.RebarGroup bars)
                        {
                            if (bars.NumberingSeries.Prefix != "LRO" || bars.NumberingSeries.Prefix != "LRB" || bars.NumberingSeries.Prefix != "LRF")
                            {
                                wapKorven.Add(bars);
                            }
                        }

                        if (child.Current is TSM.SingleRebar bar)
                        {
                            if (bar.NumberingSeries.Prefix != "LRO" || bar.NumberingSeries.Prefix != "LRB" || bar.NumberingSeries.Prefix != "LRF")
                            {
                                wapKorven.Add(bar);
                            }

                            if (bar.NumberingSeries.Prefix != "D")
                            {
                                wapKorven.Add(bar);
                            }
                        }
                    }
                    //}

                    //gefilterde staven
                    modelObjectSelector.Select(wapKorven);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var bestandsnaamFase = string.Empty;
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                Operation.DisplayPrompt("BVBS Korf Recht export gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void btnBalken_Click_1(object sender, EventArgs e)
        {

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balken = new ArrayList();


                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var naam = "";
                        part.GetReportProperty("NAME", ref naam);

                        if (naam == "BALK")
                        {
                            balken.Add(part);
                        }

                        if (naam == "BALK_SCHUIN")
                        {
                            balken.Add(part);
                        }

                        if (naam == "3_SNEDIGE_BALK")
                        {
                            balken.Add(part);
                        }

                        if (naam == "4_SNEDIGE_BALK")
                        {
                            balken.Add(part);
                        }

                    }
                }

                Operation.DisplayPrompt(balken.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };


                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balken)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }


                var prefixLr = prefixWapenings.Where(s => s.Prefix.StartsWith("LR")).Select(w => w.Wapening).ToList();
                var prefixLs = prefixWapenings.Where(s => s.Prefix.StartsWith("LS")).Select(w => w.Wapening).ToList();
                var prefixL3S = prefixWapenings.Where(s => s.Prefix.StartsWith("L3S")).Select(w => w.Wapening).ToList();
                var prefixL4S = prefixWapenings.Where(s => s.Prefix.StartsWith("L4S")).Select(w => w.Wapening).ToList();


                var dictir = new Dictionary<string, List<Wapening>>
                {
                    {"LR", prefixLr},
                    {"LS", prefixLs},
                    {"L3S", prefixL3S},
                    {"L4S", prefixL4S},

                };


                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }


                Operation.DisplayPrompt("BVBS export losse wapening gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }


        private void btnBalkenD_Click_1(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balken = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var naam = "";
                        part.GetReportProperty("NAME", ref naam);

                        if (naam == "BALK")
                        {
                            balken.Add(part);
                        }

                    }
                }

                Operation.DisplayPrompt(balken.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };



                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balken)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam
                                },
                            });
                        }

                    }
                }


                var prefixD = prefixWapenings.Where(s => s.Prefix.StartsWith("D")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                {
                    {"D", prefixD},
                };

                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }

                Operation.DisplayPrompt("BVBS export gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void btnKorfLos_Click_1(object sender, EventArgs e)
        {
            BtnKorfLos_Click(sender, e);
        }

        private void btnNetSup_Click_1(object sender, EventArgs e)
        {
            BtnNetSup_Click(sender, e);
        }

        private void BtnZbars_Click_1(object sender, EventArgs e)
        {
            BtnZbars_Click(sender, e);
        }

        private void btnKorvenSchuin_Click_1(object sender, EventArgs e)
        {
            BtnKorvenSchuin_Click(sender, e);
        }

        private void btnBalkenSchuin_Click_1(object sender, EventArgs e)
        {
            BtnBalkenSchuin_Click(sender, e);
        }

        private void btnDekkingSchuin_Click_1(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balkenSchuin = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var naam = "";
                        part.GetReportProperty("NAME", ref naam);

                        if (naam == "BALK_SCHUIN")
                        {
                            balkenSchuin.Add(part);
                        }

                    }
                }

                Operation.DisplayPrompt(balkenSchuin.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };



                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balkenSchuin)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }


                var prefixD = prefixWapenings.Where(s => s.Prefix.StartsWith("D")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                {
                    {"DS", prefixD},
                };

                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }

                Operation.DisplayPrompt("BVBS export gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void btnKorfHrsp_Click_1(object sender, EventArgs e)
        {
            BtnKorfHrsp_Click(sender, e);
        }

        private void btnMatOpMaat_Click(object sender, EventArgs e)
        {
            btnMatOpMaat_Click(sender, e);
        }

        private void BtnPoer_Click_1(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var poeren = new ArrayList();
                var wapPoeren = new ArrayList();
                var stiepen = new ArrayList();
                var stiepenLos = new ArrayList();
                var wapStiepen = new ArrayList();
                var wapStiepenLos = new ArrayList();
                var wapPoerenDekking = new ArrayList();
                var selectedObjects = new ArrayList();
                var acn = 0;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var merkPrefix = "";
                        part.GetReportProperty("ASSEMBLY_PREFIX", ref merkPrefix);

                        if (merkPrefix == "PK" || merkPrefix == "ZP") //prefix van de submerken van de poer korven)
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            poeren.Add(part);
                        }

                        if (merkPrefix == "PSK" || merkPrefix == "PSB" || merkPrefix == "BPS") //prefix van de submerken poerStiep. PSK is korf, PSB en BPS zijn betonvorm voor Los
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            if (merkPrefix == "PSK")
                            {
                                stiepen.Add(part);
                            }
                            else
                                stiepenLos.Add(part);

                        }
                    }
                }

                Operation.DisplayPrompt(poeren.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";

                foreach (TSM.Part export in poeren)
                {
                    wapPoeren.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {
                            if (bars.NumberingSeries.Prefix != "PF" || bars.NumberingSeries.Prefix != "DF")
                            {
                                wapPoeren.Add(bars);
                            }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapPoeren);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                foreach (TSM.Part export in stiepen)
                {
                    wapStiepen.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {
                            wapStiepen.Add(bars);

                        }


                        //gefilterde staven
                        modelObjectSelector.Select(wapStiepen);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("DrwNameSource", 0);
                        if (RadAlles.Checked)
                        {
                            component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                        }

                        if (RadPerFase.Checked)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            export.GetReportProperty("PHASE", ref fase);
                            export.GetReportProperty("PHASE.NAME", ref faseNaam);
                            var map = "fase " + fase + " - " + faseNaam;
                            var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                            if (!Directory.Exists("BVBS\\" + map))
                            {
                                Directory.CreateDirectory("BVBS\\" + map);
                            }

                            component.SetAttribute("FolderName", "BVBS\\" + map);
                            component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                        }

                        component.Insert();

                    }
                }

                foreach (TSM.Part export in stiepenLos)
                {
                    wapStiepen.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = "L" + merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {
                            wapStiepenLos.Add(bars);

                        }

                        //gefilterde staven
                        modelObjectSelector.Select(wapStiepenLos);
                        component.LoadAttributesFromFile("bs_standaard");
                        component.SetAttribute("DrwNameSource", 0);
                        if (RadAlles.Checked)
                        {
                            component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                        }

                        if (RadPerFase.Checked)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            export.GetReportProperty("PHASE", ref fase);
                            export.GetReportProperty("PHASE.NAME", ref faseNaam);
                            var map = "fase " + fase + " - " + faseNaam;
                            var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                            if (!Directory.Exists("BVBS\\" + map))
                            {
                                Directory.CreateDirectory("BVBS\\" + map);
                            }

                            component.SetAttribute("FolderName", "BVBS\\" + map);
                            component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                        }

                        component.Insert();

                    }
                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export Poeren gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Poeren Export: " + ex);
            }

            //BtnPoer_Click(sender, e); //oude versie
        }

        private void btnKorf3_Click_1(object sender, EventArgs e)
        {
            var model = new TSM.Model();

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korven3 = new ArrayList();
                var wapKorven3 = new ArrayList();
                var selectedObjects = new ArrayList();
                var map = string.Empty;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);

                        if (part.Name == "3_SNEDIGE_KORF")
                        {
                            var _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                var _id = part.Identifier.ID;
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            korven3.Add(part);
                        }
                    }

                }

                Operation.DisplayPrompt(korven3.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";

                foreach (TSM.Part export in korven3)
                {
                    wapKorven3.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;
                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    if (bars.NumberingSeries.Prefix != "L3SO" || bars.NumberingSeries.Prefix != "L3SB" || bars.NumberingSeries.Prefix != "L3SF")
                                    {
                                        wapKorven3.Add(bars);
                                    }

                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    if (bar.NumberingSeries.Prefix != "L3SO" || bar.NumberingSeries.Prefix != "L3SB" || bar.NumberingSeries.Prefix != "L3SF")
                                    {
                                        wapKorven3.Add(bar);
                                    }

                                    if (bar.NumberingSeries.Prefix != "D")
                                    {
                                        wapKorven3.Add(bar);
                                    }

                                    break;
                                }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapKorven3);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsNaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsNaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void btnBalken3_Click_1(object sender, EventArgs e)
        {
            BtnBalken3_Click(sender, e);
        }

        private void btnDekking3snedig_Click_1(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balken3snedig = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var naam = "";
                        part.GetReportProperty("NAME", ref naam);

                        if (naam == "3_SNEDIGE_BALK")
                        {
                            balken3snedig.Add(part);
                        }

                    }
                }

                Operation.DisplayPrompt(balken3snedig.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput)
                {
                    Name = "BVBSExport",
                    Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER
                };



                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balken3snedig)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }


                var prefixD = prefixWapenings.Where(s => s.Prefix.StartsWith("D")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                {
                    {"D3S", prefixD},
                };

                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}1.abs");
                            component.Insert();
                        }
                    }
                }

                Operation.DisplayPrompt("BVBS export gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void BtnDrawingsBalk_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                // TSM.Operations.Operation.RunMacro("#BS_TekeningenBalk.cs");
                CreateNewSamenstellingDrawings<TSM.Beam>("#balk_filter", "BALK", "#bs_balk");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }

            Cursor.Current = Cursors.Default;

        }
        // private readonly DrawingHandler MyDrawingHandler;
        //private string GetDrawingTypeCharacter(Drawing DrawingInstance)
        //{
        //    string Result = "U"; // Unknown drawing

        //    if (DrawingInstance is GADrawing)
        //    {
        //        Result = "G";
        //    }
        //    else if (DrawingInstance is AssemblyDrawing)
        //    {
        //        Result = "A";
        //    }
        //    else if (DrawingInstance is CastUnitDrawing)
        //    {
        //        Result = "C";
        //    }
        //    else if (DrawingInstance is MultiDrawing)
        //    {
        //        Result = "M";
        //    }
        //    else if (DrawingInstance is SinglePartDrawing)
        //    {
        //        Result = "W";
        //    }

        //    return Result;
        //}

        private void WizardButton_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            CreateNewDrawings<TSM.Beam>("#korf_poer_filter", "KORF_POER", "#bs_poer");
            CreateNewDrawings<TSM.Beam>("#korf_poer_filter", "KORF_PK", "#bs_poer");
            CreateNewDrawings<TSM.Beam>("#korf_poer_filter", "POER", "#bs_poer");
            CreateNewDrawings<TSM.Beam>("#korf_stiep_filter", "KORF_PSK", "#bs_poer");
            CreateNewDrawings<TSM.Beam>("#korf_filter", "KORF", "#bs_korf");
            CreateNewDrawings<TSM.Beam>("#korf_filter", "KORF_KRN", "#bs_korf");
            CreateNewDrawings<TSM.Beam>("#korf_schuin_filter", "KORF_SCHUIN", "#bs_korf_schuin");
            CreateNewDrawings<TSM.Beam>("#korf_3-snedig_filter", "3_SNEDIGE_KORF", "#bs_korf_3snedig");
            CreateNewDrawings<TSM.Beam>("#korf_4-snedig_filter", "4_SNEDIGE_KORF", "#bs_korf_4snedig");
            CreateNewDrawings<TSM.Beam>("#stek_filter", "KORF_SK", "#bs_stekrek");
            CreateNewDrawings<TSM.Beam>("#hrsp_filter", "KORF_HK", "#bs_hrsp");
            CreateNewDrawings<TSM.Beam>("#korf_nok_filter", "KORF_NOK", "#bs_korf");
            CreateNewDrawings<TSM.Beam>("#korf_nok_filter", "KORF_NK", "#bs_poer");
            CreateNewDrawings<TSM.Beam>("#korf_kolom_filter", "KORF_KOLOM", "#bs_korf");
            CreateNewDrawings<TSM.Beam>("#korf_kolom_filter", "KORF_RDKK", "#bs_korf");
            CreateNewDrawings<TSM.Beam>("#zrek_filter", "KORF_SZK", "#bs_zrek");
            //CreateNewDrawings<TSM.ContourPlate>("#plaat_filter", "PLAAT", "#bs_vloeren", "VLOER");
            CreateNewDrawings<TSM.Beam>("#mat_op_maat_filter", "MAT_MOM", "#bs_net");

            Cursor.Current = Cursors.Default;
            DrawingStatus.Text = "";

        }

        public void CreateNewDrawings<T>(string filter, string name, string setting, string drawingName = "KORF") where T : TSM.Part
        {
            var moe = new TSM.Model().GetModelObjectSelector().GetObjectsByFilterName(filter);

            while (moe.MoveNext())
            {
                if (!(moe.Current is T part) || part.Name != name)
                    continue;
                var myDrawing = new CastUnitDrawing(part.GetAssembly().Identifier, setting);

                var customerReference = string.Empty;
                var mainPartCastUnitPos = string.Empty;

                ((T)part.GetAssembly().GetMainPart()).GetReportProperty("BS_CUST_REF", ref customerReference);
                ((T)part.GetAssembly().GetMainPart()).GetReportProperty("CAST_UNIT.CAST_UNIT.CAST_UNIT_POS", ref mainPartCastUnitPos);

                if (customerReference.Length >= 1)
                {
                    myDrawing.Name = customerReference + " - " + drawingName + " - " + mainPartCastUnitPos;
                }
                else
                {
                    if (mainPartCastUnitPos.Length > 1)
                    {
                        myDrawing.Name = drawingName + " - " + mainPartCastUnitPos;
                    }
                    else
                    {
                        ((T)part.GetAssembly().GetMainPart()).GetReportProperty("CAST_UNIT.CAST_UNIT_POS", ref mainPartCastUnitPos);
                        myDrawing.Name = drawingName + " - " + mainPartCastUnitPos;

                    }
                }

                myDrawing.Insert();

                DrawingStatus.Text = $@"Processing drawing {myDrawing.Name} ...";

            }
        }


        public static void CreateNewSamenstellingDrawings<T>(string filter, string name, string setting, string drawingName = "BALK") where T : TSM.Part
        {
            var moe = new TSM.Model().GetModelObjectSelector().GetObjectsByFilterName(filter);

            while (moe.MoveNext())
            {
                if (!(moe.Current is T part) || part.Name != name)
                    continue;
                var myDrawing = new CastUnitDrawing(part.GetAssembly().Identifier, setting)
                {
                    Name = drawingName
                };

                myDrawing.Insert();
            }
        }


        private void btnStekrek_Click(object sender, EventArgs e)
        {

            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korvenStekrek = new ArrayList();
                var wapKorvenStekrek = new ArrayList();
                var selectedObjects = new ArrayList();
                string map;


                while (moe.MoveNext())
                {
                    if (!(moe.Current is TSM.Part part))
                        continue;
                    selectedObjects.Add(part);

                    if (part.Name != "KORF_STEK")
                        continue;
                    var _acn = 0;
                    part.GetUserProperty("ACN", ref _acn);

                    if (_acn == 0)
                    {
                        var _id = part.Identifier.ID;
                        //_acn++;
                        part.SetUserProperty("ACN", _id);
                        part.Modify();
                    }

                    korvenStekrek.Add(part);

                }

                Operation.DisplayPrompt(korvenStekrek.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";

                foreach (TSM.Part export in korvenStekrek)
                {
                    wapKorvenStekrek.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        var bars = child.Current as TSM.RebarGroup;
                        if (bars != null)
                        {
                            if (bars.NumberingSeries.Prefix != "LSO" || bars.NumberingSeries.Prefix != "LSB" || bars.NumberingSeries.Prefix != "LSF")
                            {
                                wapKorvenStekrek.Add(bars);
                            }
                        }

                        var bar = child.Current as TSM.SingleRebar;
                        if (bar != null)
                        {
                            if (bar.NumberingSeries.Prefix != "LSO" || bar.NumberingSeries.Prefix != "LSB" || bar.NumberingSeries.Prefix != "LSF")
                            {
                                wapKorvenStekrek.Add(bar);
                            }

                            if (bar.NumberingSeries.Prefix != "D")
                            {
                                wapKorvenStekrek.Add(bar);
                            }
                        }
                    }
                    //}

                    //gefilterde staven
                    modelObjectSelector.Select(wapKorvenStekrek);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists("BVBS"))
            {
                Directory.CreateDirectory("BVBS");
            }
            if (!Directory.Exists("BVBS\\Vervallen"))
            {
                Directory.CreateDirectory("BVBS\\Vervallen");
            }

            if (!Directory.Exists("BS_Json"))
            {
                Directory.CreateDirectory("BS_Json");
            }
            if (!Directory.Exists("BS_Json\\Vervallen"))
            {
                Directory.CreateDirectory("BS_Json\\Vervallen");
            }

            if (!Directory.Exists("Plotfiles"))
            {
                Directory.CreateDirectory("Plotfiles");
            }
            if (!Directory.Exists("Plotfiles\\Vervallen"))
            {
                Directory.CreateDirectory("Plotfiles\\Vervallen");
            }

            //if (!System.IO.File.Exists(File)) //verplaatsen naar het daadwerkelijke nummeren om te voorkomen dat er uit een eerder geopend model wordt ingelezen.
            //{
            //    IDictionary<string, Dictionary<string, int>> mainDictionary = new Dictionary<string, Dictionary<string, int>>();
            //    var dictionary = new Dictionary<string, int>
            //    {
            //        {"start nummering na...", 9}
            //    };

            //    mainDictionary.Add("HK", dictionary);
            //    mainDictionary.Add("SK", dictionary);
            //    mainDictionary.Add("STR", dictionary);
            //    mainDictionary.Add("WN", dictionary);

            //    using (var bestand = System.IO.File.CreateText(File))
            //    {
            //        var serializer = new JsonSerializer
            //        {
            //            Formatting = Formatting.Indented
            //        };
            //        serializer.Serialize(bestand, mainDictionary);
            //    }
            //}

            var coverBottom = string.Empty;
            var coverTop = string.Empty;
            var coverSides = string.Empty;
            var klantNaam = string.Empty;
            var fullLength = 0d;

            Model.GetProjectInfo().GetUserProperty("PROJECT_USERFIELD9", ref coverBottom);
            Model.GetProjectInfo().GetUserProperty("PROJECT_USERFIELD10", ref coverTop);
            Model.GetProjectInfo().GetUserProperty("PROJECT_USERFIELD11", ref coverSides);
            Model.GetProjectInfo().GetUserProperty("PROJECT_USERFIELD12", ref klantNaam);
            Model.GetProjectInfo().GetUserProperty("BS_FullLength", ref fullLength);
            CoverBottomComboBox.Text = coverBottom;
            CoverTopComboBox.Text = coverTop;
            CoverSidesComboBox.Text = coverSides;
            CustomerComboBox.Text = klantNaam;
            FullLengthTextBox.Text = Convert.ToString(fullLength, CultureInfo.InvariantCulture);

        }

        private class Pdf
        {
            public int Prefix { get; set; }
            public string OriginalFileName { get; set; }
        }

        private void BtnMergePlotfiles_Click(object sender, EventArgs e)
        {
            const string path = ".\\Plotfiles";
            var bestandsNaam = path + "\\" + Modelnaam + ".pdf";

            if (File.Exists(bestandsNaam))
            {
                File.Delete(bestandsNaam);
            }


            var directoryInfo = new DirectoryInfo(path);
            var files = directoryInfo.GetFiles("*.pdf");

            //var pdffies2 = files.Select(file => file.Name).ToList();

            var pdffies4 = new List<Pdf>();

            foreach (var file in files)
            {
                var fileName = file.Name;
                var pdf = new Pdf
                {
                    OriginalFileName = file.FullName,
                    Prefix = 99999
                };

                if (fileName.Contains('_'))
                {
                    var splitted = fileName.Split('_');
                    if (splitted.Any())
                    {
                        if (int.TryParse(splitted[0], out var number))
                        {
                            pdf.Prefix = number;
                        }
                    }
                }

                pdffies4.Add(pdf);
            }

            if (pdffies4.Any())
            {

                pdffies4 = pdffies4.OrderBy(q => q.Prefix).ToList();

                using (var outPdf = new PdfDocument())
                {
                    foreach (var pdfje in pdffies4)
                    {
                        using (var doc = PdfReader.Open(pdfje.OriginalFileName, PdfDocumentOpenMode.Import))
                        {
                            CopyPages(doc, outPdf);
                        }
                    }

                    outPdf.Save(bestandsNaam);
                }

                Operation.DisplayPrompt($"Er zijn {pdffies4.Count} bestanden samengevoegd.");
            }
            else
            {
                MessageBox.Show("Er zijn geen pdf bestanden om samen te voegen.");
            }


            void CopyPages(PdfDocument from, PdfDocument to)
            {
                for (var i = 0; i < from.PageCount; i++)
                {
                    to.AddPage(from.Pages[i]);
                }
            }

        }

        [Obsolete]
        private void PdfModifyButton_Click(object sender, EventArgs e)
        {
            const string path = ".\\Plotfiles";
            var directoryInfo = new DirectoryInfo(path);
            var files = directoryInfo.GetFiles("*.pdf");

            var pdffies = files.Select(file => file.Name).ToList();

            foreach (var f in pdffies)
            {
                var file = f;
                var bestand = Path.Combine(path, file);
                var pd = PdfReader.Open(bestand, PdfDocumentOpenMode.Modify);

                var page = pd.Pages[0];
                var tekst = XGraphics.FromPdfPage(page);
                var font = new XFont("Verdana", 20, XFontStyle.Bold);

                tekst.DrawString("Hallo Arie!", font, XBrushes.Black,
                    new XRect(50, 200, page.Width, page.Height),
                    XStringFormat.TopLeft);

                var newName = "#" + file;
                var filename = Path.Combine(path, newName);
                pd.Save(filename);

            }
        }

        private void btnGtekKelderwanden_Click(object sender, EventArgs e)
        {
            try
            {
                if (G200.Checked)
                    Operation.RunMacro("Voorblad200.cs");
                if (G201.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.201.cs");
                if (G202.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.202.cs");
                if (G203.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.203.cs");
                if (G204.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.204.cs");
                if (G205.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.205.cs");
                if (G206.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.206.cs");
                if (G207.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.207.cs");
                if (G208.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.208.cs");
                if (G209.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.209.cs");
                if (G210.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.210.cs");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }
        }

        private void Tradional_Click(object sender, EventArgs e)
        {

        }

        private void BtnGtekBreedplaten_Click(object sender, EventArgs e)
        {
            try
            {
                if (G300.Checked)
                    Operation.RunMacro("Voorblad300.cs");
                if (G301.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.301.cs");
                if (G302.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.302.cs");
                if (G303.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.303.cs");
                if (G304.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.304.cs");
                if (G305.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.305.cs");
                if (G306.Checked)
                    Operation.RunMacro("#BS_Tekeningen_2.306.cs");
                if (G307.Checked) Operation.RunMacro("#BS_Tekeningen_2.307.cs");
                if (G308.Checked) Operation.RunMacro("#BS_Tekeningen_2.308.cs");

            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }
        }

        private void BtnGtekStroken_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    if (G401.Checked) Operation.RunMacro("#BS_Tekeningen_2.401.cs");
                    if (G402.Checked) Operation.RunMacro("#BS_Tekeningen_2.402.cs");
                    if (G403.Checked) Operation.RunMacro("#BS_Tekeningen_2.403.cs");
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Melding: " + ex);
                }
            }
        }

        private void BtnDrawingBalkLos_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                CreateNewSamenstellingDrawings<TSM.Beam>("#balk_filter", "BALK_LOS", "#bs_balk_los");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding: " + ex);
            }

            Cursor.Current = Cursors.Default;
        }

        private void NummerenButton_Click_1(object sender, EventArgs e)
        {
            var currentModel = new TSM.Model();
            var pad = currentModel.GetInfo().ModelPath;
            var file = pad + "\\BetonstaalNummering.json";

            if (!File.Exists(file))
            {
                IDictionary<string, Dictionary<string, int>> mainDictionary = new Dictionary<string, Dictionary<string, int>>();
                var dictionary = new Dictionary<string, int>
                {
                    {"start nummering na...", 9}
                };

                mainDictionary.Add("HK", dictionary); // haarpeld - rebarbeam
                mainDictionary.Add("SK", dictionary); //stekrek - Lshape - rebarbeam
                mainDictionary.Add("WM", dictionary); //wandnet
                mainDictionary.Add("SZK", dictionary); //z rack - rebartype
                mainDictionary.Add("MOM", dictionary); //mat op maat, custom mesh
                mainDictionary.Add("SDM", dictionary); //standaard mat vloer
                mainDictionary.Add("SRM", dictionary); //strip mesh

                using (var bestand = File.CreateText(file))
                {
                    var serializer = new JsonSerializer
                    {
                        Formatting = Formatting.Indented
                    };
                    serializer.Serialize(bestand, mainDictionary);
                }
            }
            

            Cursor.Current = Cursors.WaitCursor;

            var moe = new TSM.Model().GetModelObjectSelector().GetAllObjectsWithType(new[] { typeof(TSM.Part) });
            while (moe.MoveNext())
            {
                if (moe.Current is TSM.Part myPart && (myPart.AssemblyNumber.Prefix != "D" && myPart.AssemblyNumber.Prefix != "TR_100"))
                {
                    string rebarType = null;
                    string meshType = null;
                    var prefix = myPart.AssemblyNumber.Prefix;
                    myPart.GetReportProperty("RebarType", ref rebarType);
                    myPart.GetReportProperty("BS_MESH_TYPE", ref meshType);
                   
                    if (meshType != null && meshType.Length > 1)
                    {
                       var codering = meshType;
                        if (!string.IsNullOrEmpty(codering))
                        {
                            if (myPart.Name == "MAT_SRM" ||
                                myPart.Name == "MAT_SDM")
                            {
                                var mainDictionary = JsonConvert.DeserializeObject<IDictionary<string, Dictionary<string, int>>>(File.ReadAllText(file));

                                foreach (var d in mainDictionary)
                                {
                                    var prfx = d.Key;
                                    if (prfx == prefix)
                                    {
                                        var dictionary = d.Value;

                                        if (!dictionary.TryGetValue(codering, out var value))
                                        {
                                            var newStartNumber = dictionary.Values.Max() + 1;
                                            dictionary.Add(codering, newStartNumber);
                                            myPart.AssemblyNumber.StartNumber = -newStartNumber;
                                        }
                                        else
                                        {
                                            myPart.AssemblyNumber.StartNumber = -value;
                                        }
                                    }
                                }

                                myPart.Modify();

                                using (var bestand = File.CreateText(file))
                                {
                                    var serializer = new JsonSerializer
                                    {
                                        Formatting = Formatting.Indented
                                    };
                                    serializer.Serialize(bestand, mainDictionary);
                                }
                            }
                        }
                    }
                    else if (rebarType != null && rebarType.Length > 1)
                    {
                       var codering = rebarType;

                        if (!string.IsNullOrEmpty(codering))
                        {
                            if (myPart.Name == "KORF_HK" ||
                                myPart.Name == "KORF_SK" ||
                                myPart.Name == "MAT_WM" ||
                                myPart.Name == "MAT_MOM" ||
                                myPart.Name == "KORF_SZK")
                            {
                                var mainDictionary = JsonConvert.DeserializeObject<IDictionary<string, Dictionary<string, int>>>(File.ReadAllText(file));

                                foreach (var d in mainDictionary)
                                {
                                    var prfx = d.Key;
                                    if (prfx == prefix)
                                    {
                                        var dictionary = d.Value;

                                        if (!dictionary.TryGetValue(codering, out var value))
                                        {
                                            var newStartNumber = dictionary.Values.Max() + 1;
                                            dictionary.Add(codering, newStartNumber);
                                            myPart.AssemblyNumber.StartNumber = -newStartNumber;
                                        }
                                        else
                                        {
                                            myPart.AssemblyNumber.StartNumber = -value;
                                        }
                                    }
                                }

                                myPart.Modify();

                                using (var bestand = File.CreateText(file))
                                {
                                    var serializer = new JsonSerializer
                                    {
                                        Formatting = Formatting.Indented
                                    };
                                    serializer.Serialize(bestand, mainDictionary);
                                }
                            }
                        }
                    }
                }
                
            }

            TeklaStructures.CommonTasks.PerformNumbering(true);
            currentModel.CommitChanges();
            Cursor.Current = Cursors.Default;
        }


        private void CopyTplButton_Click(object sender, EventArgs e)
        {
            const string stempelBestand = "bs-logo-klant.tpl";
            const string logoBestand = "bs-logo-klant.png";
            var model = new TSM.Model();
            var modelPad = model.GetInfo().ModelPath;
            var modelPadFileStempel = modelPad + "\\" + stempelBestand;
            var modelPadFileLogo = modelPad + "\\" + logoBestand;

            var mapMetTpl = Path.Combine(@"C:\\Teklanten\Betonstaal\klant_logo\", _stempelPad); //testen jeroen lokaal. Ook aanpassen bij CustomerComboBox
                                                                                                //  var mapMetTpl = Path.Combine(@"K:\klant_logo\", _stempelPad);

            var teKopierenStempel = Path.Combine(mapMetTpl, stempelBestand);
            var teKopierenLogo = Path.Combine(mapMetTpl, logoBestand);

            try
            {
                File.Copy(teKopierenStempel, modelPadFileStempel, true);

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding kopieren stempel png bestand...");
                throw;
            }

            try
            {
                File.Copy(teKopierenLogo, modelPadFileLogo, true);

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding kopieren logo png bestand...");
                throw;
            }

            CustomerComboBox.Text = CustomerComboBox.SelectedText;
        }

        private void CustomerComboBox_MouseClick(object sender, MouseEventArgs e)
        {
            CustomerComboBox.Items.Clear();
            var lijstMetKlanten = Path.Combine(@"C:\Teklanten\Betonstaal\klant_logo\", "Klant_logo_lijst.txt"); //testen jeroen lokaal. Ook aanpassen bij CopyTplButton
                                                                                                                //  var lijstMetKlanten = Path.Combine(@"K:\klant_logo\", "Klant_logo_lijst.txt");

            var klanten = System.IO.File.ReadAllLines(lijstMetKlanten).Where(line => !string.IsNullOrWhiteSpace(line));
            foreach (var klant in klanten)
            {
                CustomerComboBox.Items.Add(klant);
            }
        }

        private string _stempelPad;

        private void CustomerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            _stempelPad = CustomerComboBox.Text;
            Model.GetProjectInfo().SetUserProperty("PROJECT_USERFIELD12", _stempelPad);

            CopyTplButton_Click(sender, e);

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void CoverBottomComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coverBottom = CoverBottomComboBox.Text;
            Model.GetProjectInfo().SetUserProperty("PROJECT_USERFIELD9", coverBottom);
        }

        private void CoverTopComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coverTop = CoverTopComboBox.Text;
            Model.GetProjectInfo().SetUserProperty("PROJECT_USERFIELD10", coverTop);
        }

        private void CoverSidesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coverSides = CoverSidesComboBox.Text;
            Model.GetProjectInfo().SetUserProperty("PROJECT_USERFIELD11", coverSides);
        }

        private void tabProject_Click(object sender, EventArgs e)
        {

        }

        private void CoverInfo_Click(object sender, EventArgs e)
        {
            const string file = "TUTORIAL.docx";
            //const string path = @"C:\Temp\betonstaal\"; //lokaal Jeroen
            const string path = @"K:\basis_info\";  //server betonstaal
            var fullPath = Path.Combine(path, file);
            System.Diagnostics.Process.Start(fullPath);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            const string file = "fases.xlsx";
            //const string path = @"C:\Temp\betonstaal\"; //lokaal Jeroen
            const string path = @"K:\basis_info\";  //server betonstaal
            var fullPath = Path.Combine(path, file);
            System.Diagnostics.Process.Start(fullPath);
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            var tooltip = new ToolTip();
            tooltip.SetToolTip(CoverInfo, "Voegt '_LOS' toe aan de naam van de geselecteerde balken");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();
            var mos = new ModelObjectSelector();
            var moe = mos.GetSelectedObjects();

            while (moe.MoveNext())
            {
                var balk = (TSM.Part)moe.Current;
                if (balk == null)
                    continue;
                var currentName = balk.Name;
                var newName = currentName + "_LOS";
                balk.Name = newName;
                balk.Modify();
            }

            model.CommitChanges();
        }


        private void ExportButton_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;

            Cursor.Current = Cursors.WaitCursor;
            progressBar1.Value = 0;
            BtnBVBS_Click(sender, e);
            progressBar1.Value = 30;

            BtnIFC_Click(sender, e);
            progressBar1.Value = 60;

            BtnPdf_Click(sender, e);
            progressBar1.Value = 80;

            BtnMergePlotfiles_Click(sender, e);
            progressBar1.Value = 100;
            Thread.Sleep(400);
            Cursor.Current = Cursors.Default;
            progressBar1.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e) //JSON Wandnetten
        {
            try
            {
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);

                if (RadJsonAlles.Checked)
                {
                    new JsonWallMeshExporter(fullPath, false).Create();
                }

                if (RadJsonPerFase.Checked)
                {
                    new JsonWallMeshExporter(fullPath, true).Create();
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Wandnetten");
            }
        }


        private void JsonStekrekButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string stekRek = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, stekRek);

                if (RadJsonAlles.Checked)
                {
                    new JsonLShapedExporter(fullPath, false).Create();
                    new LooseLShapeExporter(fullPath, false).Create();
                }
                if (RadJsonPerFase.Checked)
                {
                    new JsonLShapedExporter(fullPath, true).Create();
                    new LooseLShapeExporter(fullPath, true).Create();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Stekrekken");
            }
        }

        private void JsonHrspButton_Click(object sender, EventArgs e)
        {
            try
            {

                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string hairpinDir = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, hairpinDir);


                if (RadJsonAlles.Checked)
                {
                    var objectSelector = new ModelObjectSelector();
                    var selectedObjects = objectSelector.GetSelectedObjects();

                    var looseList = new List<TSM.Part>();
                    var fixedList = new List<TSM.Part>();

                    while (selectedObjects.MoveNext())
                    {
                        if (selectedObjects.Current is TSM.Part beam)
                        {
                            if (beam.AssemblyNumber.Prefix.Equals("ZH"))
                            {
                                looseList.Add(beam);
                            }
                            if (beam.AssemblyNumber.Prefix.Equals("HK"))
                            {
                                fixedList.Add(beam);
                            }
                        }
                    }

                    if (looseList.Any())
                    {
                        new JsonLooseHairPinExporter(fullPath, true).Create();
                    }

                    if (fixedList.Any())
                    {
                        new JsonHairPinExporter(fullPath, false).Create();
                    }


                }
                if (RadJsonPerFase.Checked)
                {
                    var objectSelector = new ModelObjectSelector();
                    var selectedObjects = objectSelector.GetSelectedObjects();

                    var looseList = new List<TSM.Part>();
                    var fixedList = new List<TSM.Part>();

                    while (selectedObjects.MoveNext())
                    {
                        if (selectedObjects.Current is TSM.Part beam)
                        {
                            if (beam.AssemblyNumber.Prefix.Equals("ZH"))
                            {
                                looseList.Add(beam);
                            }
                            if (beam.AssemblyNumber.Prefix.Equals("HK"))
                            {
                                fixedList.Add(beam);
                            }
                        }
                    }

                    if (looseList.Any())
                    {
                        new JsonLooseHairPinExporter(fullPath, true).Create();
                    }

                    if (fixedList.Any())
                    {
                        new JsonHairPinExporter(fullPath, true).Create();
                    }
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Haarspelden");
            }

        }

        private void JsonKorvenButton_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedObjects = new ModelObjectSelector().GetSelectedObjects();
                var selectedParts = new List<TSM.Part>();

                while (selectedObjects.MoveNext())
                {
                    if (selectedObjects.Current is TSM.Part part)
                    {
                        var posNumber = part.GetAttributeProperty<string>("PART_POS");
                        if (posNumber.Contains("?"))
                        {
                            continue;
                        }

                        selectedParts.Add(part);
                    }
                }

                new JsonByModelRebarExporter(selectedParts).Create();

            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Test");
            }
        }

        private void KorbButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string korbDir = "BS_Json\\";
                var path = Path.Combine(modelPath, korbDir);

                if (RadJsonAlles.Checked)
                {
                    var objectSelector = new ModelObjectSelector();
                    var selectedObjects = objectSelector.GetSelectedObjects();

                    var looseList = new List<TSM.Part>();
                    var fixedList = new List<TSM.Part>();

                    while (selectedObjects.MoveNext())
                    {
                        if (!(selectedObjects.Current is TSM.Part beam))
                            continue;
                        if (beam.AssemblyNumber.Prefix.Equals("BR") ||
                            beam.AssemblyNumber.Prefix.Equals("KR") ||
                            beam.AssemblyNumber.Prefix.Equals("LR") ||
                            beam.AssemblyNumber.Prefix.Equals("ZKR") ||
                            beam.AssemblyNumber.Prefix.Equals("K3S") ||
                            beam.AssemblyNumber.Prefix.Equals("K4S") ||
                            beam.AssemblyNumber.Prefix.Equals("B4S") ||
                            beam.AssemblyNumber.Prefix.Equals("ZK3S") ||
                            beam.AssemblyNumber.Prefix.Equals("B3S"))
                        {
                            if (beam.GetUserAttributeProperty<string>("BS_IS_LOOSE").Equals("TRUE"))
                            {
                                //Its from a plugin loose rebar
                                if (beam.AssemblyNumber.Prefix.Equals("ZKR") || beam.AssemblyNumber.Prefix.Equals("ZK3S"))
                                {
                                    looseList.Add(beam);
                                }
                                if (beam.AssemblyNumber.Prefix.Equals("BR") || beam.AssemblyNumber.Prefix.Equals("B3S"))
                                {
                                    fixedList.Add(beam);
                                }
                            }
                            else
                            {
                                fixedList.Add(beam);
                            }
                        }
                    }

                    if (looseList.Any())
                    {
                        var looseKorbExporter = new LooseKorbExporter(path, false, looseList);
                        looseKorbExporter.Create();
                    }

                    if (fixedList.Any())
                    {
                        new MainKorbCoveringExporter(path, false, fixedList).Create();

                        var korbExporter = new KorbExporter(path, false, fixedList);
                        korbExporter.Create();

                        var mainKorbExporter = new MainKorbExporter(path, false, fixedList);
                        mainKorbExporter.Create();
                    }
                }
                if (RadJsonPerFase.Checked)
                {
                    var objectSelector = new ModelObjectSelector();
                    var selectedObjects = objectSelector.GetSelectedObjects();

                    var looseList = new List<TSM.Part>();
                    var fixedList = new List<TSM.Part>();

                    while (selectedObjects.MoveNext())
                    {
                        if (selectedObjects.Current is TSM.Part beam)
                        {

                            if (beam.AssemblyNumber.Prefix.Equals("BR") ||
                                beam.AssemblyNumber.Prefix.Equals("KR") ||
                                beam.AssemblyNumber.Prefix.Equals("LR") ||
                                beam.AssemblyNumber.Prefix.Equals("ZKR") ||
                                beam.AssemblyNumber.Prefix.Equals("K3S") ||
                                beam.AssemblyNumber.Prefix.Equals("K4S") ||
                                beam.AssemblyNumber.Prefix.Equals("B4S") ||
                                beam.AssemblyNumber.Prefix.Equals("ZK3S") ||
                                beam.AssemblyNumber.Prefix.Equals("B3S"))
                            {
                                if (beam.GetUserAttributeProperty<string>("BS_IS_LOOSE").Equals("TRUE"))
                                {
                                    //Its from a plugin loose rebar
                                    if (beam.AssemblyNumber.Prefix.Equals("ZKR") || beam.AssemblyNumber.Prefix.Equals("ZK3S"))
                                    {
                                        looseList.Add(beam);
                                    }
                                    if (beam.AssemblyNumber.Prefix.Equals("BR") || beam.AssemblyNumber.Prefix.Equals("B3S"))
                                    {
                                        fixedList.Add(beam);
                                    }
                                }
                                else
                                {
                                    fixedList.Add(beam);
                                }
                            }
                        }
                    }

                    if (looseList.Any())
                    {
                        var looseKorbExporter = new LooseKorbExporter(path, true, looseList);
                        looseKorbExporter.Create();
                    }

                    if (fixedList.Any())
                    {
                        new MainKorbCoveringExporter(path, true, fixedList).Create();

                        var korbExporter = new KorbExporter(path, true, fixedList);
                        korbExporter.Create();

                        var mainKorbExporter = new MainKorbExporter(path, true, fixedList);
                        mainKorbExporter.Create();
                    }
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Korb");
            }




        }

        private void StrMeshButton_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new TSM.Model();
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);

                if (checkBoxVisualizeJson.Checked)
                {
                    var selectedObjects = new List<TSM.Object>();
                    var selectedPlugins = new List<TSM.Component>();

                    var mos = new ModelObjectSelector();
                    var moe = mos.GetSelectedObjects();
                    while (moe.MoveNext())
                    {
                        if (moe.Current != null)
                        {
                            selectedObjects.Add(moe.Current);

                            if (moe.Current is TSM.Part strip)
                            {
                                if (strip.Name == "MAT_STR")
                                {
                                    var plugin = strip.GetFatherComponent();
                                    selectedPlugins.Add((TSM.Component)plugin);
                                }
                            }
                        }
                    }

                    Visualize(model, "StripMesh");

                    foreach (var plugin in selectedPlugins)
                    {
                        var children = plugin.GetChildren();

                        while (children.MoveNext())
                        {
                            if (children.Current is TSM.Beam beam)
                            {
                                selectedObjects.Add(beam);

                            }

                            if (children.Current is TSM.Brep brep)
                            {
                                selectedObjects.Add(brep);

                            }
                        }
                    }

                    var selectedObjectInclPlugin = selectedObjects.Distinct().ToList();
                    var selectedObjectsArray = new ArrayList();
                    selectedObjectsArray.AddRange(selectedObjectInclPlugin);
                    var modelObjectSelector = new ModelObjectSelector();
                    modelObjectSelector.Select(selectedObjectsArray);
                }

                if (RadJsonAlles.Checked)
                {
                    new JsonStripMeshTopExporter(fullPath, false).Create();
                    new JsonStripMeshBottomExporter(fullPath, false).Create();
                    new JsonStripMeshGirderExporter(fullPath, false).Create();
                    new JsonStripMeshSpacingExporter(fullPath, false).Create();
                }
                if (RadJsonPerFase.Checked)
                {
                    new JsonStripMeshTopExporter(fullPath, true).Create();
                    new JsonStripMeshBottomExporter(fullPath, true).Create();
                    new JsonStripMeshGirderExporter(fullPath, true).Create();
                    new JsonStripMeshSpacingExporter(fullPath, true).Create();
                }


                if (checkBoxResetJson.Checked)
                {
                    ResetVisualize(model, "StripMesh");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json STR-mesh");
            }
        }

        private void VMeshButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);

                if (RadJsonAlles.Checked)
                {
                    new JsonVMeshExporter(fullPath, false).Create();
                }
                if (RadJsonPerFase.Checked)
                {
                    new JsonVMeshExporter(fullPath, true).Create();
                }

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json V-mesh");
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RadJsonAlles_CheckedChanged(object sender, EventArgs e)
        {
            TxRadJsonbutton.Text = "A";
        }

        private void RadJsonPerFase_CheckedChanged(object sender, EventArgs e)
        {
            TxRadJsonbutton.Text = "P";
        }

        private void TxRadJsonbutton_TextChanged(object sender, EventArgs e)
        {

        }

        private void JsonFloorButton_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new TSM.Model();
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);


                if (checkBoxVisualizeJson.Checked)
                {
                    var selectedObjects = new List<TSM.Object>();
                    var selectedPlugins = new List<TSM.Component>();

                    var mos = new ModelObjectSelector();
                    var moe = mos.GetSelectedObjects();
                    while (moe.MoveNext())
                    {
                        if (moe.Current != null)
                        {
                            selectedObjects.Add(moe.Current);

                            //if (moe.Current is TSM.Component plugin)
                            //{
                            //    selectedPlugins.Add(plugin);
                            //}

                            if (moe.Current is TSM.Part floor)
                            {
                                if (floor.Name == "MAT_STD")
                                {
                                    var plugin = floor.GetFatherComponent();
                                    selectedPlugins.Add((TSM.Component)plugin);
                                }
                            }
                        }
                    }


                    Visualize(model, "RebarFloor");

                    foreach (var plugin in selectedPlugins)
                    {
                        var children = plugin.GetChildren();

                        while (children.MoveNext())
                        {
                            if (children.Current is TSM.Beam beam)
                            {
                                selectedObjects.Add(beam);

                            }

                            if (children.Current is TSM.Brep brep)
                            {
                                selectedObjects.Add(brep);

                            }
                        }
                    }

                    var selectedObjectInclPlugin = selectedObjects.Distinct().ToList();
                    var selectedObjectsArray = new ArrayList();
                    selectedObjectsArray.AddRange(selectedObjectInclPlugin);
                    var modelObjectSelector = new ModelObjectSelector();
                    modelObjectSelector.Select(selectedObjectsArray);

                }

                if (RadJsonAlles.Checked)
                {
                    new JsonFloorBottomExporter(fullPath, false).Create();
                    new JsonFloorTopExporter(fullPath, false).Create();
                    new JsonFloorGirderExporter(fullPath, false).Create();
                    new JsonFloorSpacingExporter(fullPath, false).Create();
                }
                if (RadJsonPerFase.Checked)
                {
                    new JsonFloorBottomExporter(fullPath, true).Create();
                    new JsonFloorTopExporter(fullPath, true).Create();
                    new JsonFloorGirderExporter(fullPath, true).Create();
                    new JsonFloorSpacingExporter(fullPath, true).Create();
                }

                if (checkBoxResetJson.Checked)
                {
                    ResetVisualize(model, "RebarFloor");
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Floor");
            }
        }

        private static void ResetVisualize(TSM.Model model, string pluginName)
        {
            var pluginList = new List<TSM.Component>();
            var moe = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.COMPONENT);
            while (moe.MoveNext())
            {
                if (moe.Current is TSM.Component plugin && plugin.Name == pluginName)
                {
                    pluginList.Add(plugin);
                }
            }

            foreach (var plugin in pluginList)
            {
                SetJsonAttribute(plugin, "Visualize", false);
            }

            model.CommitChanges();
        }

        private static void Visualize(TSM.Model model, string pluginName)
        {
            var pluginList = new List<TSM.Component>();
            var moe = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.COMPONENT);
            while (moe.MoveNext())
            {
                if (moe.Current is TSM.Component plugin && plugin.Name == pluginName)
                {
                    pluginList.Add(plugin);
                }
            }

            foreach (var plugin in pluginList)
            {
                SetJsonAttribute(plugin, "Visualize", true);
            }

            model.CommitChanges();
            TeklaStructures.CommonTasks.PerformNumbering(true);
        }

        private void checkBoxVisualizeJson_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxVisualizeJson.Checked)
            {
                StrMeshButton.BackColor = Color.DarkGray;
                JsonFloorButton.BackColor = Color.DarkGray;
            }
            else
            {
                StrMeshButton.BackColor = Color.Transparent;
                JsonFloorButton.BackColor = Color.Transparent;
            }
        }

        private void EmptyPhaseButton_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();
            var moe = model.GetModelObjectSelector().GetAllObjects();
            var phases = model.GetPhases();

            foreach (TSM.Phase f in phases)
            {
                f.SetUserProperty("content", "false");
            }

            while (moe.MoveNext())
            {
                if (moe.Current is TSM.Part part)
                {
                    var fase = 0;
                    part.GetReportProperty("PHASE", ref fase);

                    foreach (TSM.Phase f in phases)
                    {
                        if (fase == f.PhaseNumber)
                        {
                            var trueFalse = string.Empty;
                            f.GetUserProperty("content", ref trueFalse);
                            if (trueFalse == "false")
                            {
                                f.SetUserProperty("content", "true");
                            }
                        }
                    }
                }
            }

            var counter = 0;
            foreach (TSM.Phase f in phases)
            {
                var trueFalse = string.Empty;
                f.GetUserProperty("content", ref trueFalse);


                if (trueFalse == "false")
                {
                    f.Delete();
                    counter++;
                }
            }

            var tooltip = new ToolTip();
            tooltip.IsBalloon = true;
            tooltip.Show(counter - 1 + " fases verwijderd.", this, Cursor.Position.X - Location.X, Cursor.Position.Y - Location.Y - 50, 1300);

            EmptyPhaseButton.FlatAppearance.BorderColor = Color.Black;
            EmptyPhaseButton.FlatAppearance.BorderSize = 1;
            EmptyPhaseButton.FlatStyle = FlatStyle.Standard;

        }

        private void BVBS_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korvenStekrek = new ArrayList();
                var wapKorvenStekrek = new ArrayList();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (!(moe.Current is TSM.Part part))
                        continue;
                    selectedObjects.Add(part);

                    if (part.Name != "KORF_STEK_LOS")
                        continue;
                    var _acn = 0;
                    part.GetUserProperty("ACN", ref _acn);

                    if (_acn == 0)
                    {
                        var _id = part.Identifier.ID;
                        //_acn++;
                        part.SetUserProperty("ACN", _id);
                        part.Modify();
                    }

                    korvenStekrek.Add(part);

                }

                Operation.DisplayPrompt(korvenStekrek.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";

                foreach (TSM.Part export in korvenStekrek)
                {
                    wapKorvenStekrek.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        var bars = child.Current as TSM.RebarGroup;
                        if (bars != null)
                        {
                            if (bars.NumberingSeries.Prefix == "LST" || bars.NumberingSeries.Prefix == "LSH" || bars.NumberingSeries.Prefix == "LSF")
                            {
                                wapKorvenStekrek.Add(bars);
                            }
                        }

                        var bar = child.Current as TSM.SingleRebar;
                        if (bar != null)
                        {
                            if (bar.NumberingSeries.Prefix == "LST" || bar.NumberingSeries.Prefix == "LSH" || bar.NumberingSeries.Prefix == "LSF")
                            {
                                wapKorvenStekrek.Add(bar);
                            }
                        }
                    }
                    //}

                    //gefilterde staven
                    modelObjectSelector.Select(wapKorvenStekrek);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var map = string.Empty;
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();
                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void HrspLosButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balkenBij = new ArrayList();
                var wapBalkenZ = new ArrayList();
                balkenBij.Clear();
                wapBalkenZ.Clear();
                var selectedObjects = new ArrayList();

                while (moe.MoveNext())
                {
                    if (!(moe.Current is TSM.Part part))
                        continue;
                    selectedObjects.Add(part);
                    var naam = "";
                    part.GetReportProperty("NAME", ref naam);

                    if (naam.Contains("HRSP_LOS"))
                    {
                        balkenBij.Add(part);
                    }
                }

                Operation.DisplayPrompt(balkenBij.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;

                var wapenings = new List<Wapening>();
                var prefixWapenings = new List<PrefixWapening>();

                foreach (TSM.Part export in balkenBij)
                {
                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement reinforcement)
                        {
                            var fase = 0;
                            var faseNaam = string.Empty;
                            reinforcement.GetReportProperty("PHASE", ref fase);
                            reinforcement.GetReportProperty("PHASE.NAME", ref faseNaam);

                            prefixWapenings.Add(new PrefixWapening
                            {
                                Prefix = reinforcement.NumberingSeries.Prefix,
                                Wapening = new Wapening
                                {
                                    Reinforcement = reinforcement,
                                    Phase = fase,
                                    Phasename = faseNaam,
                                },
                            });
                        }

                    }
                }

                var prefixZ = prefixWapenings.Where(s => s.Prefix.StartsWith("L")).Select(w => w.Wapening).ToList();

                var dictir = new Dictionary<string, List<Wapening>>
                    {
                        {"Z", prefixZ}
                    }
                    ;

                foreach (var wapening in dictir)
                {

                    if (RadAlles.Checked)
                    {
                        var groupedByPrefix = wapening.Value.GroupBy(s => s.Prefix);

                        foreach (var group in groupedByPrefix)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));
                            component.LoadAttributesFromFile("bs_standaard");
                            component.SetAttribute("FileName", $".\\BVBS\\{wapening.Key}H1.abs");
                            component.Insert();
                        }
                    }


                    if (RadPerFase.Checked)
                    {
                        var groupedByPhase = wapening.Value.GroupBy(s => s.Phase);

                        foreach (var group in groupedByPhase)
                        {
                            var wapeningInGroup = group.Select(s => s.Reinforcement).ToList();
                            var faseName = group.Select(w => w.Phasename).ToList();

                            modelObjectSelector.Select(new ArrayList(wapeningInGroup));

                            component.LoadAttributesFromFile("bs_standaard");
                            var fullPath = $"BVBS\\fase {group.Key} - {faseName[0]}";
                            if (!Directory.Exists(fullPath))
                            {
                                Directory.CreateDirectory(fullPath);
                            }

                            component.SetAttribute("FolderName", fullPath);
                            component.SetAttribute("FileName", $".\\{fullPath}\\{faseName[0]}-{wapening.Key}H1.abs");
                            component.Insert();
                        }
                    }
                }

                Operation.DisplayPrompt("BVBS export Hrsp Los is gereed");
                mos.Select(selectedObjects);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export: " + ex);
            }
        }

        private void BtnBalk_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var balken = new ArrayList();
                var wapBalken = new ArrayList();
                var wapBalkenDekking = new ArrayList();
                var selectedObjects = new ArrayList();
                var acn = 0;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var merkPrefix = "";
                        part.GetReportProperty("ASSEMBLY_PREFIX", ref merkPrefix);

                        if (merkPrefix == "NK" || merkPrefix == "ZN" || merkPrefix == "NB")
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            balken.Add(part);
                        }
                    }
                }

                Operation.DisplayPrompt(balken.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";

                var wapenings = new List<Wapening>();

                #region nieuwe export vanaf dec 2021
                foreach (TSM.Part export in balken)
                {
                    wapBalken.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {
                            if (bars.NumberingSeries.Prefix != "NF" || bars.NumberingSeries.Prefix != "DF")
                            {
                                wapBalken.Add(bars);
                            }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapBalken);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }
                #endregion

                mos.Select(selectedObjects);
                //  Operation.DisplayPrompt("BVBS export Balken gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Balken Export: " + ex);
            }

        }

        private void JsonBijlegButton_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new TSM.Model();
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);


                var objectSelector = new ModelObjectSelector();
                var selectedObjects = objectSelector.GetSelectedObjects();

                var fixedList = new List<TSM.Part>();

                while (selectedObjects.MoveNext())
                {
                    if (selectedObjects.Current is TSM.Part beam)
                    {
                        if (beam.AssemblyNumber.Prefix.Equals("ZL"))
                        {
                            fixedList.Add(beam);
                        }
                    } 
                }

                if (fixedList.Any())
                {

                    if (RadJsonAlles.Checked)
                    {
                        new AdditionalJsonExport(fullPath, false).Create();
                        new AdditionalV2JsonExporter(fullPath, false).Create();
                    }

                    if (RadJsonPerFase.Checked)
                    {
                        new AdditionalJsonExport(fullPath, true).Create();
                        new AdditionalV2JsonExporter(fullPath, true).Create();
                    }
                }

            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json Bijleg");
            }
        }

        private void btnKorf4_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var korven4 = new ArrayList();
                var wapKorven4 = new ArrayList();
                var selectedObjects = new ArrayList();
                var map = string.Empty;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);

                        if (part.Name == "4_SNEDIGE_KORF")
                        {
                            var _acn = 0;
                            part.GetUserProperty("ACN", ref _acn);

                            if (_acn == 0)
                            {
                                var _id = part.Identifier.ID;
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            korven4.Add(part);
                        }
                    }

                }

                Operation.DisplayPrompt(korven4.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";

                foreach (TSM.Part export in korven4)
                {
                    wapKorven4.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;
                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    if (bars.NumberingSeries.Prefix != "L4SO" || bars.NumberingSeries.Prefix != "L4SB" || bars.NumberingSeries.Prefix != "L4SF")
                                    {
                                        wapKorven4.Add(bars);
                                    }

                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    if (bar.NumberingSeries.Prefix != "L4SO" || bar.NumberingSeries.Prefix != "L4SB" || bar.NumberingSeries.Prefix != "L4SF")
                                    {
                                        wapKorven4.Add(bar);
                                    }

                                    if (bar.NumberingSeries.Prefix != "D")
                                    {
                                        wapKorven4.Add(bar);
                                    }

                                    break;
                                }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapKorven4);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsNaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsNaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export 4-snedige korf: " + ex);
            }
        }

        private void BvbsFundDoorvoer_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var doorvoer = new ArrayList();
                var wapDoorvoer = new ArrayList();
                //var wapPoerenDekking = new ArrayList();
                var selectedObjects = new ArrayList();
                var acn = 0;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var merkPrefix = "";
                        part.GetReportProperty("ASSEMBLY_PREFIX", ref merkPrefix);

                        if (merkPrefix == "OD")
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            doorvoer.Add(part);
                        }
                    }
                }

                Operation.DisplayPrompt(doorvoer.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";

                //var wapenings = new List<Wapening>();
                //var prefixWapenings = new List<PrefixWapening>();


                foreach (TSM.Part export in doorvoer)
                {
                    wapDoorvoer.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = "Z" + merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {
                            if (bars.NumberingSeries.Prefix == "LD")
                            {
                                wapDoorvoer.Add(bars);
                            }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapDoorvoer);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export Doorvoeren gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Doorvoer Export: " + ex);
            }
        }

        private void FullLengthButton_Click(object sender, EventArgs e)
        {
            var model = new TSM.Model();
            var fullLength = Convert.ToDouble(FullLengthTextBox.Text);

            var projectUda = model.GetProjectInfo();
            projectUda.SetUserProperty("BS_FullLength", fullLength);
            projectUda.Modify();


        }

        private void PonsButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var pons = new ArrayList();
                var ponsKorven = new ArrayList();
                var selectedObjects = new ArrayList();


                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);

                        var _acn = 0;
                        if (part.Name == "PONS")
                        {
                            part.GetUserProperty("ACN", ref _acn);

                            var _id = part.Identifier.ID;
                            if (_acn == 0)
                            {
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            pons.Add(part);
                        }
                    }

                }

                Operation.DisplayPrompt(pons.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";
                var map = string.Empty;


                foreach (TSM.Part export in pons)
                {
                    ponsKorven.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;
                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    ponsKorven.Add(bars);
                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    ponsKorven.Add(bar);
                                    break;
                                }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(ponsKorven);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsNaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsNaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export Ponswapening: " + ex);
            }
        }

        private void ZrackButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var zrack = new ArrayList();
                var zrackKorven = new ArrayList();
                var selectedObjects = new ArrayList();


                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);

                        var _acn = 0;
                        if (part.Name == "KORF_Z_STEK")
                        {
                            part.GetUserProperty("ACN", ref _acn);

                            var _id = part.Identifier.ID;
                            if (_acn == 0)
                            {
                                //_acn++;
                                part.SetUserProperty("ACN", _id);
                                part.Modify();
                            }

                            zrack.Add(part);
                        }
                    }

                }

                Operation.DisplayPrompt(zrack.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";
                var acn = 0;
                var bestandsnaam = "";
                var map = string.Empty;


                foreach (TSM.Part export in zrack)
                {
                    zrackKorven.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    //export.GetReportProperty("ID", ref acn);  //dan altijd een acn!!!!
                    bestandsnaam = merk + "_" + acn;
                    if (checkBoxLosseKorf.Checked)
                    {
                        bestandsnaam = "L" + merk + "_" + acn;
                    }

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        switch (child.Current)
                        {
                            case TSM.RebarGroup bars:
                                {
                                    zrackKorven.Add(bars);
                                    break;
                                }
                            case TSM.SingleRebar bar:
                                {
                                    zrackKorven.Add(bar);
                                    break;
                                }
                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(zrackKorven);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        //fase ophalen
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        map = "fase " + fase + " - " + faseNaam;
                        var bestandsNaamFase = faseNaam + "-" + bestandsnaam;


                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsNaamFase + ".abs");
                    }

                    component.Insert();

                }

                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Export Z-rack: " + ex);
            }
        }

        private void CustomMeshButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelPath = new TSM.Model().GetInfo().ModelPath;
                const string directory = "BS_Json\\";
                var fullPath = Path.Combine(modelPath, directory);

                if (RadJsonAlles.Checked)
                {
                    new JsonCustomMeshExporter(fullPath, false).Create();
                }
                if (RadJsonPerFase.Checked)
                {
                    new JsonCustomMeshExporter(fullPath, true).Create();
                }

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Melding Betonstaal Json V-mesh");
            }
        }

        private void RondeKolomButton_Click(object sender, EventArgs e)
        {
            try
            {
                var modelObjectSelector = new ModelObjectSelector();
                var mos = new ModelObjectSelector();
                var moe = mos.GetSelectedObjects();

                var kolommen = new ArrayList();
                var wapKolommen = new ArrayList();
                var nokken = new ArrayList();
                var wapNokken = new ArrayList();
                // var wapKolommenDekking = new ArrayList();
                var selectedObjects = new ArrayList();
                var acn = 0;

                while (moe.MoveNext())
                {
                    if (moe.Current is TSM.Part part)
                    {
                        selectedObjects.Add(part);
                        var merkPrefix = "";
                        part.GetReportProperty("ASSEMBLY_PREFIX", ref merkPrefix);

                        if (merkPrefix == "RDKK" || merkPrefix == "ZRDK") //prefix van de submerken
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            kolommen.Add(part);
                        }

                        if (merkPrefix == "NK" || merkPrefix == "ZN" || merkPrefix == "ZPT") //prefix van de losse merken, en de put ZPT
                        {
                            part.GetUserProperty("ACN", ref acn);

                            var id = part.Identifier.ID;
                            if (acn == 0)
                            {
                                part.SetUserProperty("ACN", id);
                                part.Modify();
                            }

                            nokken.Add(part);
                        }
                    }
                }

                Operation.DisplayPrompt(kolommen.Count.ToString());

                var componentInput = new TSM.ComponentInput();
                componentInput.AddOneInputPosition(new TSG.Point(0, 0, 0));
                var component = new TSM.Component(componentInput);
                component.Name = "BVBSExport";
                component.Number = TSM.BaseComponent.PLUGIN_OBJECT_NUMBER;
                var merk = "";

                //var wapenings = new List<Wapening>();
                //var prefixWapenings = new List<PrefixWapening>();


                foreach (TSM.Part export in kolommen)
                {
                    wapKolommen.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {

                            wapKolommen.Add(bars);

                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapKolommen);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }

                foreach (TSM.Part export in nokken)
                {
                    wapNokken.Clear();
                    export.GetReportProperty("ASSEMBLY_POS", ref merk);
                    export.GetReportProperty("ACN", ref acn);
                    var bestandsnaam = merk + "_" + acn;

                    var child = export.GetChildren();
                    while (child.MoveNext())
                    {
                        if (child.Current is TSM.Reinforcement bars)
                        {

                            wapNokken.Add(bars);

                        }
                    }


                    //gefilterde staven
                    modelObjectSelector.Select(wapNokken);
                    component.LoadAttributesFromFile("bs_standaard");
                    component.SetAttribute("DrwNameSource", 0);
                    if (RadAlles.Checked)
                    {
                        component.SetAttribute("FileName", ".\\BVBS\\" + bestandsnaam + ".abs");
                    }

                    if (RadPerFase.Checked)
                    {
                        var fase = 0;
                        var faseNaam = string.Empty;
                        export.GetReportProperty("PHASE", ref fase);
                        export.GetReportProperty("PHASE.NAME", ref faseNaam);
                        var map = "fase " + fase + " - " + faseNaam;
                        var bestandsnaamFase = faseNaam + "-" + bestandsnaam;

                        if (!Directory.Exists("BVBS\\" + map))
                        {
                            Directory.CreateDirectory("BVBS\\" + map);
                        }

                        component.SetAttribute("FolderName", "BVBS\\" + map);
                        component.SetAttribute("FileName", ".\\BVBS\\" + map + "\\" + bestandsnaamFase + ".abs");
                    }

                    component.Insert();

                }


                mos.Select(selectedObjects);
                Operation.DisplayPrompt("BVBS export Ronde Kolom gereed");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Melding BVBS Ronde Kolom Export: " + ex);
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private static string GetJsonFilePath()
        {
            return EnvironmentFiles.GetAttributeFile("MontageExtraTemplate.json").FullName;
        }

        private void GetJsonButton_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();

                using (var r = new StreamReader(GetJsonFilePath()))

                {
                    var json = r.ReadToEnd();
                    var container = JsonConvert.DeserializeObject<TeklaProductsContainer>(json);
                    var counter = container.TeklaProducts.Count;
                    dataGridView1.Rows.Add(counter);

                    for (var i = 0; i < counter; i++)
                    {


                        dataGridView1.Rows[i].Cells[0].Value = container.TeklaProducts[i].piece_count;
                        dataGridView1.Rows[i].Cells[1].Value = container.TeklaProducts[i].reference;
                        dataGridView1.Rows[i].Cells[2].Value = container.TeklaProducts[i].fase;
                        dataGridView1.Rows[i].Cells[3].Value = container.TeklaProducts[i].sid_NL;
                    }
                }

                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Directory.Exists("BS_Json\\Montage Extra"))
                {
                    Directory.CreateDirectory("BS_Json\\Montage Extra");
                }

                const string fullFilePath = ".\\BS_Json\\Montage Extra\\Montage Extra.json";

                //var teklaProducts = new List<TeklaProduct>();
                var teklaProductContainer = new TeklaProductsContainer
                {
                    TeklaProducts = new List<TeklaProduct>()
                };


                //  MessageBox.Show(dataGridView1.SelectedRows.Count.ToString());

                var numberOfSelectedRows = dataGridView1.SelectedRows.Count;
                var balloonText = "Er zijn geen rijen geselecteerd.";

                if (numberOfSelectedRows > 0)
                {

                    foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                    {
                        var product = new TeklaProduct
                        {
                            producttype = "standard",
                            piece_count = dataGridView1.Rows[row.Index].Cells[0].Value.ToString(),
                            reference = dataGridView1.Rows[row.Index].Cells[1].Value.ToString(),
                            fase = dataGridView1.Rows[row.Index].Cells[2].Value.ToString(),
                            sid_NL = dataGridView1.Rows[row.Index].Cells[3].Value.ToString()
                        };

                        teklaProductContainer.TeklaProducts.Add(product);
                    }

                    using (var file = File.CreateText(fullFilePath))
                    {
                        var serializer = new JsonSerializer
                        {
                            Formatting = Formatting.Indented
                        };

                        serializer.Serialize(file, teklaProductContainer);
                    }

                    balloonText = "Json bestand is gemaakt.";
                }

                var tooltip = new ToolTip();
                tooltip.IsBalloon = true;
                tooltip.BackColor = Color.DeepSkyBlue;
                tooltip.Show(balloonText, this, button5.Location.X + 400, button5.Location.Y + 80, 1000);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}