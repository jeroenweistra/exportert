﻿using Tekla.Structures.Model;

namespace BS_Export
{
    public class Wapening
    {
        public Reinforcement Reinforcement { get; set; }
        public int Phase { get; set; }
        public string Phasename { get; set; }
        public string Prefix { get; set; }

        public override string ToString()
        {
            return $"{Phase} - {Phasename} - {Prefix}";
        }
    }
}
