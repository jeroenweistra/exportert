﻿using System.Collections.Generic;

namespace BS_Export
{
    public class TeklaProduct
    {
        public string producttype { get; set; }
        public string piece_count { get; set; }
        public string reference { get; set; }
        public string fase { get; set; }
        public string sid_NL { get; set; }
    }

    public class TeklaProductsContainer
    {
        public List<TeklaProduct> TeklaProducts { get; set; }
    }
}

