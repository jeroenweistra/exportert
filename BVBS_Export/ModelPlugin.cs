using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Tekla.Structures.Model;
using Tekla.Structures.Plugins;

namespace BVBS_Export
{
    public class PluginData
    {
        #region Fields

        [StructuresField("Klasse")] public string Klasse;
        [StructuresField("Radbutton")] public string Radbutton;
        [StructuresField("RadJsonbutton")] public string RadJsonbutton;
        [StructuresField("Visualize")] public int Visualize;
        [StructuresField("ResetJson")] public int ResetJson;
        [StructuresField("VisualizeJson")] public int VisualizeJson;
        [StructuresField("Reset")] public int Reset;
        [StructuresField("Colors")] public int Colors;
        [StructuresField("Bs0")] public int Bs0;
        [StructuresField("Bs1")] public int Bs1;
        [StructuresField("Bs2")] public int Bs2;
        [StructuresField("Bs3")] public int Bs3;
        [StructuresField("Bs4")] public int Bs4;
        [StructuresField("Bs5")] public int Bs5;
        [StructuresField("Bs6")] public int Bs6;
        [StructuresField("Bs7")] public int Bs7;
        [StructuresField("Bs8")] public int Bs8;
        [StructuresField("Bs9")] public int Bs9;
        [StructuresField("Bs10")] public int Bs10;
        [StructuresField("Bs101")] public int Bs101;
        [StructuresField("Bs102")] public int Bs102;
        [StructuresField("Bs103")] public int Bs103;
        [StructuresField("Bs104")] public int Bs104;
        [StructuresField("Bs105")] public int Bs105;
        [StructuresField("Bs106")] public int Bs106;
        [StructuresField("Bs107")] public int Bs107;
        [StructuresField("Bs108")] public int Bs108;
        [StructuresField("Bs109")] public int Bs109;
        [StructuresField("Bs100")] public int Bs100;
        [StructuresField("Bs500")] public int Bs500;
        [StructuresField("Bs200")] public int Bs200;
        [StructuresField("Bs201")] public int Bs201;
        [StructuresField("Bs202")] public int Bs202;
        [StructuresField("Bs203")] public int Bs203;
        [StructuresField("Bs204")] public int Bs204;
        [StructuresField("Bs205")] public int Bs205;
        [StructuresField("Bs206")] public int Bs206;
        [StructuresField("Bs207")] public int Bs207;
        [StructuresField("Bs208")] public int Bs208;
        [StructuresField("Bs209")] public int Bs209;
        [StructuresField("Bs210")] public int Bs210;
        [StructuresField("Bs300")] public int Bs300;
        [StructuresField("Bs301")] public int Bs301;
        [StructuresField("Bs302")] public int Bs302;
        [StructuresField("Bs303")] public int Bs303;
        [StructuresField("Bs304")] public int Bs304;
        [StructuresField("Bs305")] public int Bs305;
        [StructuresField("Bs306")] public int Bs306;
        [StructuresField("Bs307")] public int Bs307;
        [StructuresField("Bs308")] public int Bs308;
        [StructuresField("Bs401")] public int Bs401;
        [StructuresField("Bs402")] public int Bs402;
        [StructuresField("Bs403")] public int Bs403;
        [StructuresField("Customer")] public string Customer;
        [StructuresField("CoverBottom")] public string CoverBottom;
        [StructuresField("CoverTop")] public string CoverTop;
        [StructuresField("CoverSides")] public string CoverSides;
        [StructuresField("CoverStart")] public string CoverStart;
        [StructuresField("CoverEnd")] public string CoverEnd;
        [StructuresField("CoverInside")] public string CoverInside;
        [StructuresField("FullLength")] public double FullLength;

        #endregion
    }

    [Plugin("BVBS_Export")]
    [PluginUserInterface("BVBS_Export.MainForm")]
    [InputObjectDependency(InputObjectDependency.NOT_DEPENDENT)]
    public class BVBS_Export : PluginBase
    {
        #region Fields
        private Model _Model;
        private PluginData _Data;

        private string _klasse;
        private string _radbutton;
        private string _radJsonbutton;
        private int _visualize;
        private int _reset;
        private int _colors;
        private int _visualizeJson;
        private int _resetJson;
        private int _bs0;
        private int _bs1;
        private int _bs2;
        private int _bs3;
        private int _bs4;
        private int _bs5;
        private int _bs6;
        private int _bs7;
        private int _bs8;
        private int _bs9;
        private int _bs10;
        private int _bs100;
        private int _bs101;
        private int _bs102;
        private int _bs103;
        private int _bs104;
        private int _bs105;
        private int _bs106;
        private int _bs107;
        private int _bs108;
        private int _bs109;
        private int _bs500;
        private int _bs200;
        private int _bs201;
        private int _bs202;
        private int _bs203;
        private int _bs204;
        private int _bs205;
        private int _bs206;
        private int _bs207;
        private int _bs208;
        private int _bs209;
        private int _bs210;
        private int _bs300;
        private int _bs301;
        private int _bs302;
        private int _bs303;
        private int _bs304;
        private int _bs305;
        private int _bs306;
        private int _bs307;
        private int _bs308;
        private int _bs401;
        private int _bs402;
        private int _bs403;
        private string _customer;
        private string _coverBottom;
        private string _coverTop;
        private string _coverSides;
        private string _coverStart;
        private string _coverEnd;
        private string _coverInside;
        private double _fullLength;

        #endregion

        #region Properties
        private Model Model
        {
            set => _Model = value;
        }

        private PluginData Data
        {
            get => _Data;
            set => _Data = value;
        }
        #endregion

        #region Constructor
        public BVBS_Export(PluginData data)
        {
            Model = new Model();
            Data = data;
        }
        #endregion

        #region Overrides
        public override List<InputDefinition> DefineInput()
        {
            //
            // This is an example for selecting two points; change this to suit your needs.
            //
            //List<InputDefinition> PointList = new List<InputDefinition>();
            //Picker Picker = new Picker();
            //ArrayList PickedPoints = Picker.PickPoints(Picker.PickPointEnum.PICK_TWO_POINTS);

            //PointList.Add(new InputDefinition(PickedPoints));
            var pointList = new List<InputDefinition>();
            return pointList;
        }

        public override bool Run(List<InputDefinition> input)
        {
            try
            {
                GetValuesFromDialog();

                //
                // This is an example for selecting two points; change this to suit your needs.
                //
                //ArrayList Points = (ArrayList)Input[0].GetInput();
                //Point StartPoint = Points[0] as Point;
                //Point EndPoint = Points[1] as Point;
                //Beam beam = new Beam(StartPoint, EndPoint);
                //beam.Profile.ProfileString = "IPE500";
                //beam.Class = _klasse;
                //beam.Insert();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }

            return true;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Gets the values from the dialog and sets the default values if needed
        /// </summary>
        private void GetValuesFromDialog()
        {
            _klasse = Data.Klasse;
            _radbutton = Data.Radbutton;
            _radJsonbutton = Data.RadJsonbutton;
            _customer = Data.Customer;
            _colors = Data.Colors;
            _bs0 = Data.Bs0;
            _bs1 = Data.Bs1;
            _bs2 = Data.Bs2;
            _bs3 = Data.Bs3;
            _bs4 = Data.Bs4;
            _bs5 = Data.Bs5;
            _bs6 = Data.Bs6;
            _bs7 = Data.Bs7;
            _bs8 = Data.Bs8;
            _bs9 = Data.Bs9;
            _bs10 = Data.Bs10;
            _bs100 = Data.Bs100;
            _bs101 = Data.Bs101;
            _bs102 = Data.Bs102;
            _bs103 = Data.Bs103;
            _bs104 = Data.Bs104;
            _bs105 = Data.Bs105;
            _bs106 = Data.Bs106;
            _bs107 = Data.Bs107;
            _bs108 = Data.Bs108;
            _bs109 = Data.Bs109;
            _bs500 = Data.Bs500;
            _bs200 = Data.Bs200;
            _bs201 = Data.Bs201;
            _bs202 = Data.Bs202;
            _bs203 = Data.Bs203;
            _bs204 = Data.Bs204;
            _bs205 = Data.Bs205;
            _bs206 = Data.Bs206;
            _bs207 = Data.Bs207;
            _bs208 = Data.Bs208;
            _bs209 = Data.Bs209;
            _bs210 = Data.Bs210;
            _bs300 = Data.Bs300;
            _bs301 = Data.Bs301;
            _bs302 = Data.Bs302;
            _bs303 = Data.Bs303;
            _bs304 = Data.Bs304;
            _bs305 = Data.Bs305;
            _bs306 = Data.Bs306;
            _bs307 = Data.Bs307;
            _bs308 = Data.Bs308;
            _bs401 = Data.Bs401;
            _bs402 = Data.Bs402;
            _bs403 = Data.Bs403;
            _visualize = Data.Visualize;
            _reset = Data.Reset;
            _visualizeJson = Data.VisualizeJson;
            _resetJson = Data.ResetJson;
            _coverBottom = Data.CoverBottom;
            _coverTop = Data.CoverTop;
            _coverSides = Data.CoverSides;
            _coverStart = Data.CoverStart;
            _coverEnd = Data.CoverEnd;
            _coverInside = Data.CoverInside;
            _fullLength = Data.FullLength;
            

            if (IsDefaultValue(_klasse)) _klasse = "4";
            if (IsDefaultValue(_radbutton)) _radbutton = "A";
            if (IsDefaultValue(_radJsonbutton)) _radJsonbutton = "P";
            if (IsDefaultValue(_visualize)) _visualize = 0;
            if (IsDefaultValue(_reset)) _reset = 0;
            if (IsDefaultValue(_colors)) _colors = 0;
            if (IsDefaultValue(_visualizeJson)) _visualizeJson = 1;
            if (IsDefaultValue(_resetJson)) _resetJson = 0;
            if (IsDefaultValue(_customer)) _customer = "CDV";
            if (IsDefaultValue(_fullLength)) _fullLength = 4000d;

            if (IsDefaultValue(_bs0)) _bs0 = 0;
            if (IsDefaultValue(_bs1)) _bs1 = 0;
            if (IsDefaultValue(_bs2)) _bs2 = 0;
            if (IsDefaultValue(_bs3)) _bs3 = 0;
            if (IsDefaultValue(_bs4)) _bs4 = 0;
            if (IsDefaultValue(_bs5)) _bs5 = 0;
            if (IsDefaultValue(_bs6)) _bs6 = 0;
            if (IsDefaultValue(_bs7)) _bs7 = 0;
            if (IsDefaultValue(_bs8)) _bs8 = 0;
            if (IsDefaultValue(_bs9)) _bs9 = 0;
            if (IsDefaultValue(_bs10)) _bs10 = 0;
            if (IsDefaultValue(_bs100)) _bs100 = 0;
            if (IsDefaultValue(_bs101)) _bs101 = 0;
            if (IsDefaultValue(_bs102)) _bs102 = 0;
            if (IsDefaultValue(_bs103)) _bs103 = 0;
            if (IsDefaultValue(_bs104)) _bs104 = 0;
            if (IsDefaultValue(_bs105)) _bs105 = 0;
            if (IsDefaultValue(_bs106)) _bs106 = 0;
            if (IsDefaultValue(_bs107)) _bs107 = 0;
            if (IsDefaultValue(_bs108)) _bs108 = 0;
            if (IsDefaultValue(_bs109)) _bs109 = 0;
            if (IsDefaultValue(_bs500)) _bs500 = 0;
            if (IsDefaultValue(_bs200)) _bs200 = 0;
            if (IsDefaultValue(_bs201)) _bs201 = 0;
            if (IsDefaultValue(_bs202)) _bs202 = 0;
            if (IsDefaultValue(_bs203)) _bs203 = 0;
            if (IsDefaultValue(_bs204)) _bs204 = 0;
            if (IsDefaultValue(_bs205)) _bs205 = 0;
            if (IsDefaultValue(_bs206)) _bs206 = 0;
            if (IsDefaultValue(_bs207)) _bs207 = 0;
            if (IsDefaultValue(_bs208)) _bs208 = 0;
            if (IsDefaultValue(_bs209)) _bs209 = 0;
            if (IsDefaultValue(_bs210)) _bs210 = 0;
            if (IsDefaultValue(_bs300)) _bs300 = 0;
            if (IsDefaultValue(_bs301)) _bs301 = 0;
            if (IsDefaultValue(_bs302)) _bs302 = 0;
            if (IsDefaultValue(_bs303)) _bs303 = 0;
            if (IsDefaultValue(_bs304)) _bs304 = 0;
            if (IsDefaultValue(_bs305)) _bs305 = 0;
            if (IsDefaultValue(_bs306)) _bs306 = 0;
            if (IsDefaultValue(_bs307)) _bs307 = 0;
            if (IsDefaultValue(_bs308)) _bs308 = 0;
            if (IsDefaultValue(_bs401)) _bs401 = 0;
            if (IsDefaultValue(_bs402)) _bs402 = 0;
            if (IsDefaultValue(_bs403)) _bs403 = 0;

            if (IsDefaultValue(_coverBottom)) _coverBottom = "25";
            if (IsDefaultValue(_coverTop)) _coverTop = "25";
            if (IsDefaultValue(_coverSides)) _coverSides = "25";
            if (IsDefaultValue(_coverStart)) _coverStart = "25";
            if (IsDefaultValue(_coverEnd)) _coverEnd = "25";
            if (IsDefaultValue(_coverInside)) _coverInside = "25";
            
        }

        #endregion
    }
}
