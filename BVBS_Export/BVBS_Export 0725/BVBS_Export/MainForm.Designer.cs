namespace BVBS_Export
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.ParametersTabPage = new System.Windows.Forms.TabPage();
            this.BtnPoer = new System.Windows.Forms.Button();
            this.BtnIFC = new System.Windows.Forms.Button();
            this.btnBalken3 = new System.Windows.Forms.Button();
            this.BtnPdf = new System.Windows.Forms.Button();
            this.btnKorf3 = new System.Windows.Forms.Button();
            this.TxRadbutton = new System.Windows.Forms.TextBox();
            this.RadPerFase = new System.Windows.Forms.RadioButton();
            this.RadAlles = new System.Windows.Forms.RadioButton();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblNummering = new System.Windows.Forms.Label();
            this.btnNummeren = new System.Windows.Forms.Button();
            this.btnMatOpMaat = new System.Windows.Forms.Button();
            this.btnNetSup = new System.Windows.Forms.Button();
            this.btnKorfHrsp = new System.Windows.Forms.Button();
            this.btnKorfLos = new System.Windows.Forms.Button();
            this.btnBalkenSchuin = new System.Windows.Forms.Button();
            this.btnBalken = new System.Windows.Forms.Button();
            this.btnKorvenSchuin = new System.Windows.Forms.Button();
            this.btnKorf = new System.Windows.Forms.Button();
            this.okCancel1 = new Tekla.Structures.Dialog.UIControls.OkCancel();
            this.btnBVBS = new System.Windows.Forms.Button();
            this.TxKlasse = new System.Windows.Forms.TextBox();
            this.tabInstellingen = new System.Windows.Forms.TabPage();
            this.TxKorven = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Naam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inc_Exc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveLoad1 = new Tekla.Structures.Dialog.UIControls.SaveLoad();
            this.Info = new System.Windows.Forms.TabPage();
            this.lblDatum = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVersie = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.groupBoxPDF = new System.Windows.Forms.GroupBox();
            this.CbPdfC = new System.Windows.Forms.CheckBox();
            this.CbPdfG = new System.Windows.Forms.CheckBox();
            this.btnBVBSchecked = new System.Windows.Forms.Button();
            this.CbKorven = new System.Windows.Forms.CheckBox();
            this.CbBalken = new System.Windows.Forms.CheckBox();
            this.CbBalkenSchuin = new System.Windows.Forms.CheckBox();
            this.CbKorvenSchuin = new System.Windows.Forms.CheckBox();
            this.CbBalken3sn = new System.Windows.Forms.CheckBox();
            this.CbKorven3sn = new System.Windows.Forms.CheckBox();
            this.CbNettenSup = new System.Windows.Forms.CheckBox();
            this.CbKorvenLos = new System.Windows.Forms.CheckBox();
            this.CbMatOpMaat = new System.Windows.Forms.CheckBox();
            this.CbKorvenHrsp = new System.Windows.Forms.CheckBox();
            this.CbPoeren = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.ParametersTabPage.SuspendLayout();
            this.tabInstellingen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxPDF.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel, null);
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel, null);
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.tabControl, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(832, 553);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // tabControl
            // 
            this.structuresExtender.SetAttributeName(this.tabControl, null);
            this.structuresExtender.SetAttributeTypeName(this.tabControl, null);
            this.structuresExtender.SetBindPropertyName(this.tabControl, null);
            this.tabControl.Controls.Add(this.ParametersTabPage);
            this.tabControl.Controls.Add(this.tabInstellingen);
            this.tabControl.Controls.Add(this.Info);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(4, 4);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(824, 545);
            this.tabControl.TabIndex = 18;
            // 
            // ParametersTabPage
            // 
            this.structuresExtender.SetAttributeName(this.ParametersTabPage, null);
            this.structuresExtender.SetAttributeTypeName(this.ParametersTabPage, null);
            this.structuresExtender.SetBindPropertyName(this.ParametersTabPage, null);
            this.ParametersTabPage.Controls.Add(this.CbPoeren);
            this.ParametersTabPage.Controls.Add(this.CbMatOpMaat);
            this.ParametersTabPage.Controls.Add(this.CbKorvenHrsp);
            this.ParametersTabPage.Controls.Add(this.CbNettenSup);
            this.ParametersTabPage.Controls.Add(this.CbKorvenLos);
            this.ParametersTabPage.Controls.Add(this.CbBalken3sn);
            this.ParametersTabPage.Controls.Add(this.CbKorven3sn);
            this.ParametersTabPage.Controls.Add(this.CbBalkenSchuin);
            this.ParametersTabPage.Controls.Add(this.CbKorvenSchuin);
            this.ParametersTabPage.Controls.Add(this.CbBalken);
            this.ParametersTabPage.Controls.Add(this.CbKorven);
            this.ParametersTabPage.Controls.Add(this.btnBVBSchecked);
            this.ParametersTabPage.Controls.Add(this.groupBoxPDF);
            this.ParametersTabPage.Controls.Add(this.BtnPoer);
            this.ParametersTabPage.Controls.Add(this.BtnIFC);
            this.ParametersTabPage.Controls.Add(this.btnBalken3);
            this.ParametersTabPage.Controls.Add(this.btnKorf3);
            this.ParametersTabPage.Controls.Add(this.TxRadbutton);
            this.ParametersTabPage.Controls.Add(this.RadPerFase);
            this.ParametersTabPage.Controls.Add(this.RadAlles);
            this.ParametersTabPage.Controls.Add(this.lblStatus);
            this.ParametersTabPage.Controls.Add(this.lblNummering);
            this.ParametersTabPage.Controls.Add(this.btnNummeren);
            this.ParametersTabPage.Controls.Add(this.btnMatOpMaat);
            this.ParametersTabPage.Controls.Add(this.btnNetSup);
            this.ParametersTabPage.Controls.Add(this.btnKorfHrsp);
            this.ParametersTabPage.Controls.Add(this.btnKorfLos);
            this.ParametersTabPage.Controls.Add(this.btnBalkenSchuin);
            this.ParametersTabPage.Controls.Add(this.btnBalken);
            this.ParametersTabPage.Controls.Add(this.btnKorvenSchuin);
            this.ParametersTabPage.Controls.Add(this.btnKorf);
            this.ParametersTabPage.Controls.Add(this.okCancel1);
            this.ParametersTabPage.Controls.Add(this.btnBVBS);
            this.ParametersTabPage.Controls.Add(this.TxKlasse);
            this.ParametersTabPage.Location = new System.Drawing.Point(4, 25);
            this.ParametersTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.ParametersTabPage.Name = "ParametersTabPage";
            this.ParametersTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.ParametersTabPage.Size = new System.Drawing.Size(816, 516);
            this.ParametersTabPage.TabIndex = 2;
            this.ParametersTabPage.Text = "Betonstaal BVBS Export";
            this.ParametersTabPage.UseVisualStyleBackColor = true;
            this.ParametersTabPage.Click += new System.EventHandler(this.ParametersTabPage_Click);
            // 
            // BtnPoer
            // 
            this.structuresExtender.SetAttributeName(this.BtnPoer, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnPoer, null);
            this.structuresExtender.SetBindPropertyName(this.BtnPoer, null);
            this.BtnPoer.Location = new System.Drawing.Point(717, 67);
            this.BtnPoer.Name = "BtnPoer";
            this.BtnPoer.Size = new System.Drawing.Size(96, 32);
            this.BtnPoer.TabIndex = 21;
            this.BtnPoer.Text = "Poeren";
            this.BtnPoer.UseVisualStyleBackColor = true;
            this.BtnPoer.Click += new System.EventHandler(this.BtnPoer_Click);
            // 
            // BtnIFC
            // 
            this.structuresExtender.SetAttributeName(this.BtnIFC, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnIFC, null);
            this.structuresExtender.SetBindPropertyName(this.BtnIFC, null);
            this.BtnIFC.Location = new System.Drawing.Point(443, 325);
            this.BtnIFC.Name = "BtnIFC";
            this.BtnIFC.Size = new System.Drawing.Size(366, 47);
            this.BtnIFC.TabIndex = 20;
            this.BtnIFC.Text = "IFC Exporteren";
            this.BtnIFC.UseVisualStyleBackColor = true;
            this.BtnIFC.Click += new System.EventHandler(this.BtnIFC_Click);
            // 
            // btnBalken3
            // 
            this.structuresExtender.SetAttributeName(this.btnBalken3, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalken3, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalken3, null);
            this.btnBalken3.Location = new System.Drawing.Point(293, 105);
            this.btnBalken3.Name = "btnBalken3";
            this.btnBalken3.Size = new System.Drawing.Size(125, 32);
            this.btnBalken3.TabIndex = 19;
            this.btnBalken3.Text = " Balken 3-snedig";
            this.btnBalken3.UseVisualStyleBackColor = true;
            this.btnBalken3.Click += new System.EventHandler(this.BtnBalken3_Click);
            // 
            // BtnPdf
            // 
            this.structuresExtender.SetAttributeName(this.BtnPdf, null);
            this.structuresExtender.SetAttributeTypeName(this.BtnPdf, null);
            this.structuresExtender.SetBindPropertyName(this.BtnPdf, null);
            this.BtnPdf.Location = new System.Drawing.Point(15, 59);
            this.BtnPdf.Name = "BtnPdf";
            this.BtnPdf.Size = new System.Drawing.Size(356, 47);
            this.BtnPdf.TabIndex = 18;
            this.BtnPdf.Text = "Maak PDF Plotfiles";
            this.BtnPdf.UseVisualStyleBackColor = true;
            this.BtnPdf.Click += new System.EventHandler(this.BtnPdf_Click);
            // 
            // btnKorf3
            // 
            this.structuresExtender.SetAttributeName(this.btnKorf3, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorf3, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorf3, null);
            this.btnKorf3.Location = new System.Drawing.Point(293, 67);
            this.btnKorf3.Name = "btnKorf3";
            this.btnKorf3.Size = new System.Drawing.Size(125, 32);
            this.btnKorf3.TabIndex = 17;
            this.btnKorf3.Text = "Korven 3-snedig";
            this.btnKorf3.UseVisualStyleBackColor = true;
            this.btnKorf3.Click += new System.EventHandler(this.BtnKorf3_Click);
            // 
            // TxRadbutton
            // 
            this.structuresExtender.SetAttributeName(this.TxRadbutton, "Radbutton");
            this.structuresExtender.SetAttributeTypeName(this.TxRadbutton, "String");
            this.structuresExtender.SetBindPropertyName(this.TxRadbutton, null);
            this.TxRadbutton.Location = new System.Drawing.Point(547, 22);
            this.TxRadbutton.Name = "TxRadbutton";
            this.TxRadbutton.Size = new System.Drawing.Size(27, 22);
            this.TxRadbutton.TabIndex = 16;
            this.TxRadbutton.Text = "A";
            this.TxRadbutton.TextChanged += new System.EventHandler(this.TxRadbutton_TextChanged);
            // 
            // RadPerFase
            // 
            this.structuresExtender.SetAttributeName(this.RadPerFase, null);
            this.structuresExtender.SetAttributeTypeName(this.RadPerFase, null);
            this.RadPerFase.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadPerFase, null);
            this.RadPerFase.Location = new System.Drawing.Point(283, 23);
            this.RadPerFase.Name = "RadPerFase";
            this.RadPerFase.Size = new System.Drawing.Size(164, 21);
            this.RadPerFase.TabIndex = 15;
            this.RadPerFase.Text = "Per fase ��n submap";
            this.RadPerFase.UseVisualStyleBackColor = true;
            this.RadPerFase.CheckedChanged += new System.EventHandler(this.RadPerFase_CheckedChanged);
            // 
            // RadAlles
            // 
            this.structuresExtender.SetAttributeName(this.RadAlles, null);
            this.structuresExtender.SetAttributeTypeName(this.RadAlles, null);
            this.RadAlles.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.RadAlles, null);
            this.RadAlles.Checked = true;
            this.RadAlles.Location = new System.Drawing.Point(136, 23);
            this.RadAlles.Name = "RadAlles";
            this.RadAlles.Size = new System.Drawing.Size(133, 21);
            this.RadAlles.TabIndex = 14;
            this.RadAlles.TabStop = true;
            this.RadAlles.Text = "Alles in ��n map";
            this.RadAlles.UseVisualStyleBackColor = true;
            this.RadAlles.CheckedChanged += new System.EventHandler(this.RadAlles_CheckedChanged);
            // 
            // lblStatus
            // 
            this.structuresExtender.SetAttributeName(this.lblStatus, null);
            this.structuresExtender.SetAttributeTypeName(this.lblStatus, null);
            this.lblStatus.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblStatus, null);
            this.lblStatus.Location = new System.Drawing.Point(501, 2);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(46, 17);
            this.lblStatus.TabIndex = 13;
            this.lblStatus.Text = "status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatus.Visible = false;
            this.lblStatus.Click += new System.EventHandler(this.Label1_Click);
            // 
            // lblNummering
            // 
            this.structuresExtender.SetAttributeName(this.lblNummering, null);
            this.structuresExtender.SetAttributeTypeName(this.lblNummering, null);
            this.lblNummering.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblNummering, null);
            this.lblNummering.Location = new System.Drawing.Point(333, 13);
            this.lblNummering.Name = "lblNummering";
            this.lblNummering.Size = new System.Drawing.Size(0, 17);
            this.lblNummering.TabIndex = 12;
            // 
            // btnNummeren
            // 
            this.structuresExtender.SetAttributeName(this.btnNummeren, null);
            this.structuresExtender.SetAttributeTypeName(this.btnNummeren, null);
            this.structuresExtender.SetBindPropertyName(this.btnNummeren, null);
            this.btnNummeren.Location = new System.Drawing.Point(7, 7);
            this.btnNummeren.Name = "btnNummeren";
            this.btnNummeren.Size = new System.Drawing.Size(112, 23);
            this.btnNummeren.TabIndex = 11;
            this.btnNummeren.Text = "Check Nummering";
            this.btnNummeren.UseVisualStyleBackColor = true;
            this.btnNummeren.Visible = false;
            this.btnNummeren.Click += new System.EventHandler(this.BtnNummeren_Click);
            // 
            // btnMatOpMaat
            // 
            this.structuresExtender.SetAttributeName(this.btnMatOpMaat, null);
            this.structuresExtender.SetAttributeTypeName(this.btnMatOpMaat, null);
            this.structuresExtender.SetBindPropertyName(this.btnMatOpMaat, null);
            this.btnMatOpMaat.Enabled = false;
            this.btnMatOpMaat.Location = new System.Drawing.Point(591, 105);
            this.btnMatOpMaat.Name = "btnMatOpMaat";
            this.btnMatOpMaat.Size = new System.Drawing.Size(96, 32);
            this.btnMatOpMaat.TabIndex = 10;
            this.btnMatOpMaat.Text = "Mat op Maat";
            this.btnMatOpMaat.UseVisualStyleBackColor = true;
            // 
            // btnNetSup
            // 
            this.structuresExtender.SetAttributeName(this.btnNetSup, null);
            this.structuresExtender.SetAttributeTypeName(this.btnNetSup, null);
            this.structuresExtender.SetBindPropertyName(this.btnNetSup, null);
            this.btnNetSup.Enabled = false;
            this.btnNetSup.Location = new System.Drawing.Point(443, 105);
            this.btnNetSup.Name = "btnNetSup";
            this.btnNetSup.Size = new System.Drawing.Size(116, 32);
            this.btnNetSup.TabIndex = 9;
            this.btnNetSup.Text = "Netten en Sup";
            this.btnNetSup.UseVisualStyleBackColor = true;
            this.btnNetSup.Click += new System.EventHandler(this.BtnNetSup_Click);
            // 
            // btnKorfHrsp
            // 
            this.structuresExtender.SetAttributeName(this.btnKorfHrsp, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorfHrsp, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorfHrsp, null);
            this.btnKorfHrsp.Enabled = false;
            this.btnKorfHrsp.Location = new System.Drawing.Point(591, 67);
            this.btnKorfHrsp.Name = "btnKorfHrsp";
            this.btnKorfHrsp.Size = new System.Drawing.Size(96, 32);
            this.btnKorfHrsp.TabIndex = 8;
            this.btnKorfHrsp.Text = "Korven Hrsp";
            this.btnKorfHrsp.UseVisualStyleBackColor = true;
            // 
            // btnKorfLos
            // 
            this.structuresExtender.SetAttributeName(this.btnKorfLos, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorfLos, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorfLos, null);
            this.btnKorfLos.Enabled = false;
            this.btnKorfLos.Location = new System.Drawing.Point(443, 67);
            this.btnKorfLos.Name = "btnKorfLos";
            this.btnKorfLos.Size = new System.Drawing.Size(116, 32);
            this.btnKorfLos.TabIndex = 7;
            this.btnKorfLos.Text = "Korven Los";
            this.btnKorfLos.UseVisualStyleBackColor = true;
            // 
            // btnBalkenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.btnBalkenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalkenSchuin, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalkenSchuin, null);
            this.btnBalkenSchuin.Location = new System.Drawing.Point(147, 105);
            this.btnBalkenSchuin.Name = "btnBalkenSchuin";
            this.btnBalkenSchuin.Size = new System.Drawing.Size(116, 32);
            this.btnBalkenSchuin.TabIndex = 6;
            this.btnBalkenSchuin.Text = "Balken Schuin";
            this.btnBalkenSchuin.UseVisualStyleBackColor = true;
            this.btnBalkenSchuin.Click += new System.EventHandler(this.BtnBalkenSchuin_Click);
            // 
            // btnBalken
            // 
            this.structuresExtender.SetAttributeName(this.btnBalken, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBalken, null);
            this.structuresExtender.SetBindPropertyName(this.btnBalken, null);
            this.btnBalken.Location = new System.Drawing.Point(23, 105);
            this.btnBalken.Name = "btnBalken";
            this.btnBalken.Size = new System.Drawing.Size(96, 32);
            this.btnBalken.TabIndex = 5;
            this.btnBalken.Text = "Balken";
            this.btnBalken.UseVisualStyleBackColor = true;
            this.btnBalken.Click += new System.EventHandler(this.BtnBalken_Click);
            // 
            // btnKorvenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.btnKorvenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorvenSchuin, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorvenSchuin, null);
            this.btnKorvenSchuin.Location = new System.Drawing.Point(147, 67);
            this.btnKorvenSchuin.Name = "btnKorvenSchuin";
            this.btnKorvenSchuin.Size = new System.Drawing.Size(116, 32);
            this.btnKorvenSchuin.TabIndex = 4;
            this.btnKorvenSchuin.Text = "Korven Schuin";
            this.btnKorvenSchuin.UseVisualStyleBackColor = true;
            this.btnKorvenSchuin.Click += new System.EventHandler(this.BtnKorvenSchuin_Click);
            // 
            // btnKorf
            // 
            this.structuresExtender.SetAttributeName(this.btnKorf, null);
            this.structuresExtender.SetAttributeTypeName(this.btnKorf, null);
            this.structuresExtender.SetBindPropertyName(this.btnKorf, null);
            this.btnKorf.Location = new System.Drawing.Point(23, 67);
            this.btnKorf.Name = "btnKorf";
            this.btnKorf.Size = new System.Drawing.Size(96, 32);
            this.btnKorf.TabIndex = 3;
            this.btnKorf.Text = "Korven";
            this.btnKorf.UseVisualStyleBackColor = true;
            this.btnKorf.Click += new System.EventHandler(this.BtnKorf_Click);
            // 
            // okCancel1
            // 
            this.structuresExtender.SetAttributeName(this.okCancel1, null);
            this.structuresExtender.SetAttributeTypeName(this.okCancel1, null);
            this.structuresExtender.SetBindPropertyName(this.okCancel1, null);
            this.okCancel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.okCancel1.Location = new System.Drawing.Point(4, 475);
            this.okCancel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.okCancel1.Name = "okCancel1";
            this.okCancel1.Size = new System.Drawing.Size(808, 37);
            this.okCancel1.TabIndex = 2;
            this.okCancel1.OkClicked += new System.EventHandler(this.OkApplyModifyGetOnOffCancel_OkClicked);
            this.okCancel1.CancelClicked += new System.EventHandler(this.OkApplyModifyGetOnOffCancel_CancelClicked);
            // 
            // btnBVBS
            // 
            this.structuresExtender.SetAttributeName(this.btnBVBS, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBVBS, null);
            this.structuresExtender.SetBindPropertyName(this.btnBVBS, null);
            this.btnBVBS.Location = new System.Drawing.Point(443, 142);
            this.btnBVBS.Name = "btnBVBS";
            this.btnBVBS.Size = new System.Drawing.Size(366, 54);
            this.btnBVBS.TabIndex = 1;
            this.btnBVBS.Text = "Alle BVBS exporteren";
            this.btnBVBS.UseVisualStyleBackColor = true;
            this.btnBVBS.Click += new System.EventHandler(this.BtnBVBS_Click);
            // 
            // TxKlasse
            // 
            this.structuresExtender.SetAttributeName(this.TxKlasse, "Klasse");
            this.structuresExtender.SetAttributeTypeName(this.TxKlasse, "String");
            this.structuresExtender.SetBindPropertyName(this.TxKlasse, null);
            this.TxKlasse.Location = new System.Drawing.Point(504, 22);
            this.TxKlasse.Name = "TxKlasse";
            this.TxKlasse.Size = new System.Drawing.Size(27, 22);
            this.TxKlasse.TabIndex = 0;
            this.TxKlasse.Text = "4";
            this.TxKlasse.TextChanged += new System.EventHandler(this.TxKlasse_TextChanged);
            // 
            // tabInstellingen
            // 
            this.structuresExtender.SetAttributeName(this.tabInstellingen, null);
            this.structuresExtender.SetAttributeTypeName(this.tabInstellingen, null);
            this.structuresExtender.SetBindPropertyName(this.tabInstellingen, null);
            this.tabInstellingen.Controls.Add(this.TxKorven);
            this.tabInstellingen.Controls.Add(this.dataGridView1);
            this.tabInstellingen.Controls.Add(this.saveLoad1);
            this.tabInstellingen.Location = new System.Drawing.Point(4, 25);
            this.tabInstellingen.Name = "tabInstellingen";
            this.tabInstellingen.Size = new System.Drawing.Size(192, 71);
            this.tabInstellingen.TabIndex = 3;
            this.tabInstellingen.Text = "Instellingen";
            this.tabInstellingen.UseVisualStyleBackColor = true;
            // 
            // TxKorven
            // 
            this.structuresExtender.SetAttributeName(this.TxKorven, null);
            this.structuresExtender.SetAttributeTypeName(this.TxKorven, null);
            this.structuresExtender.SetBindPropertyName(this.TxKorven, null);
            this.TxKorven.Location = new System.Drawing.Point(31, 351);
            this.TxKorven.Name = "TxKorven";
            this.TxKorven.Size = new System.Drawing.Size(82, 22);
            this.TxKorven.TabIndex = 2;
            this.TxKorven.Text = "Korven";
            // 
            // dataGridView1
            // 
            this.structuresExtender.SetAttributeName(this.dataGridView1, null);
            this.structuresExtender.SetAttributeTypeName(this.dataGridView1, null);
            this.structuresExtender.SetBindPropertyName(this.dataGridView1, null);
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naam,
            this.Filter,
            this.Inc_Exc,
            this.Prefix});
            this.dataGridView1.Location = new System.Drawing.Point(14, 75);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(559, 258);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Naam
            // 
            this.Naam.HeaderText = "Naam";
            this.Naam.MinimumWidth = 6;
            this.Naam.Name = "Naam";
            this.Naam.Width = 125;
            // 
            // Filter
            // 
            this.Filter.HeaderText = "Filter";
            this.Filter.MinimumWidth = 6;
            this.Filter.Name = "Filter";
            this.Filter.Width = 125;
            // 
            // Inc_Exc
            // 
            this.Inc_Exc.HeaderText = "Incl/Excl.";
            this.Inc_Exc.MinimumWidth = 6;
            this.Inc_Exc.Name = "Inc_Exc";
            this.Inc_Exc.Width = 125;
            // 
            // Prefix
            // 
            this.Prefix.HeaderText = "Prefix";
            this.Prefix.MinimumWidth = 6;
            this.Prefix.Name = "Prefix";
            this.Prefix.Width = 125;
            // 
            // saveLoad1
            // 
            this.structuresExtender.SetAttributeName(this.saveLoad1, null);
            this.structuresExtender.SetAttributeTypeName(this.saveLoad1, null);
            this.saveLoad1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.saveLoad1, null);
            this.saveLoad1.Dock = System.Windows.Forms.DockStyle.Top;
            this.saveLoad1.HelpFileType = Tekla.Structures.Dialog.UIControls.SaveLoad.HelpFileTypeEnum.General;
            this.saveLoad1.HelpKeyword = "";
            this.saveLoad1.HelpUrl = "";
            this.saveLoad1.Location = new System.Drawing.Point(0, 0);
            this.saveLoad1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.saveLoad1.Name = "saveLoad1";
            this.saveLoad1.SaveAsText = "";
            this.saveLoad1.Size = new System.Drawing.Size(192, 43);
            this.saveLoad1.TabIndex = 0;
            this.saveLoad1.UserDefinedHelpFilePath = null;
            // 
            // Info
            // 
            this.structuresExtender.SetAttributeName(this.Info, null);
            this.structuresExtender.SetAttributeTypeName(this.Info, null);
            this.structuresExtender.SetBindPropertyName(this.Info, null);
            this.Info.Controls.Add(this.lblDatum);
            this.Info.Controls.Add(this.label2);
            this.Info.Controls.Add(this.pictureBox1);
            this.Info.Controls.Add(this.lblVersie);
            this.Info.Controls.Add(this.lblVersion);
            this.Info.Location = new System.Drawing.Point(4, 25);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(192, 71);
            this.Info.TabIndex = 4;
            this.Info.Text = "Info";
            this.Info.UseVisualStyleBackColor = true;
            // 
            // lblDatum
            // 
            this.structuresExtender.SetAttributeName(this.lblDatum, null);
            this.structuresExtender.SetAttributeTypeName(this.lblDatum, null);
            this.lblDatum.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblDatum, null);
            this.lblDatum.Location = new System.Drawing.Point(173, 16);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(64, 17);
            this.lblDatum.TabIndex = 4;
            this.lblDatum.Text = "datedate";
            // 
            // label2
            // 
            this.structuresExtender.SetAttributeName(this.label2, null);
            this.structuresExtender.SetAttributeTypeName(this.label2, null);
            this.label2.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label2, null);
            this.label2.Location = new System.Drawing.Point(129, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Datum:";
            // 
            // pictureBox1
            // 
            this.structuresExtender.SetAttributeName(this.pictureBox1, null);
            this.structuresExtender.SetAttributeTypeName(this.pictureBox1, null);
            this.structuresExtender.SetBindPropertyName(this.pictureBox1, null);
            this.pictureBox1.Image = global::BS_Export.Properties.Resources.betonstaal_scherm_small;
            this.pictureBox1.Location = new System.Drawing.Point(169, 251);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(273, 97);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // lblVersie
            // 
            this.structuresExtender.SetAttributeName(this.lblVersie, null);
            this.structuresExtender.SetAttributeTypeName(this.lblVersie, null);
            this.lblVersie.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblVersie, null);
            this.lblVersie.Location = new System.Drawing.Point(56, 15);
            this.lblVersie.Name = "lblVersie";
            this.lblVersie.Size = new System.Drawing.Size(26, 17);
            this.lblVersie.TabIndex = 1;
            this.lblVersie.Text = "xxx";
            // 
            // lblVersion
            // 
            this.structuresExtender.SetAttributeName(this.lblVersion, null);
            this.structuresExtender.SetAttributeTypeName(this.lblVersion, null);
            this.lblVersion.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.lblVersion, null);
            this.lblVersion.Location = new System.Drawing.Point(13, 16);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(52, 17);
            this.lblVersion.TabIndex = 0;
            this.lblVersion.Text = "Versie:";
            // 
            // groupBoxPDF
            // 
            this.structuresExtender.SetAttributeName(this.groupBoxPDF, null);
            this.structuresExtender.SetAttributeTypeName(this.groupBoxPDF, null);
            this.structuresExtender.SetBindPropertyName(this.groupBoxPDF, null);
            this.groupBoxPDF.Controls.Add(this.CbPdfG);
            this.groupBoxPDF.Controls.Add(this.CbPdfC);
            this.groupBoxPDF.Controls.Add(this.BtnPdf);
            this.groupBoxPDF.Location = new System.Drawing.Point(18, 266);
            this.groupBoxPDF.Name = "groupBoxPDF";
            this.groupBoxPDF.Size = new System.Drawing.Size(400, 120);
            this.groupBoxPDF.TabIndex = 22;
            this.groupBoxPDF.TabStop = false;
            this.groupBoxPDF.Text = "PDF Plotfiles";
            // 
            // CbPdfC
            // 
            this.structuresExtender.SetAttributeName(this.CbPdfC, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPdfC, null);
            this.CbPdfC.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPdfC, null);
            this.CbPdfC.Checked = true;
            this.CbPdfC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPdfC.Location = new System.Drawing.Point(40, 32);
            this.CbPdfC.Name = "CbPdfC";
            this.CbPdfC.Size = new System.Drawing.Size(114, 21);
            this.CbPdfC.TabIndex = 19;
            this.CbPdfC.Text = "C-tekeningen";
            this.CbPdfC.UseVisualStyleBackColor = true;
            // 
            // CbPdfG
            // 
            this.structuresExtender.SetAttributeName(this.CbPdfG, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPdfG, null);
            this.CbPdfG.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPdfG, null);
            this.CbPdfG.Checked = true;
            this.CbPdfG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPdfG.Location = new System.Drawing.Point(228, 32);
            this.CbPdfG.Name = "CbPdfG";
            this.CbPdfG.Size = new System.Drawing.Size(116, 21);
            this.CbPdfG.TabIndex = 20;
            this.CbPdfG.Text = "G-tekeningen";
            this.CbPdfG.UseVisualStyleBackColor = true;
            // 
            // btnBVBSchecked
            // 
            this.structuresExtender.SetAttributeName(this.btnBVBSchecked, null);
            this.structuresExtender.SetAttributeTypeName(this.btnBVBSchecked, null);
            this.structuresExtender.SetBindPropertyName(this.btnBVBSchecked, null);
            this.btnBVBSchecked.Location = new System.Drawing.Point(23, 143);
            this.btnBVBSchecked.Name = "btnBVBSchecked";
            this.btnBVBSchecked.Size = new System.Drawing.Size(395, 54);
            this.btnBVBSchecked.TabIndex = 23;
            this.btnBVBSchecked.Text = "Geselecteerde BVBS exporteren";
            this.btnBVBSchecked.UseVisualStyleBackColor = true;
            this.btnBVBSchecked.Click += new System.EventHandler(this.BtnBVBSchecked_Click);
            // 
            // CbKorven
            // 
            this.structuresExtender.SetAttributeName(this.CbKorven, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorven, null);
            this.CbKorven.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorven, null);
            this.CbKorven.Checked = true;
            this.CbKorven.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorven.Location = new System.Drawing.Point(4, 75);
            this.CbKorven.Name = "CbKorven";
            this.CbKorven.Size = new System.Drawing.Size(18, 17);
            this.CbKorven.TabIndex = 24;
            this.CbKorven.UseVisualStyleBackColor = true;
            // 
            // CbBalken
            // 
            this.structuresExtender.SetAttributeName(this.CbBalken, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalken, null);
            this.CbBalken.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalken, null);
            this.CbBalken.Checked = true;
            this.CbBalken.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalken.Location = new System.Drawing.Point(4, 113);
            this.CbBalken.Name = "CbBalken";
            this.CbBalken.Size = new System.Drawing.Size(18, 17);
            this.CbBalken.TabIndex = 25;
            this.CbBalken.UseVisualStyleBackColor = true;
            // 
            // CbBalkenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.CbBalkenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalkenSchuin, null);
            this.CbBalkenSchuin.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalkenSchuin, null);
            this.CbBalkenSchuin.Checked = true;
            this.CbBalkenSchuin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalkenSchuin.Location = new System.Drawing.Point(127, 113);
            this.CbBalkenSchuin.Name = "CbBalkenSchuin";
            this.CbBalkenSchuin.Size = new System.Drawing.Size(18, 17);
            this.CbBalkenSchuin.TabIndex = 27;
            this.CbBalkenSchuin.UseVisualStyleBackColor = true;
            // 
            // CbKorvenSchuin
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenSchuin, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenSchuin, null);
            this.CbKorvenSchuin.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenSchuin, null);
            this.CbKorvenSchuin.Checked = true;
            this.CbKorvenSchuin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenSchuin.Location = new System.Drawing.Point(128, 75);
            this.CbKorvenSchuin.Name = "CbKorvenSchuin";
            this.CbKorvenSchuin.Size = new System.Drawing.Size(18, 17);
            this.CbKorvenSchuin.TabIndex = 26;
            this.CbKorvenSchuin.UseVisualStyleBackColor = true;
            // 
            // CbBalken3sn
            // 
            this.structuresExtender.SetAttributeName(this.CbBalken3sn, null);
            this.structuresExtender.SetAttributeTypeName(this.CbBalken3sn, null);
            this.CbBalken3sn.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbBalken3sn, null);
            this.CbBalken3sn.Checked = true;
            this.CbBalken3sn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbBalken3sn.Location = new System.Drawing.Point(271, 113);
            this.CbBalken3sn.Name = "CbBalken3sn";
            this.CbBalken3sn.Size = new System.Drawing.Size(18, 17);
            this.CbBalken3sn.TabIndex = 29;
            this.CbBalken3sn.UseVisualStyleBackColor = true;
            // 
            // CbKorven3sn
            // 
            this.structuresExtender.SetAttributeName(this.CbKorven3sn, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorven3sn, null);
            this.CbKorven3sn.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorven3sn, null);
            this.CbKorven3sn.Checked = true;
            this.CbKorven3sn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorven3sn.Location = new System.Drawing.Point(271, 75);
            this.CbKorven3sn.Name = "CbKorven3sn";
            this.CbKorven3sn.Size = new System.Drawing.Size(18, 17);
            this.CbKorven3sn.TabIndex = 28;
            this.CbKorven3sn.UseVisualStyleBackColor = true;
            // 
            // CbNettenSup
            // 
            this.structuresExtender.SetAttributeName(this.CbNettenSup, null);
            this.structuresExtender.SetAttributeTypeName(this.CbNettenSup, null);
            this.CbNettenSup.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbNettenSup, null);
            this.CbNettenSup.Checked = true;
            this.CbNettenSup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbNettenSup.Location = new System.Drawing.Point(424, 113);
            this.CbNettenSup.Name = "CbNettenSup";
            this.CbNettenSup.Size = new System.Drawing.Size(18, 17);
            this.CbNettenSup.TabIndex = 31;
            this.CbNettenSup.UseVisualStyleBackColor = true;
            // 
            // CbKorvenLos
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenLos, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenLos, null);
            this.CbKorvenLos.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenLos, null);
            this.CbKorvenLos.Checked = true;
            this.CbKorvenLos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenLos.Location = new System.Drawing.Point(424, 75);
            this.CbKorvenLos.Name = "CbKorvenLos";
            this.CbKorvenLos.Size = new System.Drawing.Size(18, 17);
            this.CbKorvenLos.TabIndex = 30;
            this.CbKorvenLos.UseVisualStyleBackColor = true;
            // 
            // CbMatOpMaat
            // 
            this.structuresExtender.SetAttributeName(this.CbMatOpMaat, null);
            this.structuresExtender.SetAttributeTypeName(this.CbMatOpMaat, null);
            this.CbMatOpMaat.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbMatOpMaat, null);
            this.CbMatOpMaat.Checked = true;
            this.CbMatOpMaat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbMatOpMaat.Location = new System.Drawing.Point(571, 113);
            this.CbMatOpMaat.Name = "CbMatOpMaat";
            this.CbMatOpMaat.Size = new System.Drawing.Size(18, 17);
            this.CbMatOpMaat.TabIndex = 33;
            this.CbMatOpMaat.UseVisualStyleBackColor = true;
            // 
            // CbKorvenHrsp
            // 
            this.structuresExtender.SetAttributeName(this.CbKorvenHrsp, null);
            this.structuresExtender.SetAttributeTypeName(this.CbKorvenHrsp, null);
            this.CbKorvenHrsp.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbKorvenHrsp, null);
            this.CbKorvenHrsp.Checked = true;
            this.CbKorvenHrsp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbKorvenHrsp.Location = new System.Drawing.Point(571, 75);
            this.CbKorvenHrsp.Name = "CbKorvenHrsp";
            this.CbKorvenHrsp.Size = new System.Drawing.Size(18, 17);
            this.CbKorvenHrsp.TabIndex = 32;
            this.CbKorvenHrsp.UseVisualStyleBackColor = true;
            // 
            // CbPoeren
            // 
            this.structuresExtender.SetAttributeName(this.CbPoeren, null);
            this.structuresExtender.SetAttributeTypeName(this.CbPoeren, null);
            this.CbPoeren.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.CbPoeren, null);
            this.CbPoeren.Checked = true;
            this.CbPoeren.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbPoeren.Location = new System.Drawing.Point(698, 75);
            this.CbPoeren.Name = "CbPoeren";
            this.CbPoeren.Size = new System.Drawing.Size(18, 17);
            this.CbPoeren.TabIndex = 34;
            this.CbPoeren.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.structuresExtender.SetAttributeName(this, null);
            this.structuresExtender.SetAttributeTypeName(this, null);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.structuresExtender.SetBindPropertyName(this, null);
            this.ClientSize = new System.Drawing.Size(832, 553);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Betonstaal Dashboard";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.ParametersTabPage.ResumeLayout(false);
            this.ParametersTabPage.PerformLayout();
            this.tabInstellingen.ResumeLayout(false);
            this.tabInstellingen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Info.ResumeLayout(false);
            this.Info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxPDF.ResumeLayout(false);
            this.groupBoxPDF.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage ParametersTabPage;
        private System.Windows.Forms.TextBox TxKlasse;
        private Tekla.Structures.Dialog.UIControls.OkCancel okCancel1;
        private System.Windows.Forms.Button btnBVBS;
        private System.Windows.Forms.Button btnKorvenSchuin;
        private System.Windows.Forms.Button btnKorf;
        private System.Windows.Forms.Button btnBalkenSchuin;
        private System.Windows.Forms.Button btnBalken;
        private System.Windows.Forms.Button btnKorfHrsp;
        private System.Windows.Forms.Button btnKorfLos;
        private System.Windows.Forms.Button btnMatOpMaat;
        private System.Windows.Forms.Button btnNetSup;
        private System.Windows.Forms.Button btnNummeren;
        private System.Windows.Forms.Label lblNummering;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.RadioButton RadPerFase;
        private System.Windows.Forms.RadioButton RadAlles;
        private System.Windows.Forms.TextBox TxRadbutton;
        private System.Windows.Forms.TabPage tabInstellingen;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Tekla.Structures.Dialog.UIControls.SaveLoad saveLoad1;
        private System.Windows.Forms.Button btnKorf3;
        private System.Windows.Forms.Button BtnPdf;
        private System.Windows.Forms.Button btnBalken3;
        private System.Windows.Forms.TextBox TxKorven;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naam;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inc_Exc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prefix;
        private System.Windows.Forms.Button BtnIFC;
        private System.Windows.Forms.Button BtnPoer;
        private System.Windows.Forms.TabPage Info;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblVersie;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxPDF;
        private System.Windows.Forms.CheckBox CbPdfG;
        private System.Windows.Forms.CheckBox CbPdfC;
        private System.Windows.Forms.CheckBox CbPoeren;
        private System.Windows.Forms.CheckBox CbMatOpMaat;
        private System.Windows.Forms.CheckBox CbKorvenHrsp;
        private System.Windows.Forms.CheckBox CbNettenSup;
        private System.Windows.Forms.CheckBox CbKorvenLos;
        private System.Windows.Forms.CheckBox CbBalken3sn;
        private System.Windows.Forms.CheckBox CbKorven3sn;
        private System.Windows.Forms.CheckBox CbBalkenSchuin;
        private System.Windows.Forms.CheckBox CbKorvenSchuin;
        private System.Windows.Forms.CheckBox CbBalken;
        private System.Windows.Forms.CheckBox CbKorven;
        private System.Windows.Forms.Button btnBVBSchecked;
    }
}